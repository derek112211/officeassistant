//
//  StaffLoginModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 8/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import UIKit
import SwiftyJSON

class StaffLoginModel: NSObject {
    var staff_number:String = ""
    var home_shop_id:String = ""
    var short_name:String = ""
    var shop_name:String = ""
    var device_code:String = ""
    var address:String = ""
    var dob:String = ""
    var id:String = ""

    var firstname_zho:String = "" //first_name
    var firstname_eng:String = "" //first_name_eng
    var lastname_zho:String = "" //last_name
    var lastname_eng:String = ""
    var phone:String = "" //phone1
    var join_date:String = "" //date_of_entry
    var email:String = "" //email
    var gender:String = ""
    var login_type = ""
    var shop_phone = ""
    var ip = ""
    //var token_expiry:String!
    //    var token:String = ""
    //var gallery:Bool = false
    //var qr_code_base64:String = ""
    
    override init() {
        super.init()
    }
    
    init(userInfo:JSON) {
        let data = userInfo
        
        if let id = data["id"].string {
            self.id = id
        }
        
        if let staff_number = data["staff_number"].string {
            self.staff_number = staff_number
        }
        
        if let shop_name = data["shop_name"].string {
            self.shop_name = shop_name
        }
        
        if let address = data["address"].string {
            self.address = address
        }
        
        if let firstname_zho = data["first_name"].string {
            self.firstname_zho = firstname_zho
        }
        
        if let firstname_eng = data["first_name_eng"].string {
            self.firstname_eng = firstname_eng
        }
        
        if let lastname_zho = data["last_name"].string {
            self.lastname_zho = lastname_zho
        }
        
        if let lastname_eng = data["last_name_eng"].string {
            self.lastname_eng = lastname_eng
        }

        if let short_name = data["short_name"].string {
            self.short_name = short_name
        }
        
        if let phone = data["phone"].string {
            self.phone = phone
        }
        if let join_date = data["date_of_entry"].string {
            self.join_date = join_date
        }
        if let email = data["email"].string {
            self.email = email
        }
        if let dob = data["dob"].string {
            self.dob = dob
        }
        if let gender = data["sex"].string {
            if(data["sex"] == 1){
                self.gender = "M"
            }else if(data["sex"] == 2){
                self.gender = "F"
            }
        }
//        if let ip = data["ip"].string {
//            self.ip = ip
//        }
//        if let token = data["login_token"].string {
//            self.token = token
//        }

        if let device_code = data["device_code"].string{
            self.device_code = device_code
        }
        if let home_shop_id = data["home_shop_id"].string{
            self.home_shop_id = home_shop_id
        }
        if let shop_phone = data["shop_phone"].string {
            self.shop_phone = shop_phone
        }
        if let ip = data["ip"].string {
            self.ip = ip
        }
        
    }
    
    func save_profile() {
        DataHelper.save(obj: self.id, key: "id")
        DataHelper.save(obj: self.staff_number, key: "staff_number")
        DataHelper.save(obj: self.firstname_zho, key: "firstname_zho")
        DataHelper.save(obj: self.firstname_eng, key: "firstname_eng")
        DataHelper.save(obj: self.lastname_zho, key: "lastname_zho")
        DataHelper.save(obj: self.lastname_eng, key: "lastname_eng")
        DataHelper.save(obj: self.short_name, key: "short_name")
        DataHelper.save(obj: self.phone, key: "phone")
        DataHelper.save(obj: self.join_date, key: "join_date")
        DataHelper.save(obj: self.email, key: "email")
        DataHelper.save(obj: self.address, key: "address")
        DataHelper.save(obj: self.dob, key: "dob")
        DataHelper.save(obj: self.gender, key: "gender")
//        DataHelper.save(obj: self.token, key: "token")
        DataHelper.save(obj: self.shop_phone, key: "shop_phone")
        DataHelper.save(obj: self.shop_name, key: "shop_name")
        DataHelper.save(obj: "staff", key:"login_type")
        DataHelper.save(obj: self.device_code, key: "device_code")
        DataHelper.save(obj: self.home_shop_id, key: "home_shop_id")
        DataHelper.save(obj: self.ip, key: "ip")
        
    }
    
    func save() {
        
        DataHelper.save(obj: self.id, key: "id")
        DataHelper.save(obj: self.staff_number, key: "staff_number")
        DataHelper.save(obj: self.firstname_zho, key: "firstname_zho")
        DataHelper.save(obj: self.firstname_eng, key: "firstname_eng")
        DataHelper.save(obj: self.lastname_zho, key: "lastname_zho")
        DataHelper.save(obj: self.lastname_eng, key: "lastname_eng")
        DataHelper.save(obj: self.short_name, key: "short_name")
        DataHelper.save(obj: self.phone, key: "phone")
        DataHelper.save(obj: self.join_date, key: "join_date")
        DataHelper.save(obj: self.email, key: "email")
        DataHelper.save(obj: self.address, key: "address")
        DataHelper.save(obj: self.dob, key: "dob")
        DataHelper.save(obj: self.gender, key: "gender")
//        DataHelper.save(obj: self.token, key: "token")
        DataHelper.save(obj: self.shop_phone, key: "shop_phone")
        DataHelper.save(obj: self.shop_name, key: "shop_name")
        DataHelper.save(obj: "staff", key:"login_type")
        DataHelper.save(obj: self.device_code, key: "device_code")
        DataHelper.save(obj: self.home_shop_id, key: "home_shop_id")
        DataHelper.save(obj: self.ip, key: "ip")
    }
    
    func clear() {
        let mirror = Mirror(reflecting: self)
        for (name, value) in mirror.children {
            guard let name = name else { continue }
            DataHelper.save(obj: "", key: name)
        }
    }
    
    class func getOne() -> StaffLoginModel {
        let one = StaffLoginModel()
        
        if let f = DataHelper.get(key: "id") as? String {
            one.id = f
        }
        
        if let f = DataHelper.get(key: "shop_name") as? String {
            one.shop_name = f
        }
        if let f = DataHelper.get(key: "staff_number") as? String {
            one.staff_number = f
        }
        
        if let f = DataHelper.get(key: "firstname_zho") as? String {
            one.firstname_zho = f
        }
        //one.firstname_eng = DataHelper.get(key: "firstname_eng") as! String
        if let f = DataHelper.get(key: "firstname_eng") as? String {
            one.firstname_eng = f
        }
        //one.lastname_zho = DataHelper.get(key: "lastname_zho") as! String
        if let f = DataHelper.get(key: "lastname_zho") as? String {
            one.lastname_zho = f
        }
        
        //one.lastname_eng = DataHelper.get(key: "lastname_eng") as! String
        if let f = DataHelper.get(key: "lastname_eng") as? String {
            one.lastname_eng = f
        }
        
        //one.phone = DataHelper.get(key: "phone") as! String
        if let f = DataHelper.get(key: "phone") as? String {
            one.phone = f
        }
        
        //one.join_date = DataHelper.get(key: "join_date") as! String
        if let f = DataHelper.get(key: "join_date") as? String {
            one.join_date = f
        }
        
        //one.email = DataHelper.get(key: "email") as! String
        if let f = DataHelper.get(key: "email") as? String {
            one.email = f
        }
        
        if let f = DataHelper.get(key: "login_type") as? String {
            one.login_type = f
        }
        
        //one.gender = DataHelper.get(key: "gender") as! String
        if let f = DataHelper.get(key: "dob") as? String {
            one.dob = f
        }
        
        //one.gender = DataHelper.get(key: "gender") as! String
        if let f = DataHelper.get(key: "gender") as? String {
            one.gender = f
        }
        
//        if let f = DataHelper.get(key: "token") as? String {
//            one.token = f
//        }
        
        if let f = DataHelper.get(key: "device_code") as? String{
            one.device_code = f
        }
        
        if let f = DataHelper.get(key: "home_shop_id") as? String{
            one.home_shop_id = f
        }
        
        if let f = DataHelper.get(key: "address") as? String{
            one.address = f
        }
        
        if let f = DataHelper.get(key: "shop_phone") as? String{
            one.shop_phone = f
        }
        
        if let f = DataHelper.get(key: "short_name") as? String{
            one.short_name = f
        }
        return one
    }
    
    
    func getName() -> String {
        if(lastname_eng != "" && firstname_eng != ""){
            if(short_name != ""){
                return "\(self.lastname_eng) \(self.firstname_eng), \(self.short_name)"
            }else{
                return "\(self.lastname_eng) \(self.firstname_eng)"
            }
        }
        return ""
    }
    
    func getDOB() -> String {
        //return self.dob_year + "-" + self.dob_month + "-" + self.dob_day
        return self.dob
    }
    
    class func isLoggedIn() -> Bool {
        if let id = StaffLoginModel.getOne().id as? String {
            if !id.isEmpty {
                return true
            }
        }
        return false
    }
    
    class func isStaff() -> Bool {
        let login_type = StaffLoginModel.getOne().login_type
        if login_type.isEmpty {
            return false
        }
        if(login_type == "host"){
            return false
        }
        return true
    }
}
