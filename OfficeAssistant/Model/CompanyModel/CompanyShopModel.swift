//
//  CompanyShopModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 30/12/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

class CompanyShopModel : NSObject{
    
    var shop_id:String!
    var name:String!
    var address:String!
    var phone:String!
    var email:String!
    var display_name:String!
    
    init(data:JSON){
        
        if let shop_id = data["id"].string{
            self.shop_id = shop_id
        }
        if let name = data["name"].string{
            self.name = name
        }
        if let address = data["address"].string{
            self.address = address
        }
        if let phone = data["phone"].string{
            self.phone = phone
        }
        if let email = data["email"].string{
            self.email = email
        }
        if let display_name = data["display_name"].string{
            self.display_name = display_name
        }
    }
    
    
}
