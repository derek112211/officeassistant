//
//  CompanyStaffModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 30/12/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import SwiftyJSON

class CompanyStaffModel:NSObject{
    
    var id:String!
    var user_name:String!
    var home_shop_id:String!
    var job_title_id:String!
    var staff_number:String!
    var first_name:String!
    var last_name:String!
    var first_name_eng:String!
    var last_name_eng:String!
    var short_name:String!
    var dob:String!
    var phone:String!
    var email:String!
    var sex:String!
    var shop_name:String!
    var shop_phone:String!
    
    init(data:JSON){
        
        if let id = data["id"].string{
            self.id = id
        }
        if let user_name = data["user_name"].string{
            self.user_name = user_name
        }
        if let home_shop_id = data["home_shop_id"].string{
            self.home_shop_id = home_shop_id
        }
        if let job_title_id = data["job_title_id"].string{
            self.job_title_id = job_title_id
        }
        if let staff_number = data["staff_number"].string{
            self.staff_number = staff_number
        }
        if let first_name = data["first_name"].string{
            self.first_name = first_name
        }
        if let last_name = data["last_name"].string{
            self.last_name = last_name
        }
        if let first_name_eng = data["first_name_eng"].string{
            self.first_name_eng = first_name_eng
        }
        if let last_name_eng = data["last_name_eng"].string{
            self.last_name_eng = last_name_eng
        }
        if let dob = data["dob"].string{
            self.dob = dob
        }
        if let phone = data["phone"].string{
            if(phone.isEmptyStr()){
                self.phone = "NA"
            }else{
                self.phone = phone
            }
            
        }
        if let email = data["email"].string{
            self.email = email
        }
        if let short_name = data["short_name"].string{
            self.short_name = short_name
        }
        if let sex = data["sex"].string{
            if(sex == "1"){
                self.sex = "M"
            }else if(sex == "2"){
                self.sex = "F"
            }
        }
        if let shop_name = data["shop_name"].string{
            self.shop_name = shop_name
        }
        if let shop_phone = data["shop_phone"].string{
            if(shop_phone.isEmptyStr()){
                self.shop_phone = "NA"
            }else{
                self.shop_phone = shop_phone
            }
        }
    }
    
    func getDisplayName() -> String{
        return self.staff_number+" "+self.last_name_eng+" "+self.first_name_eng+", "+self.short_name
    }
    
    func getFullName() -> String{
        if(self.short_name != nil){
            return self.last_name_eng+" "+self.first_name_eng+", "+self.short_name
        }else{
            return self.last_name_eng+" "+self.first_name_eng
        }
        
    }
    
    func getNameWithShortName() -> String{
        return self.last_name_eng+" "+self.first_name_eng+", "+self.short_name
    }
}
