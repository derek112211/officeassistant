//
//  ReportLeaveModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 9/10/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ReportLeaveModel: NSObject {
    var id:String!
    var start_date:String!
    var end_date:String!
    var start_time:String!
    var end_time:String!
    var leave_type:String!
    var approve_remark:String!
    
    init(data:JSON) {
        if let id = data["id"].string {
            self.id = id
        }
        if let start_date = data["start_date"].string {
            self.start_date = start_date
        }
        if let end_date = data["end_date"].string {
            self.end_date = end_date
        }
        if let start_time = data["start_time"].string {
            self.start_time = start_time
        }
        if let end_time = data["end_time"].string {
            self.end_time = end_time
        }
        if let leave_type = data["leave_type"].string {
            self.leave_type = leave_type
        }
        if let approve_remark = data["remark"].string {
            self.approve_remark = approve_remark
        }
    }
    
    override init() {
        
    }
    
    func getFullDate() -> String{
        let fullDate = start_date+" "+start_time+" - "+end_date+" "+end_time
        return fullDate
    }
    
    
}
