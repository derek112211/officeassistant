//
//  ReportDetailsModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 17/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ReportDetailsModel: NSObject {
    var user_staff_id:String!
    var working_date:String!
    var working_time_start:String!
    var working_time_end:String!
    var attendance_start:String!
    var attendance_end:String!
    var staff_schedule_item_id:String!
    var leave_id:String! // 0 is not leave
    var shop_id:String!
    
    init(data:JSON) {
        if let user_staff_id = data["user_staff_id"].string {
            self.user_staff_id = user_staff_id
        }
        if let shop_id = data["shop_id"].string {
            self.shop_id = shop_id
        }
        if let working_date = data["working_date"].string {
            self.working_date = working_date
        }
        if let working_time_start = data["working_time_start"].string {
            self.working_time_start = working_time_start
        }
        if let working_time_end = data["working_time_end"].string {
            self.working_time_end = working_time_end
        }else{
            self.working_time_end = ""
        }
        
        if let attendance_start = data["attendance_start"].string {
            self.attendance_start = attendance_start
        }else{
            self.attendance_start = " - "
        }
        
        if let attendance_end = data["attendance_end"].string {
            self.attendance_end = attendance_end
        }else{
            self.attendance_end = " - "
        }
        
        if let leave_id = data["leave_id"].string {
            self.leave_id = leave_id
        }
        
        if let staff_schedule_item_id = data["staff_schedule_item_id"].string {
            self.staff_schedule_item_id = staff_schedule_item_id
        }
    }
    
    override init() {
        
    }

    
}
