//
//  ReportCheckinModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 18/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ReportCheckinDetailModel:NSObject {
    var staff_id:String!
    var shop_id:String!
    var type:String! //1 = GPS , 2 = QRCode
    var longitude:String!
    var latitude:String!
    var img:String!
    var time:String!
    var address:String!
    var shop_name:String!
    
    init(data:JSON) {
        if let staff_id = data["staff_id"].string {
            self.staff_id = staff_id
        }
        if let shop_id = data["shop_id"].string {
            self.shop_id = shop_id
        }
        if let type = data["type"].string {
            self.type = type
            if(type == "1"){
                if let longitude = data["longitude"].string {
                    self.longitude = longitude
                }
                if let latitude = data["latitude"].string {
                    self.latitude = latitude
                }
                if let img = data["img"].string {
                    self.img = img
                }
                if let address = data["address"].string {
                    self.address = address
                }
            }else if(type == "2"){
                if let img = data["img"].string {
                    self.img = img
                }
                if let address = data["shop_address"].string {
                    self.address = address
                }
                if let shop_name = data["shop_name"].string {
                    self.shop_name = shop_name
                }
            }
        }
        if let time = data["timestamp"].string {
            let temp = time.components(separatedBy: " ")
            self.time = temp[1]
        }

        
    }
    
}

class ReportCheckinModel:NSObject {
    var staff_id:String!
    var shop_id:String!
    var year:Int!
    var month:Int!
    var day:Int!
    var count:Int!
    var type:String! //1 = GPS , 2 = QRCode
    
    init(data:JSON) {
        if let staff_id = data["staff_id"].string {
            self.staff_id = staff_id
        }
        if let shop_id = data["shop_id"].string {
            self.shop_id = shop_id
        }
        if let type = data["type"].string {
            self.type = type
        }
            
        if let year = data["year"].string {
            self.year = Int(year)
        }
        if let month = data["month"].string {
            self.month = Int(month)
        }
        if let day = data["day"].string {
            self.day = Int(day)
        }
        if let count = data["count"].string {
            self.count = Int(count)
        }else{
            self.count = 0
        }
        
    }
    
    init(year:Int,month:Int,day:Int,count:Int){
        self.year = year
        self.month = month
        self.day = day
        self.count = count
    }
}
