//
//  ReportUserModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 17/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ReportUserModel: NSObject {
    var last_name_eng:String!
    var first_name_eng:String!
    var id:String!
    var user_name:String!
    var shop_name:String!
    var home_shop_id:String!
    
        init(data:JSON) {
            if let last_name_eng = data["last_name_eng"].string {
                self.last_name_eng = last_name_eng
            }
            if let first_name_eng = data["first_name_eng"].string {
                self.first_name_eng = first_name_eng
            }
            if let id = data["id"].string {
                self.id = id
            }
            if let user_name = data["user_name"].string {
                self.user_name = user_name
            }
            if let shop_name = data["shop_name"].string {
                self.shop_name = shop_name
            }
            if let home_shop_id = data["home_shop_id"].string {
                self.home_shop_id = home_shop_id
            }
        }
    
    func getFullNameWithUserName() -> String{
        let fullname = self.last_name_eng+" "+self.first_name_eng
        return fullname+"("+self.user_name+")"
    }
    
}
