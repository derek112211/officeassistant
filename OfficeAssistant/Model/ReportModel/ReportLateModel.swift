//
//  ReportLateModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 18/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ReportLateModel: NSObject {
    var user_staff_id:String!
    var working_date:String!
    var working_time_start:String!
    var attendance_start:String!
    var staff_schedule_item_id:String! //0 = dayoff
    var day:Int!
    var lateInMins:Int!
    var title:String!
    var total:Int!
    
    init(data:JSON) {
        if let user_staff_id = data["user_staff_id"].string {
            self.user_staff_id = user_staff_id
        }
        if let working_date = data["working_date"].string {
            self.working_date = working_date
//            print(working_date.suffix(2))
//            self.day = Int(working_date.suffix(2))
        }
        if let working_time_start = data["working_time_start"].string {
            self.working_time_start = working_time_start
        }else{
            self.working_time_start = "00:00:00"
        }
        if let attendance_start = data["attendance_start"].string {
            self.attendance_start = attendance_start
        }else{
            self.attendance_start = "00:00:00"
        }
        if let staff_schedule_item_id = data["staff_schedule_item_id"].string {
            self.staff_schedule_item_id = staff_schedule_item_id
        }
        if let lateInMins = data["lateInMins"].int {
            self.lateInMins = lateInMins
        }
        if let day = data["day"].int {
            self.day = day
        }
        
        //difference cal
//        let formatter = DateFormatter()
//        formatter.dateFormat = "HH:mm:ss"
//        let work_start = formatter.date(from: self.working_time_start)
//        let attend_start = formatter.date(from: self.attendance_start)
//        let calendar = Calendar.current
//        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: work_start!)
//        let nowComponents = calendar.dateComponents([.hour, .minute, .second], from: attend_start!)
//
//        let difference = calendar.dateComponents([.minute], from: timeComponents, to: nowComponents).minute!
//
//        if(difference<0){
//            self.lateInMins = 0
//        }else{
//            self.lateInMins = difference
//        }
        
    }
    
//    init(day:Int){
//        self.day = day
//        self.lateInMins = 0
//        self.working_time_start = "00:00:00"
//        self.attendance_start = "00:00:00"
//
//        //difference cal
//        let formatter = DateFormatter()
//        formatter.dateFormat = "HH:mm:ss"
//        let work_start = formatter.date(from: working_time_start)
//        let attend_start = formatter.date(from: attendance_start)
//        let calendar = Calendar.current
//        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: work_start!)
//        let nowComponents = calendar.dateComponents([.hour, .minute, .second], from: attend_start!)
//
//        let difference = calendar.dateComponents([.minute], from: timeComponents, to: nowComponents).minute!
//
//        if(difference<0){
//            self.lateInMins = 0
//        }else{
//            self.lateInMins = difference
//        }
//    }
    
    init(title:String,total:Int){
        self.title = title
        self.total = total
    }
    
}
