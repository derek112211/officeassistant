//
//  LeaveListModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 16/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftIcons

class IconListModel: NSObject {
    var name:String = ""
    var icon:FontType
    var color:UIColor
//        init(data:JSON) {
//            if let id = data["id"].string {
//                self.id = id
//            }
//            if let date = data["date"].string {
//                self.date = date
//            }
//            if let name = data["name"].string {
//                self.name = name
//            }
//            if let my_description = data["description"].string {
//                self.my_description = my_description
//            }
//            if let content = data["content"].string {
//                self.content = content
//            }
//        }
    init(name:String,icon:FontType,color:UIColor){
        self.name = name
        self.icon = icon
        self.color = color
    }
}

class LeaveUserModel: NSObject,Codable {
    //user info
    var id:String!
    var application_no:String!
    var staff_id:String!
    var shop_id:String!
    var first_name_eng:String!
    var last_name_eng:String!
    var reason:String!
    var create_time:String!
    var shop_name:String!
    var short_name:String!
    
    //leave info
    var leave_from:String!
    var leave_time_from:String!
    var leave_to:String!
    var leave_time_to:String!
    var leave_type:String!
    var leave_days:String!
    var leave_hours:String!
    var leave_type_name:String!
    var leave_department_approve_status:String!
    var leave_hr_approve_status:String!
    
    
    init(data:JSON) {
        if let id = data["id"].string {
            self.id = id
        }
        if let application_no = data["application_no"].string {
            self.application_no = application_no
        }
        if let staff_id = data["user_staff_id"].string {
            self.staff_id = staff_id
        }
        if let shop_id = data["shop_id"].string {
            self.shop_id = shop_id
        }
        if let first_name_eng = data["first_name_eng"].string {
            self.first_name_eng = first_name_eng
        }
        if let last_name_eng = data["last_name_eng"].string {
            self.last_name_eng = last_name_eng
        }
        if let reason = data["reason"].string {
            self.reason = reason
        }
        if let create_time = data["create_time"].string {
            self.create_time = create_time
        }
        if let leave_from = data["leave_from"].string {
            self.leave_from = leave_from
        }
        if let leave_time_from = data["leave_time_from"].string {
            self.leave_time_from = leave_time_from
        }
        if let leave_to = data["leave_to"].string {
            self.leave_to = leave_to
        }
        if let leave_time_to = data["leave_time_to"].string {
            self.leave_time_to = leave_time_to
        }
        if let leave_type = data["leave_type"].string {
            self.leave_type = leave_type
        }
        if let leave_days = data["leave_days"].string {
            self.leave_days = leave_days
        }
        if let leave_hours = data["leave_hours"].string {
            self.leave_hours = leave_hours
        }
        if let leave_type_name = data["leave_type_name"].string {
            self.leave_type_name = leave_type_name
        }
        if let shop_name = data["shop_name"].string {
            self.shop_name = shop_name
        }
        if let short_name = data["short_name"].string {
            self.short_name = short_name
        }
        if let leave_department_approve_status = data["department_approve_status"].string {
            self.leave_department_approve_status = leave_department_approve_status
        }
        if let leave_hr_approve_status = data["hr_approve_status"].string {
            self.leave_hr_approve_status = leave_hr_approve_status
        }
    }
    
    override init(){
    }
    
    func getFullName() -> String{
        if(last_name_eng != nil && first_name_eng != nil){
            if(short_name != nil){
                return "\(self.last_name_eng!) \(self.first_name_eng!), \(self.short_name!)"
            }else{
                return "\(self.last_name_eng!) \(self.first_name_eng!)"
            }
        }
        return ""
    }

}
