//
//  ShopLoginModel.swift
//  EM_Ios_new
//
//  Created by SocialMind on 11/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//


import UIKit
import SwiftyJSON

class ShopLoginModel: NSObject {
    var id:String!
    var shop_no:String!
    var name:String!
    var address:String!
    var phone:String!
    var ip_address:String!
    var device_code:String!
    var login_type:String!
    
    override init() {
        super.init()
    }
    
    init(userInfo:JSON) {
        let data = userInfo
        
        if let id = data["id"].string {
            self.id = id
        }
        if let shop_no = data["shop_no"].string {
            self.shop_no = shop_no
        }
        if let name = data["name"].string {
            self.name = name
        }
        if let address = data["address"].string {
            self.address = address
        }
        if let phone = data["phone"].string {
            self.phone = phone
        }
        if let ip_address = data["ip_address"].string {
            self.ip_address = ip_address
        }
        if let device_code = data["device_code"].string {
            self.device_code = device_code
        }
    }
    
    func save_profile() {
        DataHelper.save(obj: self.id, key: "shop_id")
        DataHelper.save(obj: self.shop_no, key: "shop_no")
        DataHelper.save(obj: self.name, key: "shop_name")
        DataHelper.save(obj: self.address, key: "shop_address")
        DataHelper.save(obj: self.phone, key: "shop_phone")
        DataHelper.save(obj: self.ip_address, key: "shop_ip_address")
        DataHelper.save(obj: self.device_code, key: "device_code")
        DataHelper.save(obj: self.device_code, key: "shop_phone")
    }
    
    func save() {
        
        DataHelper.save(obj: self.id, key: "shop_id")
        DataHelper.save(obj: self.shop_no, key: "shop_no")
        DataHelper.save(obj: self.name, key: "shop_name")
        DataHelper.save(obj: self.address, key: "shop_address")
        DataHelper.save(obj: self.phone, key: "shop_phone")
        DataHelper.save(obj: self.ip_address, key: "shop_ip_address")
        DataHelper.save(obj: self.device_code, key: "shop_device_code")
        DataHelper.save(obj: "host", key:"login_type")
    }
    
    func clear() {
        let mirror = Mirror(reflecting: self)
        for (name, value) in mirror.children {
            guard let name = name else { continue }
            DataHelper.save(obj: "", key: name)
        }
    }
    
    class func getOne() -> ShopLoginModel {
        let one = ShopLoginModel()
        
        if let f = DataHelper.get(key: "shop_id") as? String {
            one.id = f
        }
        if let f = DataHelper.get(key: "shop_no") as? String {
            one.shop_no = f
        }
        if let f = DataHelper.get(key: "shop_name") as? String {
            one.name = f
        }
        if let f = DataHelper.get(key: "shop_address") as? String {
            one.address = f
        }
        if let f = DataHelper.get(key: "shop_phone") as? String {
            one.phone = f
        }
        if let f = DataHelper.get(key: "shop_ip_address") as? String {
            one.ip_address = f
        }
        if let f = DataHelper.get(key: "shop_device_code") as? String {
            one.device_code = f
        }
        if let f = DataHelper.get(key: "login_type") as? String {
            one.login_type = f
        }
        
        return one
    }
    
    class func isLoggedIn() -> Bool {
        if let device = DataHelper.get(key: "shop_device_code") as? String {
            if !device.isEmpty {
                return true
            }
        }
        if let id = DataHelper.get(key: "shop_id") as? String {
            if !id.isEmpty {
                return true
            }
        }
        return false
    }
    
    class func isStaff() -> Bool {
        let login_type = StaffLoginModel.getOne().login_type
        if login_type.isEmpty {
            return false
        }
        if(login_type == "member"){
            return false
        }
        return true
    }
}
