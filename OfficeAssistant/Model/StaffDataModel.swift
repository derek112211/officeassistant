//
//  StaffDataModel.swift
//  ExpertMedical
//
//  Created by SocialMind on 25/6/2019.
//  Copyright © 2019 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class StaffDataModel: NSObject {
    var id:String = ""
    var date:String = ""
    var name:String = ""
    var my_description:String = ""
    
//    init(data:JSON) {
//        if let id = data["id"].string {
//            self.id = id
//        }
//        if let date = data["date"].string {
//            self.date = date
//        }
//        if let name = data["name"].string {
//            self.name = name
//        }
//        if let my_description = data["description"].string {
//            self.my_description = my_description
//        }
//        if let content = data["content"].string {
//            self.content = content
//        }
//    }
    init(id: String,date: String, name:String, description: String){
        self.id = id
        self.date = date
        self.name = name
        self.my_description = description
    }
    
    
}
