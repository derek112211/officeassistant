//
//  Date.swift
//  EM_Ios_new
//
//  Created by SocialMind on 18/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//
import UIKit

extension Date {
    
    func getDaysInMonth() -> Int{
        let calendar = Calendar.current
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        return numDays
    }
    
}
