//
//  UITextField+Extension.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 08/05/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

extension UITextField {
    func disableAuto() -> UITextField {
        self.autocorrectionType = .no
        self.autocapitalizationType = .none
        self.spellCheckingType = .no
        return self
    }
}

extension UITextView {
    func disableAuto() -> UITextView {
        self.autocorrectionType = .no
        self.autocapitalizationType = .none
        self.spellCheckingType = .no
        return self
    }
}



