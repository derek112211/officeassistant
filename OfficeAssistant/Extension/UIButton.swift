//
//  UIButton.swift
//  Mobijuce
//
//  Created by VTLMACOS on 10/8/2017.
//  Copyright © 2017 VTLMACOS. All rights reserved.
//

import UIKit

extension UIButton {
    
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var action :(() -> Void)?
        }
        if action != nil {
            __.action = action
        } else {
            __.action?()
        }
    }
    
    @objc private func triggerActionHandleBlock() {
        self.actionHandleBlock()
    }
    
    func actionHandle(controlEvents control :UIControlEvents, ForAction action:@escaping () -> Void) {
        self.actionHandleBlock(action: action)
        self.addTarget(self, action: #selector(UIButton.triggerActionHandleBlock), for: control)
    }
    
    convenience init(fontSize: CGFloat, color: UIColor, str: String) {
        self.init(type: .custom)
//        self.titleLabel?.font = UIFont(name: (self.titleLabel?.font.fontName)!, size: fontSize)
        self.setTitleColor(color, for: UIControlState())
        self.setTitle(str, for: UIControlState())
    }
    
    convenience init(fontSize: CGFloat, txColor: UIColor, bgColor: UIColor, txt: String) {
        self.init(type: .custom)
//        self.titleLabel?.font = UIFont(name: (self.titleLabel?.font.fontName)!, size: fontSize)
        self.setTitleColor(txColor, for: UIControlState())
        self.backgroundColor = bgColor
        self.setTitle(txt, for: UIControlState())
    }
    
}
