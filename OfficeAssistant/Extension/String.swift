//
//  String.swift
//  cosybuy
//
//  Created by vtl on 4/5/2016.
//  Copyright © 2016 PinkBox. All rights reserved.
//

import UIKit

extension String{
    
    func localized() -> String {
        var lang = DataHelper.get(key: "Language") as? String
        if lang == nil { lang = "zh-Hant" }
        if let path = Bundle.main.path(forResource: lang!, ofType:"lproj") {
            let languageBundle = Bundle(path: path)
            if let str = languageBundle?.localizedString(forKey: self, value: "", table: nil) {
                return str
            }
        }
        return self
    }
    
    func URLQueryAllowed() -> String {
        if let str = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed) {
            return str
        }
        return self
    }
    
    func isPatternMatch(pattern:String) -> Bool {
        let PHONE_REGEX = pattern
        let predic = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  predic.evaluate(with: self)
        return result
    }
    
    func toURL() -> URL? {
        if let url = URL(string: self) {
            return url
        }else{
            return URL(string: self.URLQueryAllowed())
        }
    }

    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func isKeyPresentInUserDefaults() -> Bool {
        return UserDefaults.standard.object(forKey: self) != nil
    }
    
    func isEmptyStr() -> Bool {
        if self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).length == 0 { return true }
        return false
    }
    
    func imageView() -> UIImageView {
        let img = UIImage(named: self)
        return UIImageView(image: img)
    }
    
    func imageView(resizedWidth width:CGFloat) -> UIImageView {
        let img = UIImage(named: self)
        return UIImageView(image: img?.resizeWith(width: width))
    }
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension String {
    func convertHtml(withFont: UIFont? = UIFont.systemFont(ofSize: 10), align: NSTextAlignment = .left) -> NSAttributedString {
        if let data = self.data(using: .utf8, allowLossyConversion: true),
            let attributedText = try? NSAttributedString(
                data: data,
                options: [.documentType: NSAttributedString.DocumentType.html,
                          .characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil
            ) {
            let style = NSMutableParagraphStyle()
            style.alignment = align
            
            let fullRange = NSRange(location: 0, length: attributedText.length)
            let mutableAttributeText = NSMutableAttributedString(attributedString: attributedText)
            
            if let font = withFont {
                mutableAttributeText.addAttribute(.paragraphStyle, value: style, range: fullRange)
                mutableAttributeText.enumerateAttribute(.font, in: fullRange, options: .longestEffectiveRangeNotRequired, using: { attribute, range, _ in
                    if let attributeFont = attribute as? UIFont {
                        let traits: UIFontDescriptorSymbolicTraits = attributeFont.fontDescriptor.symbolicTraits
                        var newDescripter = attributeFont.fontDescriptor.withFamily(font.familyName)
                        if (traits.rawValue & UIFontDescriptorSymbolicTraits.traitBold.rawValue) != 0 {
                            newDescripter = newDescripter.withSymbolicTraits(.traitBold)!
                        }
                        if (traits.rawValue & UIFontDescriptorSymbolicTraits.traitItalic.rawValue) != 0 {
                            newDescripter = newDescripter.withSymbolicTraits(.traitItalic)!
                        }
                        //let scaledFont = UIFont(descriptor: newDescripter, size: attributeFont.pointSize)
                        let scaledFont = UIFont(descriptor: newDescripter, size: 14)
                        mutableAttributeText.addAttribute(.font, value: scaledFont, range: range)
                    }
                })
            }
            
            return mutableAttributeText
        }
        
        return NSAttributedString(string: self)
    }
}

