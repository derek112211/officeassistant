//
//  UILabel.swift
//  Mobijuce
//
//  Created by VTLMACOS on 12/7/2017.
//  Copyright © 2017 VTLMACOS. All rights reserved.
//

import UIKit

extension UILabel {

    convenience init(font: UIFont, color: UIColor) {
        self.init()
        self.font = font
        self.textColor = color
    }

    convenience init(fontSize: CGFloat, color: UIColor) {
        self.init()
        self.font = self.font.withSize(fontSize)
        //self.font = UIFont(name: "Roboto-Regular", size: fontSize)
        self.textColor = color
    }
    
    convenience init(fontSize: CGFloat, color: UIColor, str: String) {
        self.init()
        self.font = self.font.withSize(fontSize)
        //self.font = UIFont(name: "Roboto-Regular", size: fontSize)
        self.textColor = color
        self.text = str
    }
    
    convenience init(fontSize: CGFloat, color: UIColor, str: String, bgColor: UIColor) {
        self.init()
        self.font = self.font.withSize(fontSize)
        //self.font = UIFont(name: "Roboto-Regular", size: fontSize)
        self.textColor = color
        self.text = str
        self.backgroundColor = bgColor
    }
    
    convenience init(color: UIColor) {
        self.init()
        self.textColor = color
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
