//
//  UIFont.swift
//  EM_Ios_new
//
//  Created by SocialMind on 29/10/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import UIKit

extension UIFont {
    func forSize(_ pointSize: CGFloat) -> UIFont {
        if !fontName.contains(".SF"), let font = UIFont(name: fontName, size: pointSize) {
            return font
        }

        return UIFont.systemFont(ofSize: UIFont.systemFontSize)
    }
}
