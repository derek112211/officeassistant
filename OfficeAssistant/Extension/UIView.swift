//
//  UIView.swift
//  eKoool
//
//  Created by vtl on 4/5/2016.
//  Copyright © 2016 ekoool. All rights reserved.
//

import UIKit

extension UIScrollView {
    /// Sets content offset to the top.
    func resetScrollPositionToTop() {
        self.contentOffset = CGPoint(x: -contentInset.left, y: -contentInset.top)
    }
}

extension UIView {
    
    class func loadNib<T: UIView>(_ viewType: T.Type) -> T {
        let className = String.className(viewType)
        return Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)!.first as! T
    }
    
    class func loadNib() -> Self {
        return loadNib(self)
    }

    func randomBorder() {
        //self.layer.borderColor = UIColor.random().cgColor
        //self.layer.borderWidth = 3
    }
    func randomBG() {
        //self.backgroundColor = UIColor.random()
    }
    func topY() -> CGFloat{
        return self.frame.origin.y
    }
    func topY(posY:CGFloat) -> Void{
        self.frame = CGRect(x: self.frame.origin.x, y: posY, width: self.frame.size.width, height: self.frame.size.height)
    }
    func bottomY() -> CGFloat{
        return self.frame.origin.y+self.frame.size.height
    }
    func bottomY(posY:CGFloat) -> Void{
        self.frame = CGRect(x: self.frame.origin.x, y: posY-self.frame.size.height, width: self.frame.size.width, height: self.frame.size.height)
    }
    func leadingX() -> CGFloat{
        return self.frame.origin.x
    }
    func leadingX(posX:CGFloat) -> Void{
        self.frame = CGRect(x: posX, y: self.frame.origin.y, width: self.frame.size.width, height: self.frame.size.height)
    }
    func trailingX() -> CGFloat{
        return self.frame.origin.x+self.frame.size.width
    }
    func trailingX(posX:CGFloat) -> Void{
        self.frame = CGRect(x: posX-self.frame.size.width, y: self.frame.origin.y, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    func width() -> CGFloat{
        return self.frame.size.width
    }
    func height() -> CGFloat{
        return self.frame.size.height
    }
    func width(w:CGFloat) -> Void{
        self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: w, height: self.frame.size.height)
    }
    func height(h:CGFloat) -> Void{
        self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: h)
    }
    func centerX(posX:CGFloat) -> Void{
        self.center = CGPoint(x: posX, y: self.center.y)
    }
    func centerY(posY:CGFloat) -> Void{
        self.center = CGPoint(x: self.center.x, y: posY)
    }
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func aspectFitWidth(width: CGFloat)
    {
        let ratio = self.frame.size.height/self.frame.size.width
        self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: width, height: width*ratio)
    }
    
    func rounded(withRadius radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func makeRoundedAndShadow() {
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = KEY_RADIUS
        
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 4.0
        let shadowRect: CGRect = self.bounds.insetBy(dx: 0, dy: 4)
        self.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
    }
    
    func roundCorner(corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.cgPath;
        self.layer.mask = maskLayer;
    }
    
}

extension UITableView {
    
    func scrollToBottom(animated:Bool=false) {
        let rows = self.numberOfRows(inSection: 0)
        
        let indexPath = IndexPath(row: rows - 1, section: 0)
        self.scrollToRow(at: indexPath, at: .top, animated: animated)
    }
}


