//
//  MyDatePicker.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 20/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import DateTimePicker

class MonthAndDayModel {
    var value:String = ""
    var type:InputFieldType = .TimeFrom
    var dayIndex:Int = 0
    var monthIndex:Int = 0
    var yearIndex:Int = 0
    var hourIndex:Int = 0
    var minsIndex:Int = 0
}

class NEW_DateTimePicker {

    var result = Date()
    var picker:DateTimePicker
    
    init(selectedDate:Date){
//        super.init(frame : frame)
//        picker = DateTimePicker.create()
        var min = Date()
        var max = Date().addingTimeInterval(60 * 60 * 24 * 365)
        
        picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        picker.selectedDate = selectedDate
        picker.frame = CGRect(x: 0, y: 100, width: picker.frame.size.width, height: picker.frame.size.height)
        picker.locale = Locale(identifier: "en_US_POSIX")
    }
    
    class func addTo(selectedDate:Date,didCompletion:@escaping ((Date)->())){
        let dpv = NEW_DateTimePicker(selectedDate: selectedDate)
        dpv.picker.completionHandler = didCompletion
        dpv.picker.show()
    }
    
    
//    @objc func close(_ sender: UITapGestureRecognizer) {
//        self.removeFromSuperview()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
}

class MyDatePicker: UIView, UIPickerViewDelegate, UIPickerViewDataSource {
    var year:Int!
    var month:Int!
    var day:Int!
    var years:[Int]!
    var mins:[Int]!
    var itemAtDefaultPosition: String?
    
    var didSelect:((MonthAndDayModel)->())!
    var picker = UIPickerView()
    
    class func addTo(view:UIView,model:MonthAndDayModel, didCompletion:@escaping ((MonthAndDayModel)->())){
        let dpv = MyDatePicker()
        dpv.setDefaultRow(model: model)
        dpv.didSelect = didCompletion
        view.addSubview(dpv)
        dpv.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        
    }
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        
        self.year = Calendar.current.component(.year, from: Date())
        self.month = Calendar.current.component(.month, from: Date())
        self.day = Calendar.current.component(.day, from: Date())
        self.years = (year...year+5).map { $0 }
        self.mins = (0...59).map { $0 }
        
        let dimLayer = UIView()
        dimLayer.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.addSubview(dimLayer)
        dimLayer.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.close(_:)))
        dimLayer.addGestureRecognizer(tap)
        dimLayer.isUserInteractionEnabled = true
        
        self.addSubview(picker)
        picker.backgroundColor = UIColor.white
        picker.snp.makeConstraints({ (make)->Void in
            make.leading.bottom.trailing.equalToSuperview()
            make.height.equalTo(self.snp.width)
        })
        picker.delegate = self
        picker.dataSource = self
        //init selected row
//        picker.selectRow(month-1, inComponent: 1, animated: false)
//        picker.selectRow(day-1, inComponent: 0, animated: false)
        
        let toobar = UIToolbar()
        self.addSubview(toobar)
        toobar.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(44)
            make.bottom.equalTo(picker.snp.top)
        })
        toobar.backgroundColor = UIColor.white
        toobar.randomBorder()
        
        let doneBtn = UIButton(fontSize: 18, color: UIColor.darkText, str: "完成")
        self.addSubview(doneBtn)
        doneBtn.snp.makeConstraints({ (make)->Void in
            make.top.height.equalTo(toobar)
            make.trailing.equalTo(toobar).offset(-10)
        })
        doneBtn.randomBorder()
        doneBtn.addTarget(self, action: #selector(DayOfBirthPicker.btnClick(_:)), for: .touchUpInside)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 5
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 1 {
            //second component
            return Calendar.current.shortMonthSymbols.count
        }else if component == 2 {
            //year component
            return years.count
        }else if component == 3{
            //hour 00-23
            return 24
        }else if component == 4{
            //min 00...59
            return 60
        }
//        else if component == 5{
//            //sec 00-59
//            return 60
//        }
        //day
        return 31
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 1 {
            return  Calendar.current.shortMonthSymbols[row]
        }else if component == 2{
            return "\(years[row])"
        }else if component == 3{
            //hour
            let hours = (0...23).map { $0 }
            if(hours[row]<10){
                return "0\(hours[row])"
            }else{
                return "\(hours[row])"
            }
        }else if component == 4{
            //min
//            let mins = (0...59).map { $0 }
            
            if(mins[row]<10){
                return "0\(mins[row])"
            }else{
                return "\(mins[row])"
            }
        }
//        else if component == 5{
//            //sec
//            let secs = (0...59).map { $0 }
//            if(secs[row]<10){
//                return "0\(secs[row])"
//            }else{
//                return "\(secs[row])"
//            }
//        }
        //day
        return "\(row+1)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
    }
    
    func setDefaultRow(model:MonthAndDayModel){
        self.picker.selectRow(model.dayIndex, inComponent: 0, animated: false)
        self.picker.selectRow(model.monthIndex, inComponent: 1, animated: false)
        self.picker.selectRow(model.yearIndex, inComponent: 2, animated: false)
        self.picker.selectRow(model.hourIndex, inComponent: 3, animated: false)
        self.picker.selectRow(model.minsIndex, inComponent: 4, animated: false)
    }
    
    @objc func btnClick(_ sender: UIButton) {
        self.removeFromSuperview()
        
        let day = picker.selectedRow(inComponent: 0)
        let month = picker.selectedRow(inComponent: 1)
        let year = years[picker.selectedRow(inComponent: 2)]
        let hour = picker.selectedRow(inComponent: 3)
        let min = mins[picker.selectedRow(inComponent: 4)]
//        let sec = picker.selectedRow(inComponent: 5)
        let model = MonthAndDayModel()
        model.value = "\(String(format: "%04d", year))-\(String(format: "%02d", month+1))-\(String(format: "%02d", day+1)) \(String(format: "%02d", hour)):\(String(format: "%02d", min))"
        model.dayIndex = day
        model.monthIndex = month
        model.yearIndex = picker.selectedRow(inComponent: 2)
        model.hourIndex = hour
        model.minsIndex = picker.selectedRow(inComponent: 4)
        self.didSelect(model)
    }
    
    @objc func close(_ sender: UITapGestureRecognizer) {
        self.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
