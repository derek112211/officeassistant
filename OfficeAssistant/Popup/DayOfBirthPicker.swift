//
//  DayOfBirthPicker.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 16/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

class UserDayOfBirthModel {
    var date:Date!
    init(date:Date) {
        self.date = date
    }
    func getDay() -> String {
        let day = Calendar.current.component(.day, from: date)
        return "\(day)"
    }
    func getMonth() -> String {
        let month = Calendar.current.component(.month, from: date)
        return "\(month)"
    }
    func getYear() -> String {
        let year = Calendar.current.component(.year, from: date)
        return "\(year)"
    }
    func getString() -> String {
        return getYear() + "-" + getMonth() + "-" + getDay()
    }
}

class DayOfBirthPicker: UIView {

    class func addTo(view:UIView, didCompletion:@escaping ((UserDayOfBirthModel)->())){
        let dpv = DayOfBirthPicker()
        dpv.didSelectDayOfBirth = didCompletion
        view.addSubview(dpv)
        dpv.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
    }
    
    var didSelectDayOfBirth:((UserDayOfBirthModel)->())!
    var datePicker:UIDatePicker!
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        
        let dimLayer = UIView()
        dimLayer.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.addSubview(dimLayer)
        dimLayer.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        
        var tap = UITapGestureRecognizer(target: self, action: #selector(self.close(_:)))
        dimLayer.addGestureRecognizer(tap)
        dimLayer.isUserInteractionEnabled = true
        
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: "zh-HK")
        datePicker.maximumDate = Date()
        self.addSubview(datePicker)
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged(sender:)), for: .valueChanged)
        
        datePicker.backgroundColor = UIColor.white
        datePicker.snp.makeConstraints({ (make)->Void in
            make.leading.bottom.trailing.equalToSuperview()
            make.height.equalTo(200)
        })
        
//        if let result = EditedInfoFieldHelper.dayOfBirth {
//            datePicker.date = result.date
//        }
        
        let toobar = UIToolbar()
        self.addSubview(toobar)
        toobar.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(44)
            make.bottom.equalTo(datePicker.snp.top)
        })
        toobar.backgroundColor = UIColor.white
        toobar.randomBorder()
        
        let doneBtn = UIButton(fontSize: 18, color: UIColor.darkText, str: "完成")
        self.addSubview(doneBtn)
        doneBtn.snp.makeConstraints({ (make)->Void in
            make.top.height.equalTo(toobar)
            make.trailing.equalTo(toobar).offset(-10)
        })
        doneBtn.randomBorder()
        doneBtn.addTarget(self, action: #selector(DayOfBirthPicker.btnClick(_:)), for: .touchUpInside)
        
    }
    
    @objc func btnClick(_ sender: UIButton) {
        self.removeFromSuperview()

        self.didSelectDayOfBirth(UserDayOfBirthModel(date: datePicker.date))
    }
    
    @objc func close(_ sender: UITapGestureRecognizer) {
        self.removeFromSuperview()
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        print(sender.date)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
