//
//  LeavePicker.swift
//  EM_Ios_new
//
//  Created by SocialMind on 7/8/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//


import UIKit

class LeaveModel {
    var name:String
    var id:String
    init(name:String,id:String){
        self.name = name
        self.id = id
    }

}

class LeavePicker: UIView, UIPickerViewDelegate, UIPickerViewDataSource {
    var didSelectLeave:((LeaveModel)->())!
    var picker = UIPickerView()
    var leaveNames:Array<LeaveModel>!
    
    class func addTo(view:UIView, leaves:Array<LeaveModel>, didCompletion: @escaping ((LeaveModel)->())){
        let dpv = LeavePicker()
        dpv.leaveNames = leaves
        dpv.didSelectLeave = didCompletion
        view.addSubview(dpv)
        dpv.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
    }
    
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        
        
        let dimLayer = UIView()
        dimLayer.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.addSubview(dimLayer)
        dimLayer.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        
        var tap = UITapGestureRecognizer(target: self, action: #selector(self.close(_:)))
        dimLayer.addGestureRecognizer(tap)
        dimLayer.isUserInteractionEnabled = true
        
        self.addSubview(picker)
        picker.backgroundColor = UIColor.white
        picker.snp.makeConstraints({ (make)->Void in
            make.leading.bottom.trailing.equalToSuperview()
            make.height.equalTo(self.snp.width)
        })
        picker.delegate = self
        
        //        if let result = EditedInfoFieldHelper.dayOfBirth {
        //            datePicker.date = result.date
        //        }
        
        let toobar = UIToolbar()
        self.addSubview(toobar)
        toobar.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(44)
            make.bottom.equalTo(picker.snp.top)
        })
        toobar.backgroundColor = UIColor.white
        toobar.randomBorder()
        
        let doneBtn = UIButton(fontSize: 18, color: UIColor.darkText, str: "完成")
        self.addSubview(doneBtn)
        doneBtn.snp.makeConstraints({ (make)->Void in
            make.top.height.equalTo(toobar)
            make.trailing.equalTo(toobar).offset(-10)
        })
        doneBtn.randomBorder()
        doneBtn.addTarget(self, action: #selector(LeavePicker.btnClick(_:)), for: .touchUpInside)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Int(leaveNames!.count)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return leaveNames[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
    }
    
    
    
    @objc func btnClick(_ sender: UIButton) {
        self.removeFromSuperview()
        
        let index1 = picker.selectedRow(inComponent: 0)
        print(index1)
        let model = leaveNames[index1]
        self.didSelectLeave(model)
    }
    
    @objc func close(_ sender: UITapGestureRecognizer) {
        self.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
