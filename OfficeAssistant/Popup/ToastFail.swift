//
//  ToastFail.swift
//  EM_Ios_new
//
//  Created by SocialMind on 24/1/2020.
//  Copyright © 2020 SocialMind. All rights reserved.
//

import Foundation

import UIKit

class ToastViewFail: UIView {

    override init (frame : CGRect) {
        super.init(frame : frame)
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = CGFloat(6)
        self.layer.masksToBounds = true
        self.frame = CGRect(x: 0, y: 0, width: 100, height: 40)

        let lb = UILabel()
        lb.text = "Check-in Fail, Please re-try".localized()
        lb.textColor = UIColor.darkText
        lb.sizeToFit()
        lb.width(w: lb.width()+20)
        lb.height(h: 40)
        lb.textAlignment = .center
        self.addSubview(lb)

        let indicator = UIActivityIndicatorView()
        indicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        indicator.activityIndicatorViewStyle  = .gray
        self.addSubview(indicator)
        indicator.leadingX(posX: lb.trailingX())
        indicator.centerY(posY: lb.center.y)
        indicator.startAnimating()

        self.width(w: indicator.trailingX())
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

class ToastPopUpFail: UIView {

    static func addTo(view:UIView) -> ToastPopUpFail
    {
        let v = ToastPopUpFail()
        view.addSubview(v)
        return v
    }
    
    static func addTo(vc:UIViewController) -> ToastPopUpFail
    {
        let v = ToastPopUpFail()
        vc.view.addSubview(v)
        return v
    }
    
    let dimLayer = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRect(x: 0, y: 0, width: sw, height: sh)
        
        dimLayer.frame = CGRect(x: 0, y: 0, width: sw, height: sh)
        dimLayer.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.1)
        self.addSubview(dimLayer)
        
        /* do animation */
        dimLayer.alpha = 0
        
        let lbl = ToastViewFail()
        self.addSubview(lbl)
        lbl.center = self.center

        UIView.animate(withDuration: 0.3, animations: { self.dimLayer.alpha = 1 })
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
