//
//  ViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 8/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    static var me:ViewController!
    let nav = UINavigationController()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        ViewController.me = self
        
        self.view.backgroundColor = UIColor.white
        
        nav.isNavigationBarHidden = true
        self.addChildViewController(nav)
        self.view.addSubview(nav.view)
        nav.view.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        nav.didMove(toParentViewController: self)
        
        if StaffLoginModel.isLoggedIn() || ShopLoginModel.isLoggedIn() {
            APIHelper.profile(completion: {}, fail: { }, needShowInfo: false)
            let vc = MainViewController()
            ViewController.me.nav.viewControllers = [vc]
        }else{
            let vc = LoginViewController()
            ViewController.me.nav.viewControllers = [vc]
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
