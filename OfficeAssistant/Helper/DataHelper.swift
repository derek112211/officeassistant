//
//  DataHelper.swift
//  EM_Ios_new
//
//  Created by SocialMind on 8/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//
import UIKit
//import SwiftLevelDB

class DataHelper: NSObject {
    
    class func save(obj:Any, key:String) {
        let defaults = UserDefaults.standard
        defaults.set(obj, forKey: key)
        defaults.synchronize()
    }
    class func get(key:String) -> (Any?) {
        if let obj = UserDefaults.standard.value(forKey: key) {
            return obj
        }
        return nil
    }
    class func delete(key:String) {
        let defaults = UserDefaults.standard
        defaults.set(nil, forKey: key)
    }
    
    class func token() -> String? {
        let token = DataHelper.get(key: "token") as? String
        return token
    }
    
    
    //    static let database = LevelDB(databaseName: "MobiJuceDB-v04")
    //    class func save(obj:Any, key:String) {
    //        LevelDBHelper.database?.setObject(obj, key: key)
    //    }
    //    class func get(key:String) -> (Any?) {
    //        if let obj = LevelDBHelper.database?.getObject(key) {
    //            return obj
    //        }
    //        return nil
    //    }
    //    class func delete(key:String) {
    //        LevelDBHelper.database?.deleteObject(key)
    //    }
}
