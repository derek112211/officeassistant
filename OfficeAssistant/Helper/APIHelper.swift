//
//  APIHelper.swift
//  CIC
//
//  Created by kelvin.lee(VTL) on 04/01/2018.
//  Copyright © 2018 kelvin.lee(VTL). All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class APIHelper: NSObject {

//    key changes
//    static var SERVER_API:String = "https://em.algotech-hk.com/em_cms/medical_booking_cms/"
    static var STAFF_SERVER_API:String = "http://3.19.217.73/"


    class func debugReq(_ request: Request) {
        let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
                isSim = true
            #endif
            return isSim
        }()
        
        //if isSimulator {
        debugPrint(request)
        //}
    }
    
    class func profile(completion:@escaping (()->()),
                       fail:@escaping (()->()),
                       needShowInfo:Bool=true) {
        print("TYPE GET : " + StaffLoginModel.getOne().login_type)
        
        if(StaffLoginModel.getOne().login_type == "host"){
            //not in use --> may use later
            var parameters = Parameters()
            parameters["shop_id"] = ShopLoginModel.getOne().id
            let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s02_retrieve_shop_profile.php",
                method: .post,
                parameters: parameters).validate().responseJSON { response in
                    switch response.result {
                    case .success:
                        print("SUCCESS")
//                        let json = JSON(response.result.value!)
//                        print(json.debugDescription)
//                        if json["status"] == true {
//
//                            shopLoginModel = ShopLoginModel(userInfo: json["params"])
//                            shopLoginModel!.save_profile()
//
//                            completion()
//                        }else{
//                            if needShowInfo {
//                                showInfo(message: json["message"].string!, didCompletion: {
//                                    fail()
//                                })
//                            }else{
//                                fail()
//                            }
//                            //showError(errorMessage: json["message"].string!)
//                        }
                    case .failure(let error):
                        print("error - \(error.localizedDescription)")
                        showError(errorMessage: error.localizedDescription)
                        fail()
                        logout(completion: {}, fail: {})
                    }
            }
            debugReq(req)
        }else if (StaffLoginModel.getOne().login_type == "staff"){
            var parameters = Parameters()
            parameters["staff_id"] = StaffLoginModel.getOne().id
            let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s03_retrieve_staff_profile.php",
                method: .post,
                parameters: parameters).validate().responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        print(json.debugDescription)
                        if json["status"] == true {
                            
                            staffLoginModel = StaffLoginModel(userInfo: json["params"])
                            staffLoginModel!.save_profile()
                            completion()
                        }else{
                            if needShowInfo {
                                showInfo(message: json["message"].string!, didCompletion: {
                                    fail()
                                })
                            }else{
                                fail()
                            }
                            //showError(errorMessage: json["message"].string!)
                        }
                    case .failure(let error):
                        print("error - \(error.localizedDescription)")
                        showError(errorMessage: error.localizedDescription)
                        fail()
                        logout(completion: {}, fail: {})
                    }
            }
            debugReq(req)
        }
    }
    
    
    class func send_verification(withParams params:Parameters, completion:@escaping ((String)->()), fail:@escaping (()->())) {
        var parameters = params
        parameters["action"] = "sendSMS"
        //params["facebook_id"] = ""
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s16_verification.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                
                switch response.result {
                case .success:
                    print("SEND VERIFICAITON SUCCESS")
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    if json["status"] == true {
                        completion(json["message"].string!)
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("SEND VERIFICAITON FAIL")
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }

    class func logout(completion:@escaping (()->()), fail:@escaping (()->())) {
        var parameters = Parameters()
        parameters["language"] = "zho"
        if(StaffLoginModel.isLoggedIn()){
            parameters["id"] = StaffLoginModel.getOne().id
            parameters["logout_type"] = "staff"
        }else{
            parameters["id"] = ShopLoginModel.getOne().id
            parameters["logout_type"] = "host"
        }
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s02_logout.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    if json["status"] == true {
                        showInfo(message: json["message"].string!, didCompletion: {
                            completion()
                        })
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    //derek
    class func checkDevice(completion:@escaping (()->()), fail:@escaping (()->())) {
        var parameters = Parameters()
        if(StaffLoginModel.isStaff()){
            parameters["member_phone"] = StaffLoginModel.getOne().phone
        }else{
            parameters["shop_id"] = ShopLoginModel.getOne().id
        }
        parameters["device_code"] = UIDevice.current.identifierForVendor!.uuidString
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s00_checkDeviceCode.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    if json["status"] == true {
                        completion()
                    }else{
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    static var shopLoginModel:ShopLoginModel?
    static var staffLoginModel:StaffLoginModel?
    class func login(withParams params:Parameters, completion:@escaping (()->()), fail:@escaping (()->())) {
        var parameters = params
        //print(parameters["ip"])
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s01_staff_login.php",
        //let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s01_staff_login.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    
                    if json["status"] == true {
                        shopLoginModel = ShopLoginModel(userInfo: json["params"])
                        shopLoginModel!.save()

                        print(ShopLoginModel.getOne().login_type)
                        
                        completion()
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    class func manageLeave(withParams params:Parameters,completion:@escaping ((String)->()), fail:@escaping (()->())) {
        var parameters = params;
        parameters["staff_id"] = StaffLoginModel.getOne().id
        parameters["shop_id"] = StaffLoginModel.getOne().home_shop_id
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s05_manageLeave.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    
                    if json["status"] == true {
                        switch json["operation"].string!{
                        case "insert":
                            let application_id = json["params"]["appliaction_id"].string!
                            completion(application_id)
                            break
                        case "check":
                            let position = json["params"]["position"].string!
                            completion(position)
                            break
                        case "approve":
                            let approve_time = json["params"]["approve_time"].string!
                            let approve_status = json["params"]["approve_status"].string!
                            if(approve_status == "true"){
                                completion("Approve Successful: Approved Application\nTime："+approve_time)
                            }
                            else{
                                completion("Approve Successful: Rejected Application\nTime："+approve_time)
                            }
                            
                            break
                        default:
                            break
                            
                        }
                    }else{
                        showError(errorMessage: json["params"]["reason"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    class func getLeaveUserList(withParams params:Parameters,completion:@escaping ((Array<LeaveUserModel>)->()), fail:@escaping (()->())) {
        var parameters = params
        parameters["staff_id"] = StaffLoginModel.getOne().id
        parameters["shop_id"] = StaffLoginModel.getOne().home_shop_id
        
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s05_manageLeave.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    
                    if json["status"] == true {
                        var temp = Array<LeaveUserModel>()
                        for leaveObject in json["params"].array!{
                            temp.append(LeaveUserModel(data: leaveObject))
                        }
                        completion(temp)
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    class func getLeaveTypeList(completion:@escaping ((Array<LeaveModel>)->()), fail:@escaping (()->())) {
        var parameters = Parameters()
        
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s06_getLeaveType.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    
                    if json["status"] == true {
                        var temp = Array<LeaveModel>()
                        for leaveObject in json["params"].array!{
                            temp.append(LeaveModel(name: leaveObject["name"].string!, id: leaveObject["id"].string!))
                        }
                        completion(temp)
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    class func staff_login(withParams params:Parameters, completion:@escaping (()->()), fail:@escaping (()->())) {
        var parameters = params
        parameters["language"] = "zho"
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s01_staff_login.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    
                    if json["status"] == true {
                        staffLoginModel = StaffLoginModel(userInfo: json["params"])
                        staffLoginModel!.save()
                        print("Welcome " + StaffLoginModel.getOne().getName())
                        completion()
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    class func checkin(withParams params:Parameters, completion:@escaping (()->()), fail:@escaping (()->())) {
        let parameters = params
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s08_checkin.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    
                    if json["status"] == true {
                        completion()
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    class func apply_dayoff(withParams params:Parameters, completion:@escaping ((String)->()), fail:@escaping (()->())) {
        var parameters = params
        parameters["staff_id"] = StaffLoginModel.getOne().id
        parameters["device"] = StaffLoginModel.getOne().device_code
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s07_apply_dayoff.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    
                    if json["status"] == true {
                        completion(json["id"].stringValue)
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    class func uploadImage(img: UIImage,completion:@escaping ((String)->()), fail:@escaping (()->())){
        let ImageData = UIImagePNGRepresentation(img)
        let urlReq = "\(STAFF_SERVER_API)app_api_s07_upload_img.php"
//        let parameters = ["staff_number": StaffLoginModel.getOne().staff_number]
        var parameters = [String:String]()
        parameters["staff_number"] = StaffLoginModel.getOne().staff_number
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(ImageData!,withName: "image", fileName: "file.jpeg", mimeType: "image/jpg")
            for (key, value) in parameters {
                // this will loop the 'parameters' value, you can comment this if not needed
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },to:urlReq)
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    print(response.result.value)
                    let json = JSON(response.result.value!)
                    completion(json["params"]["img_path"].stringValue)
                }
            case .failure(let encodingError):
                print(encodingError)
                fail()
            }
        }
    }
    
    class func getReportData(withParams params:Parameters, success:@escaping ((Array<NSObject>)->()), fail:@escaping (()->())) {
        var parameters = params
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s10_getReportData.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    
                    if json["status"] == true {
                        
                        switch json["report_type"].stringValue{
                            case "details":
                                var detailsReport = [ReportDetailsModel]()
                                for (key,detailJSON) in json["params"]{
                                    let detail = ReportDetailsModel(data: detailJSON)
                                    detailsReport.append(detail)
                                }
                                success(detailsReport)
                                break
                            case "check_in":
                                //get year & month
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM"
                                let date = dateFormatter.date(from: parameters["date"] as! String)
                                let calendar = Calendar.current
                                let year_components = calendar.dateComponents([.year], from: date!)
                                let year = year_components.year
                                let month_components = calendar.dateComponents([.month], from: date!)
                                let month = month_components.month
                                
                                var checkinReport = [ReportCheckinModel]()
                                var checkinReportdays = [Int]()
                                
                                for checkinJSON in json["params"].array!{
                                    let checkin = ReportCheckinModel(data:checkinJSON)
                                    checkinReport.append(checkin)
                                    checkinReportdays.append(checkin.day)
                                }
                                //let totalCheckinReport = [ReportCheckinModel]()
                                for n in 0..<date!.getDaysInMonth(){
                                    let day = n+1
                                    if(!checkinReportdays.contains(day)){
                                        let tempModel = ReportCheckinModel(year: year!,month:month!,day: day, count: 0)
                                        checkinReport.append(tempModel)
                                    }
                                }
                                checkinReport.sort(by: {$0.day<$1.day})
                                success(checkinReport)
                                break
                            case "check_in_details":
                                var checkinDetailReport = [ReportCheckinDetailModel]()
                                
                                for checkinDetailJSON in json["params"].array!{
                                    let checkindetail = ReportCheckinDetailModel(data:checkinDetailJSON)
                                    checkinDetailReport.append(checkindetail)
                                }
                                success(checkinDetailReport)
                                break
                            case "late":
                                //get year & month
//                                let dateFormatter = DateFormatter()
//                                dateFormatter.dateFormat = "yyyy-MM"
//                                let date = dateFormatter.date(from: parameters["date"] as! String)
                                
                                var lateReport = [ReportLateModel]()
//                                var lateReportdays = [Int]()
//                                var lateReportTotals:Int = 0
                                //print(json["params"].array!)
                                for lateJSON in json["params"]["0"].array!{
                                    let late = ReportLateModel(data:lateJSON)
                                    lateReport.append(late)
//                                    lateReportdays.append(late.day)
//                                    lateReportTotals += late.lateInMins
                                }
//                                for n in 0..<date!.getDaysInMonth(){
//                                    let day = n+1
//                                    if(!lateReportdays.contains(day)){
//                                        let tempModel = ReportLateModel(day: day)
//                                        lateReport.append(tempModel)
//                                    }
//                                }
                                lateReport.sort(by: {$0.day<$1.day})
                                lateReport.insert(ReportLateModel(title:"Total in Minutes",total: json["params"]["total"].int!), at: 0)
                                success(lateReport)
                                break
                            case "leave":
                                var leavesReport = [ReportLeaveModel]()
                                for leaveJSON in json["params"].array!{
                                    let Leavedetail = ReportLeaveModel(data: leaveJSON)
                                    leavesReport.append(Leavedetail)
                                }
                                success(leavesReport)
                                break
                            default:
                                print("Process Failed - StaffID: " + json["params"]["user_staff_id"].stringValue)
                                fail()
                        }
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    showError(errorMessage: "Get Staff Data Fail")
                    //showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    class func getAvailableStaff(withParams params:Parameters, success:@escaping ((Array<ReportUserModel>)->()), fail:@escaping(()->())) {
        var parameters = params
        parameters["staff_id"] = StaffLoginModel.getOne().id
        let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s09_getAvailableStaff.php",
            method: .post,
            parameters: parameters).validate().responseJSON { response in
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    print(json.debugDescription)
                    
                    if json["status"] == true {
                        var availableUsers = [ReportUserModel]()
                        for userJSON in json["params"].array! {
                            let user = ReportUserModel(data: userJSON)
                            availableUsers.append(user)
//                            print(user.user_name!)
                        }
                        success(availableUsers)
                    }else{
                        showError(errorMessage: json["message"].string!)
                        fail()
                    }
                case .failure(let error):
                    print("error - \(error.localizedDescription)")
                    //showError(errorMessage: error.localizedDescription)
                    fail()
                }
        }
        debugReq(req)
    }
    
    class func getCompanyShop(withParams params:Parameters, success:@escaping ((Array<NSObject>)->()), fail:@escaping(()->())) {
            var parameters = params
            let req = Alamofire.request("\(STAFF_SERVER_API)app_api_s04_companyShop.php",
                method: .post,
                parameters: parameters).validate().responseJSON { response in
                    switch response.result {
                    case .success:
                        let json = JSON(response.result.value!)
                        print(json.debugDescription)
                        
                        if json["status"] == true {
                            if(json["action"] == "list_shop"){
                                var shops = [CompanyShopModel]()
                                for shopJSON in json["params"].array! {
                                    let shop = CompanyShopModel(data: shopJSON)
                                    shops.append(shop)
                                }
                                success(shops)
                            }else if(json["action"] == "list_staff"){
                                var staffs = [CompanyStaffModel]()
                                for staffJSON in json["params"].array! {
                                    let staff = CompanyStaffModel(data: staffJSON)
                                    staffs.append(staff)
                                }
                                success(staffs)
                            }

                        }else{
                            showError(errorMessage: json["message"].string!)
                            fail()
                            logout(completion: {}, fail: {})
                        }
                    case .failure(let error):
                        print("error - \(error.localizedDescription)")
                        //showError(errorMessage: error.localizedDescription)
                        fail()
                    }
            }
            debugReq(req)
        }
    
}
