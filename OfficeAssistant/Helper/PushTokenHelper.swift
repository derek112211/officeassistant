//
//  PushTokenHelper.swift
//  WellCore
//
//  Created by kelvin.lee(VTL) on 06/03/2018.
//  Copyright © 2018 VTLMACOS. All rights reserved.
//

import UIKit

class PushTokenHelper: NSObject {
 
    class func token(token:String) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "push_token")
    }
    
    class func token() -> String {
        if let result = UserDefaults.standard.value(forKey: "push_token") as? String {
            return result
        }
        return ""
    }
    
}
