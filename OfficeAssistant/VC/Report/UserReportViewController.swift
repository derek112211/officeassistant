//
//  UserReportViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 17/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit
import SwiftIcons

class UserReportViewController:UIViewController,UITableViewDelegate,UITableViewDataSource {
    var array = [NSObject]()
    var tbview:UITableView!
    var topic:String!
    var month:Int!
    var year:Int!
    var day:Int!
    var report_type:String!
    var staff_id:String!
    var shop_id:String!
    //var finalPage:Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if(report_type == "check_in_details"){
//            finalPage = false
//        }
        
        let btnBack = "btn-back".imageView()
        self.view.addSubview(btnBack)
        btnBack.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
        })
        let backArea = UIView()
        self.view.addSubview(backArea)
        backArea.snp.makeConstraints({ (make)->Void in
            make.center.equalTo(btnBack)
            make.width.height.equalTo(44)
        })
        backArea.randomBorder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
        backArea.addGestureRecognizer(tap)
        backArea.isUserInteractionEnabled = true
        
        let lbTitle = UILabel(fontSize: 24, color: UIColor.black)
        lbTitle.font = UIFont.boldSystemFont(ofSize: 24)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        let lbdate = UILabel(fontSize: 12, color: .black)
        self.view.addSubview(lbdate)
        lbdate.snp.makeConstraints({ (make)->Void in
            make.leading.equalTo(lbTitle)
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*2)
        })
        if let topic = topic{
            lbTitle.text = topic
        }
        if let month = month,let year = year{
            lbdate.text = String(self.year)+"/"+String(self.month)
            if(report_type == "check_in_details"){
                lbdate.text = "\(self.year!)/\(self.month!)/\(self.day!)"
            }
        }
        
        //init table view
        let fullScreenSize = self.view.bounds.size
        let tabViewHeight = MainViewController.me.stabBar.getHeight()
        tbview = UITableView(frame:CGRect(x:0, y:20, width: fullScreenSize.width, height: fullScreenSize.height-20),style: .grouped)
        
        //cells setting
        switch self.report_type {
        case "details":
            print("details Cell")
            tbview.register(ReportDetailCell.classForCoder(), forCellReuseIdentifier: "Cell")
            break
        case "check_in":
            print("check_in Cell")
            tbview.register(ReportCheckinCell.classForCoder(), forCellReuseIdentifier: "Cell")
            break
        case "check_in_details":
            print("check_in_details Cell")
            tbview.register(ReportCheckinDetailCell.classForCoder(), forCellReuseIdentifier: "Cell")
            break
        case "late":
            print("Late Cell")
            tbview.register(ReportLateCell.classForCoder(), forCellReuseIdentifier: "Cell")
            break
        case "leave":
            print("Leave Cell")
            tbview.register(ReportLeaveCell.classForCoder(), forCellReuseIdentifier: "Cell")
            break
        default:
            print("Set up cells failed")
            return
        }
        
        
        tbview.delegate =  self
        tbview.dataSource = self
        
        tbview.separatorStyle = .singleLine
        tbview.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        tbview.allowsSelection = true
        tbview.allowsMultipleSelection = false
        
        self.view.addSubview(tbview)
        tbview.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*4)
            //            make.height.equalTo(fullScreenSize.height-tabHeight-KEY_MARGIN*6)
            //make.bottom.equalToSuperview()
            make.width.equalTo(fullScreenSize.width)
            if(report_type == "check_in_details"){
                make.bottom.equalToSuperview()
            }

        })
        
        let lbpreviosM = MySquareButton(withData: ("Previos Month",UIColor.white,UIColor.black,FontType.fontAwesomeSolid(.arrowLeft)),pos:"left")
        self.view.addSubview(lbpreviosM)
        lbpreviosM.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(tbview.snp.bottom)
            make.bottom.equalToSuperview().inset(tabViewHeight)
            make.width.equalTo(fullScreenSize.width/2-1)
            make.height.equalTo(50)
        })
        
        let lbnextM = MySquareButton(withData: ("Next Month",UIColor.white,UIColor.black,FontType.fontAwesomeSolid(.arrowRight)),pos:"right")
        self.view.addSubview(lbnextM)
        lbnextM.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(tbview.snp.bottom)
            make.right.equalToSuperview()
            make.bottom.equalToSuperview().inset(tabViewHeight)
            make.width.equalTo(fullScreenSize.width/2-1)
            make.height.equalTo(50)
        })
        
        lbpreviosM.onClick = {
            self.month-=1
            //check
            self.checkMonthValid(month: self.month)
            var params = [String:Any]()
            params["report_type"] = self.report_type
            params["staff_id"] = self.staff_id
            params["shop_id"] = self.shop_id
            
            //date handling
            if(self.month<10){
                params["date"] = String(self.year)+"-0"+String(self.month)
//                print(String(self.year)+"-0"+String(self.month))
            }else{
                params["date"] = String(self.year)+"-"+String(self.month)
//                print(String(self.year)+"-"+String(self.month))
            }
            
            
            let lp = LoadingPopUp.addTo(vc: self)
            APIHelper.getReportData(withParams: params, success: {(reportdata) in
                if(reportdata.count < 1){
                    switch self.report_type {
                    case "details":
                        var empty_arr = [ReportDetailsModel]()
                        empty_arr.append(ReportDetailsModel())
                        self.array = empty_arr
                        break
                    case "leave":
                        var empty_arr = [ReportLeaveModel]()
                        empty_arr.append(ReportLeaveModel())
                        self.array = empty_arr
                        break
                    default:
                        break
                    }
                }else{
                    self.array = reportdata
                }
                self.tbview.reloadData()
                lbdate.text = String(self.year)+"/"+String(self.month)
                lp.removeFromSuperview()
            }, fail: {
                self.month+=1
                //check
                self.checkMonthValid(month: self.month)
                showError(errorMessage: "FAIL TO RELOAD DATA")
                lp.removeFromSuperview()
                return
            })
            
        }
        lbnextM.onClick = {
            self.month+=1
            self.checkMonthValid(month: self.month)
            var params = [String:Any]()
            params["report_type"] = self.report_type
            params["staff_id"] = self.staff_id
            params["shop_id"] = self.shop_id
            
            //date handling
            if(self.month<10){
                params["date"] = String(self.year)+"-0"+String(self.month)
                //                print(String(self.year)+"-0"+String(self.month))
            }else{
                params["date"] = String(self.year)+"-"+String(self.month)
                //                print(String(self.year)+"-"+String(self.month))
            }
            
            
            let lp = LoadingPopUp.addTo(vc: self)
            APIHelper.getReportData(withParams: params, success: {(reportdata) in
                if(reportdata.count < 1){
                    switch self.report_type {
                    case "details":
                        var empty_arr = [ReportDetailsModel]()
                        empty_arr.append(ReportDetailsModel())
                        self.array = empty_arr
                        break
                    case "leave":
                        var empty_arr = [ReportLeaveModel]()
                        empty_arr.append(ReportLeaveModel())
                        self.array = empty_arr
                        break
                    default:
                        break
                    }
                }else{
                    self.array = reportdata
                }
                self.tbview.reloadData()
                lbdate.text = String(self.year)+"/"+String(self.month)
                lp.removeFromSuperview()
            }, fail: {
                self.month-=1
                //check
                self.checkMonthValid(month: self.month)
                showError(errorMessage: "FAIL TO RELOAD DATA")
                lp.removeFromSuperview()
                return
            })
        }
        
        if(report_type == "check_in_details"){
            lbpreviosM.isHidden = true
            lbnextM.isHidden = true
        }
    }
    
    func checkMonthValid(month:Int){
        if(self.month<1){
            self.year-=1
            self.month=12
        }else if(self.month>12){
            self.year+=1
            self.month=1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.report_type {
        case "details":
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ReportDetailCell
            cell.selectionStyle = .none
            cell.setData(data: array[indexPath.row] as! ReportDetailsModel)
            return cell
        case "check_in":
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ReportCheckinCell
            cell.selectionStyle = .none
            cell.setData(data: array[indexPath.row] as! ReportCheckinModel)
            return cell
        case "check_in_details":
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ReportCheckinDetailCell
            cell.selectionStyle = .none
            cell.setData(data: array[indexPath.row] as! ReportCheckinDetailModel)
            return cell
        case "late":
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ReportLateCell
            cell.selectionStyle = .none
            cell.setData(data: array[indexPath.row] as! ReportLateModel)
            return cell
        case "leave":
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ReportLeaveCell
            cell.selectionStyle = .none
            cell.setData(data: array[indexPath.row] as! ReportLeaveModel)
            return cell
        default:
            print("set cell fail in table view")
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ListCell
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = "No records"
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        //cell.accessoryType = .disclosureIndicator
        //print("CALLED " + (String)(indexPath.row))
        return cell
    }
    
    //on tab send info
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = indexPath.row
        switch self.report_type {
        case "check_in":
            let lp = LoadingPopUp.addTo(vc: self)
            let temp = self.array[p] as! ReportCheckinModel
            if(temp.count == 0){
                showError(errorMessage: "\(temp.year!)-\(temp.month!)-\(temp.day!)，No Check-in Record！")
                lp.removeFromSuperview()
                return
            }
            var params = [String: Any]()
            params["report_type"] = "check_in_details"
            params["staff_id"] = self.staff_id
            params["shop_id"] = self.shop_id
            params["year"] = temp.year
            params["month"] = temp.month
            params["day"] = temp.day
            let controller = UserReportViewController()
            let startDate = Date()
            APIHelper.getReportData(withParams: params, success: { (reportsData) in
                controller.array = reportsData
                controller.topic = self.topic
                controller.report_type = "check_in_details"
                controller.year = temp.year
                controller.month = temp.month
                controller.day = temp.day
                
                //DEBUG - get Response Time
                getResponseTime(startDate: startDate)
                
                MainViewController.me.nav.pushViewController(controller, animated: false)
                lp.removeFromSuperview()
            }, fail: {
                showError(errorMessage: "Get Check in Details Fail")
                lp.removeFromSuperview()
            })
            break
        case "check_in_details":
            let lp = LoadingPopUp.addTo(vc: self)
            let temp = self.array[p] as! ReportCheckinDetailModel
            let alert = UIAlertController(title: "Information", message: nil, preferredStyle: UIAlertControllerStyle.alert)


            if let topController = UIApplication.topViewController(){
                let btn1 = UIAlertAction(title: "Copy Address", style: .default, handler: {
                    (action: UIAlertAction!) in
                    UIPasteboard.general.string = temp.address
                    showInfo(message: "Address Copied", didCompletion: {
                        print("Copy Address Action")
                    })
                })
                let btn2 = UIAlertAction(title: "Map", style: .default, handler: {
                    (action: UIAlertAction!) in
                    print("Map Action")
                })
                alert.addAction(btn1)
//                alert.addAction(btn2)
                let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
                    (action: UIAlertAction!) in
                    lp.removeFromSuperview()
                })
                alert.addAction(cancel)
                topController.present(alert, animated: true, completion: nil)
                lp.removeFromSuperview()
            }else{
                let delegate = UIApplication.shared.delegate as! AppDelegate
                delegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
            
            break
        default:
            print("Report Type Error")
            return
        }
        
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
}

