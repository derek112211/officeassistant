//
//  SubReportViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 17/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit

class SubReportViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var topic:String!
    var report_type:String!
    var array = [ReportUserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btnBack = "btn-back".imageView()
        self.view.addSubview(btnBack)
        btnBack.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
        })
        let backArea = UIView()
        self.view.addSubview(backArea)
        backArea.snp.makeConstraints({ (make)->Void in
            make.center.equalTo(btnBack)
            make.width.height.equalTo(44)
        })
        backArea.randomBorder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
        backArea.addGestureRecognizer(tap)
        backArea.isUserInteractionEnabled = true
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black)
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        if let topic = topic{
            lbTitle.text = topic
        }
        
        //init table view
        let fullScreenSize = self.view.bounds.size
        let tbview = UITableView(frame:CGRect(x:0, y:20, width: fullScreenSize.width, height: fullScreenSize.height-20),style: .grouped)
        
        //cells setting
        tbview.register(ListUserCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        tbview.delegate =  self
        tbview.dataSource = self
        
        tbview.separatorStyle = .singleLine
        tbview.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        tbview.allowsSelection = true
        tbview.allowsMultipleSelection = false
        
        self.view.addSubview(tbview)
        let tabHeight = MainViewController.me.stabBar.bounds.size.height
        tbview.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*4)
//            make.height.equalTo(fullScreenSize.height-tabHeight-KEY_MARGIN*6)
            make.bottom.equalToSuperview()
            make.width.equalTo(fullScreenSize.width)
        })
        
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ListUserCell
        cell.selectionStyle = .none
        //cell.textLabel?.text = array[indexPath.row].getFullNameWithUserName()
        cell.setData(data: array[indexPath.row])
        cell.accessoryType = .disclosureIndicator
        print("CALLED " + (String)(indexPath.row))
        
        return cell
    }
    
    //on tab send info
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = indexPath.row
        var params = [String: Any]()
        params["report_type"] = self.report_type
        params["staff_id"] = self.array[p].id
        params["shop_id"] = self.array[p].home_shop_id
        
        //get today String
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM"
        let date = formatter.string(from: Date())
        params["date"] = date
  
        let controller = UserReportViewController()
        let lp = LoadingPopUp.addTo(vc: self)
        let startDate = Date()
        APIHelper.getReportData(withParams: params, success: { (reportsData) in
            //data array
            if(reportsData.count < 1){
                if(self.report_type == "details"){
                    var empty_arr = [ReportDetailsModel]()
                    empty_arr.append(ReportDetailsModel())
                    controller.array = empty_arr
                }else if(self.report_type == "leave"){
                    var empty_arr = [ReportLeaveModel]()
                    empty_arr.append(ReportLeaveModel())
                    controller.array = empty_arr
                }

            }else{
                controller.array = reportsData
            }
            
            //params
            controller.report_type = self.report_type
            controller.staff_id = self.array[p].id
            controller.shop_id = self.array[p].home_shop_id
            
            //extract month
            let date_array = date.components(separatedBy: "-")
            controller.month = Int(date_array[1])!
            controller.year = Int(date_array[0])!
            controller.topic  = self.array[p].getFullNameWithUserName()
            
            //DEBUG - get Response Time
            getResponseTime(startDate: startDate)
            
            MainViewController.me.nav.pushViewController(controller, animated: false)
            lp.removeFromSuperview()
        }, fail: {
            showError(errorMessage: "Get Staff -"+self.array[p].getFullNameWithUserName()+" Fail")
            lp.removeFromSuperview()
        })
        
    }
}
