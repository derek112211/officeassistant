//
//  ReportViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 16/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit

class ReportViewController : UIViewController,UITableViewDelegate, UITableViewDataSource{

    var array = ["Schedule Records","Check-in Records","Late Records","Leave Records"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black, str: "Report")
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        //init table view
        let fullScreenSize = self.view.bounds.size
        let tbview = UITableView(frame:CGRect(x:0, y:20, width: fullScreenSize.width, height: fullScreenSize.height-20),style: .grouped)
        
        //cells setting
        tbview.register(ListCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        tbview.delegate =  self
        tbview.dataSource = self
        
        tbview.separatorStyle = .singleLine
        tbview.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        tbview.allowsSelection = true
        tbview.allowsMultipleSelection = false
        
        self.view.addSubview(tbview)
        tbview.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*4)
            make.width.equalTo(fullScreenSize.width)
            make.bottom.equalToSuperview()
        })
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ListCell
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = array[indexPath.row]
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
//        cell.setData(data: array[indexPath.row])
        
        print("CALLED " + (String)(indexPath.row))
        
        return cell
    }
    
    //on tab send info
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = indexPath.row
        var params = [String: Any]()
        switch p {
        case 0:
            params["report_type"] = "details"
            break
        case 1:
            params["report_type"] = "check_in"
            break
        case 2:
            params["report_type"] = "late"
            break
        case 3:
            params["report_type"] = "leave"
            break
        default:
            params["report_type"] = ""
        }
        let report_type:String = params["report_type"] as! String
//        if(report_type == "leave"){
//            showInfo(message: "Coming Soon", didCompletion: {})
//            return;
//        }
        let controller = SubReportViewController()
        let lp = LoadingPopUp.addTo(vc: self)
        let startDate = Date()
        APIHelper.getAvailableStaff(withParams: params, success: { (availableUsers) in
            controller.array = availableUsers
            controller.topic  = self.array[p]
            controller.report_type = params["report_type"] as! String
            
            //DEBUG - get Response Time
            getResponseTime(startDate: startDate)
            
            MainViewController.me.nav.pushViewController(controller, animated: false)
            lp.removeFromSuperview()
        }, fail: {
            showError(errorMessage: "Get Available Staff Fail")
            lp.removeFromSuperview()
        })

    }
}

