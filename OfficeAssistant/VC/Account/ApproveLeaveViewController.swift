//
//  ApproveLeaveViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 8/8/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit

class ApproveLeaveViewController : UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var topic:String!
    var position:String! // staff || department_approver || hr_approver
    var picker:UIDatePicker!
    var array = [LeaveUserModel]()
    var tbview:UITableView!
    var refreshControl = UIRefreshControl()
    
    override func viewWillAppear(_ animated: Bool) {
        tbview.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btnBack = "btn-back".imageView()
        self.view.addSubview(btnBack)
        btnBack.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
        })
        let backArea = UIView()
        self.view.addSubview(backArea)
        backArea.snp.makeConstraints({ (make)->Void in
            make.center.equalTo(btnBack)
            make.width.height.equalTo(44)
        })
        backArea.randomBorder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
        backArea.addGestureRecognizer(tap)
        backArea.isUserInteractionEnabled = true
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black)
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        let lbPosition = UILabel(fontSize: 12, color: UIColor.black)
        lbPosition.font = UIFont.boldSystemFont(ofSize: 12)
        self.view.addSubview(lbPosition)
        lbPosition.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(lbTitle.snp.bottom)
        })
        
        //init table view
        let fullScreenSize = self.view.bounds.size
        tbview = UITableView(frame:CGRect(x:0, y:20, width: fullScreenSize.width, height: fullScreenSize.height-20),style: .grouped)
        
        //cells setting
        tbview.register(LeaveCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        tbview.delegate =  self
        tbview.dataSource = self
        
        tbview.separatorStyle = .singleLine
        tbview.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        tbview.allowsSelection = true
        tbview.allowsMultipleSelection = false
        
        self.view.addSubview(tbview)
        tbview.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbPosition.snp.bottom).offset(KEY_MARGIN*4)
            make.bottom.equalToSuperview()
            make.width.equalTo(fullScreenSize.width)
        })
        
        if let topic = self.topic,let position = self.position{
            lbTitle.text = topic
            switch position{
            case "department_approver":
                lbPosition.text = "Department Head"
                break;
            case "hr_approver":
                lbPosition.text = "HR"
                break;
            case "both":
                lbPosition.text = "Department Head & HR"
                break;
            default:
                break
            }
            
        }
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: UIControl.Event.valueChanged)
        tbview.addSubview(refreshControl) // not required when using UITableViewController

        
    }
    
    @objc func refresh(_ sender:AnyObject) {
        // Code to refresh table view
        let lp = LoadingPopUp.addTo(vc: self)
        var params = [String:Any]()
        params["position"] = self.position
        params["operation"] = "list"
        APIHelper.getLeaveUserList(withParams: params, completion: { (array) in
            self.array = array
            lp.removeFromSuperview()
        }, fail: {
            lp.removeFromSuperview()
            return
        })
        tbview.reloadData()
        refreshControl.endRefreshing()
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LeaveCell
        cell.selectionStyle = .none
        //cell.textLabel?.text = array[indexPath.row].getFullNameWithUserName()
        cell.setData(data: array[indexPath.row])
        cell.accessoryType = .disclosureIndicator
        print("CALLED " + (String)(indexPath.row))
        
        return cell
    }
    
    //on tab send info
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = indexPath.row
        if(array.count == 1 && array[p].id == nil){
           return
        }
        var params = [String: Any]()
        let controller = ApproveLeaveDetailsViewController()
        controller.topic = topic
        controller.subtopic = "Application No. - \(array[p].application_no!) Data"
        controller.leaveObject = array[p]
        controller.position = self.position
        MainViewController.me.nav.pushViewController(controller, animated: false)
        
    }
}
