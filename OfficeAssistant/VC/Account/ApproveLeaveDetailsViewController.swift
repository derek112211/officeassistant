//
//  ApproveLeaveDetailsViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 8/8/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import SwiftIcons

class ApproveLeaveDetailsViewController : UIViewController,UITextViewDelegate{
    
    var subtopic:String!
    var topic:String!
    var position:String!
    var leaveObject:LeaveUserModel!
    var Reason:UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let btnBack = "btn-back".imageView()
        self.view.addSubview(btnBack)
        btnBack.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
        })
        let backArea = UIView()
        self.view.addSubview(backArea)
        backArea.snp.makeConstraints({ (make)->Void in
            make.center.equalTo(btnBack)
            make.width.height.equalTo(44)
        })
        backArea.randomBorder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
        backArea.addGestureRecognizer(tap)
        backArea.isUserInteractionEnabled = true
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black)
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        let lbSubTitle = UILabel(fontSize: 12, color: UIColor.black)
        lbSubTitle.font = UIFont.boldSystemFont(ofSize: 12)
        self.view.addSubview(lbSubTitle)
        lbSubTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(lbTitle.snp.bottom)
        })
        
        let sv = UIScrollView()
        self.view.addSubview(sv)
        sv.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.bottom.equalTo(view)
            make.top.equalToSuperview().offset(statusBarH + 120)
            make.bottom.equalToSuperview().offset(0)
        })
        sv.showsHorizontalScrollIndicator = false
        
        let lbStatus = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbStatus)
        lbStatus.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*8)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let lbAppId = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbAppId)
        lbAppId.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbStatus.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let lbName = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbName)
        lbName.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbAppId.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let lbShopName = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbShopName)
        lbShopName.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbName.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let lbLeaveType = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbLeaveType)
        lbLeaveType.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbShopName.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let lbLeaveFrom = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbLeaveFrom)
        lbLeaveFrom.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveType.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let lbLeaveTo = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbLeaveTo)
        lbLeaveTo.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveFrom.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let lbLeaveDays = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbLeaveDays)
        lbLeaveDays.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveTo.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let lbLeaveReason = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbLeaveReason)
        lbLeaveReason.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveDays.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let lbReason = UILabel(fontSize: 16, color: .black)
        sv.addSubview(lbReason)
        lbReason.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveReason.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        Reason = UITextView()
        Reason.textColor = UIColor.lightGray
        Reason.text = "Approve Reason..."
        Reason.textAlignment = .left
        Reason.font = UIFont.systemFont(ofSize: 16)
        Reason.delegate = self
        self.view.addSubview(Reason)
        Reason.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveReason.snp.bottom).offset(KEY_MARGIN)
            make.left.equalTo(lbReason.snp.right)
            make.right.equalToSuperview().inset(KEY_MARGIN*4)
            make.height.equalTo(100)
        })
        
        let fullScreenSize = self.view.bounds.size
        let submitBtn = MySquareButton(withData: ("Approve",UIColor.white,UIColor.black,FontType.fontAwesomeSolid(.check)),pos: "left")
        
        self.view.addSubview(submitBtn)
        submitBtn.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(Reason.snp.bottom).offset(KEY_MARGIN*4)
            make.width.equalTo(fullScreenSize.width/2-1)
            make.height.equalTo(50)
        })
        let cancelBtn = MySquareButton(withData: ("Reject",UIColor.white,UIColor.black,FontType.fontAwesomeSolid(.times)),pos: "left")
        self.view.addSubview(cancelBtn)
        cancelBtn.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(Reason.snp.bottom).offset(KEY_MARGIN*4)
            make.right.equalToSuperview()
            make.width.equalTo(fullScreenSize.width/2-1)
            make.height.equalTo(50)
        })
        
        submitBtn.onClick = {
            showInfo(message: "Approve Application?", didOk: {
                var params = [String: Any]()
                params["operation"] = "approve"
                params["application_id"] = self.leaveObject.id
                params["application_no"] = self.leaveObject.application_no
                params["application_shop_id"] = self.leaveObject.shop_id
//                params["approve_position"] = self.position
                params["approve_status"] = "true"
                if(self.Reason.text == "Approve Reason..."){
                    params["approve_remark"] = ""
                }else{
                    params["approve_remark"] = self.Reason.text
                }
                params["application_staff_id"] = self.leaveObject.staff_id
                if(self.position == "both"){
                    if(self.leaveObject.leave_department_approve_status == "-1"){
                        params["approve_position"] = "department_approver"
                    }else if(self.leaveObject.leave_hr_approve_status == "-1"){
                        params["approve_position"] = "hr_approver"
                    }
                }else{
                    params["approve_position"] = self.position
                }
                
                APIHelper.manageLeave(withParams: params, completion: { (resultStr) in
                    showInfo(message: resultStr, didCompletion: {
                        self.navigationController!.popViewControllers(viewsToPop: 2)
                    })
                }, fail: {
                    return
                })
            }, didCancel: {
                return
            })
        }
        
        cancelBtn.onClick = {
            showInfo(message: "Reject Application?", didOk: {
                var params = [String: Any]()
                params["operation"] = "approve"
                params["application_id"] = self.leaveObject.id
                params["application_no"] = self.leaveObject.application_no
                params["application_shop_id"] = self.leaveObject.shop_id
//                params["approve_position"] = self.position
                params["approve_status"] = "false"
                if(self.Reason.text == "Approve Reson..."){
                    params["approve_remark"] = ""
                }else{
                    params["approve_remark"] = self.Reason.text
                }
                params["application_staff_id"] = self.leaveObject.staff_id
                if(self.position == "both"){
                    if(self.leaveObject.leave_department_approve_status == "-1"){
                        params["approve_position"] = "department_approver"
                    }else if(self.leaveObject.leave_hr_approve_status == "-1"){
                        params["approve_position"] = "hr_approver"
                    }
                }else{
                    params["approve_position"] = self.position
                }
                APIHelper.manageLeave(withParams: params, completion: { (resultStr) in
                    showInfo(message: resultStr, didCompletion: {
                        self.navigationController!.popViewControllers(viewsToPop: 2)
                    })
                }, fail: {
                    return
                })
            }, didCancel: {
                return
            })
        }
        
        
        if let topic:String = topic,
            let subtopic:String = subtopic,
            let application_id:String = leaveObject.id,
            let application_no:String = leaveObject.application_no,
            let application_department:String = leaveObject.leave_department_approve_status,
            let application_hr:String = leaveObject.leave_hr_approve_status,
            let name:String = leaveObject.getFullName(),
            let shop_name:String = leaveObject.shop_name,
            let leave_type:String = leaveObject.leave_type_name,
            let leave_from:String = leaveObject.leave_from,
            let leave_days:String = leaveObject.leave_days,
            let leave_reason:String = leaveObject.reason,
            let short_name:String = leaveObject.short_name,
            let leave_time_to:String = leaveObject.leave_time_to,
            let leave_time_from:String = leaveObject.leave_time_from,
            let leave_to:String = leaveObject.leave_to
            {
            lbTitle.text = topic
            lbSubTitle.text = subtopic
            lbAppId.text = "Application ID: "+application_id
            if(application_department == "-1"){
                lbStatus.text = "Status - Waiting Department approval"
            }else if(application_hr == "-1"){
                lbStatus.text = "Status - Waiting HR approval"
            }else{
                lbStatus.text = "Status - Error"
            }
            lbName.text = "Staff Name: "+name
            lbShopName.text = "Shop: "+shop_name
            lbLeaveType.text = "Leave Type: "+leave_type
            lbLeaveTo.text = "End Date: "+leave_to+" "+leave_time_to
            lbLeaveFrom.text  = "Start Date: "+leave_from+" "+leave_time_from
            lbLeaveDays.text = "Time Duration: \(leave_days)Days"
            lbLeaveReason.text = "Leave Reason: \(leave_reason)"
            lbReason.text = "Approve Reason: "
        }
        
        

    }
    
    func textViewDidBeginEditing (_ textView: UITextView) {
        if Reason.textColor == UIColor.lightGray && Reason.isFirstResponder {
            Reason.text = nil
            Reason.textColor = .black
        }
    }
    
    func textViewDidEndEditing (_ textView: UITextView) {
        if Reason.text.isEmpty || Reason.text == "" {
            Reason.textColor = .lightGray
            Reason.text = "Approve Reason..."
        }
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
}
