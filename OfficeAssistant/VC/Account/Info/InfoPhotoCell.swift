//
//  InfoPhotoCell.swift
//  EM_Ios_new
//
//  Created by SocialMind on 10/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//
import UIKit

class InfoPhotoCell: UICollectionViewCell {
    
    let photo = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        
        let margin:CGFloat = 20
        contentView.snp.makeConstraints({ (make)->Void in
            make.width.height.equalTo(sw-margin*2)
        })
        
        contentView.addSubview(photo)
        photo.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        photo.contentMode = .scaleAspectFit
        photo.clipsToBounds = true
        
        contentView.addSubview(videoPlayer)
        videoPlayer.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Photo(_:)))
        //        photo.addGestureRecognizer(tap)
        //        photo.isUserInteractionEnabled = true
        
    }
    
    func setData(data:Media) {
        if data.type == MediaType.Image {
            photo.isHidden = false
            videoPlayer.isHidden = true
            //videoPlayer.stopVideo()
            videoPlayer.stop()
            videoPlayer.clear()
            
            photo.kf.setImage(with: URL(string: data.path!),  //.toURL()
                placeholder:  UIImage(named: "dummy-image.jpg"),
                options: nil,
                progressBlock: nil,
                completionHandler: { (img, e, c, u) in
            })
        }
        if data.type == MediaType.Youtube {
            photo.isHidden = true
            videoPlayer.isHidden = false
            
            if let id = youtubeIdFromString(str: data.path!) {
                videoPlayer.loadVideoID(id)
                //videoPlayer.isHidden = true
            }
            
            //            var playerVars = [String: Any]()
            //            playerVars["showinfo"] = 0
            //            //playerVars["playsinline"] = 1
            //            //https://github.com/youtube/youtube-ios-player-helper/issues/292
            //            if let id = youtubeIdFromString(str: data.path!) {
            //                videoPlayer.load(withVideoId: id, playerVars: playerVars)
            //            }
            
        }
}
