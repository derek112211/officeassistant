//
//  AccountInfo.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 13/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class AccountInfo: UIView, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    let picker = UIImagePickerController()
    let profilePic = UIImageView()
    let qrImage = UIImageView()
    var btnSend:MyButton!
    var btnUpdate:UILabel!
    let qrImageOverlay = UIView()
    let contentView = UIView()
    var isStaff = StaffLoginModel.isStaff()
    
    override init(frame: CGRect) {

        super.init(frame: frame)
        let sv = UIScrollView()
        self.addSubview(sv)
        sv.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        sv.showsHorizontalScrollIndicator = false
        
        sv.addSubview(contentView)
        contentView.snp.makeConstraints({ (make)->Void in
            make.trailing.leading.equalTo(self)
            make.top.equalTo(sv)
            make.bottom.equalTo(sv)
        })
        
        contentView.addSubview(profilePic)
        profilePic.snp.makeConstraints({ (make)->Void in
            make.centerX.equalToSuperview()
            make.top.equalTo(contentView).offset(26)
            make.width.height.equalTo(80)
        })
        profilePic.backgroundColor = UIColor.white
        profilePic.layer.borderColor = UIColor.lightGray.cgColor
        profilePic.layer.borderWidth = 1
        profilePic.layer.cornerRadius = 40
        profilePic.contentMode = .scaleAspectFill
        profilePic.clipsToBounds = true

//        //"https://vignette.wikia.nocookie.net/merchantrpg/images/5/5d/Android_os1600.png/revision/latest?cb=20170806162743"
//        profilePic.kf.setImage(with: URL(string: StaffLoginModel.getOne().photo),  //.toURL()
//            placeholder:  UIImage(named: "menu-acc-on"),
//            options: nil,
//            progressBlock: nil,
//            completionHandler: { (img, e, c, u) in
//                
//        })
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_ProfilePic(_:)))
//        profilePic.addGestureRecognizer(tap)
//        profilePic.isUserInteractionEnabled = true
        
//        let editIcon = "btn-edit".imageView()
//        contentView.addSubview(editIcon)
//        editIcon.snp.makeConstraints({ (make)->Void in
//            make.centerX.equalTo(profilePic.snp.trailing).offset(3)
//            make.bottom.equalTo(profilePic)
//            make.width.height.equalTo(16)
//        })
        
        let qrCodeIconView = UIView()
        contentView.addSubview(qrCodeIconView)
        qrCodeIconView.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(profilePic.snp.bottom).offset(KEY_MARGIN)
            make.width.equalToSuperview()
            make.height.equalTo(20)
        })
        
        let lb = UILabel(fontSize: 16, color: UIColor.darkText)
        qrCodeIconView.addSubview(lb)
        lb.snp.makeConstraints({ (make)->Void in
            make.centerX.equalToSuperview()
//            make.centerX.equalToSuperview()
//            make.top.equalTo(qrImage.snp.left).offset(KEY_MARGIN)
        })
        lb.text = StaffLoginModel.getOne().getName()
        lb.isUserInteractionEnabled = true
        lb.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.qrImageTap)))
        
        
        qrCodeIconView.addSubview(qrImage)
        qrImage.snp.makeConstraints({ (make)->Void in
            make.left.equalTo(lb.snp.right).offset(5)
            make.width.height.equalTo(20)
        })
        qrImage.contentMode = .scaleAspectFill
        qrImage.clipsToBounds = true
        qrImage.tintColor = UIColor.init(hex: "7b7b7b")
        qrImage.image = UIImage(named: "qr_code_icon.png");
        qrImage.isUserInteractionEnabled = true
        qrImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.qrImageTap)))
        
        let progressCircle = CircularProgress()
        if(!isStaff){
            contentView.addSubview(progressCircle)
            progressCircle.snp.makeConstraints({ (make)->Void in
                //            make.centerX.equalToSuperview()
                //            make.top.equalTo(lb.snp.bottom).offset(KEY_MARGIN*2)
                //            make.width.height.equalTo(170)
                
                make.leading.trailing.equalToSuperview()
                make.top.equalTo(qrCodeIconView.snp.bottom).offset(KEY_MARGIN*2)
                make.height.equalTo(238*sr)
            })
            progressCircle.upateValue()
        }

        
        let info = UILabel(fontSize: 16, color: UIColor.darkText, str: "個人資料")
        contentView.addSubview(info)
        info.snp.makeConstraints({ (make)->Void in
            make.leading.equalTo(contentView).offset(KEY_MARGIN)
            if(isStaff){
                make.top.equalTo(qrCodeIconView.snp.bottom).offset(30)
            }else{
                make.top.equalTo(progressCircle.snp.bottom).offset(30)
                make.height.equalTo(30)
            }
            
        })

        let info2 = UILabel(fontSize: 11, color: UIColor.init(hex: "bbbbbb"), str: "如需更新其他個人資料，可於門市尋求職員協助。")
        info2.numberOfLines = 0
        info2.lineBreakMode = .byWordWrapping
        contentView.addSubview(info2)
        info2.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(contentView).inset(KEY_MARGIN)
            make.height.equalTo(20)
            make.top.equalTo(info.snp.bottom).offset(KEY_MARGIN)
        })
        
        btnUpdate = UILabel(fontSize: 16, color: UIColor.darkText, str: "更新")
        btnUpdate.textAlignment = .center
        btnUpdate.sizeToFit()
        contentView.addSubview(btnUpdate)
        btnUpdate.snp.makeConstraints({ (make)->Void in
            make.trailing.equalTo(contentView).offset(-KEY_MARGIN)
            make.top.equalTo(info.snp.bottom).offset(KEY_MARGIN)
            make.width.equalTo(btnUpdate.frame.size.width + 20)
            make.height.equalTo(btnUpdate.frame.size.height + 10)
        })
        btnUpdate.layer.cornerRadius = KEY_RADIUS
        btnUpdate.layer.masksToBounds = true
        btnUpdate.backgroundColor = UIColor.init(hex: "eeeeee")
        btnUpdate.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_btnUpdate(_:)))
        btnUpdate.addGestureRecognizer(tap)
        btnUpdate.isUserInteractionEnabled = true
        
        let infoTel = AccountInfoCell(withData: ("電話", StaffLoginModel.getOne().phone, AccountInfoType.Phone))
        contentView.addSubview(infoTel)
        infoTel.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(contentView)
            make.height.equalTo(50)
            make.top.equalTo(info2.snp.bottom).offset(KEY_MARGIN)
            //            if n == 4 {
            //                make.bottom.equalTo(contentView)
            //            }
        })
        
        let infoEmail = AccountInfoCell(withData: ("電郵", StaffLoginModel.getOne().email, AccountInfoType.Email))
        contentView.addSubview(infoEmail)
        infoEmail.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(contentView)
            make.height.equalTo(50)
            make.top.equalTo(infoTel.snp.bottom).offset(0)
        })
        infoEmail.tf.delegate = self

        let infoDOB:AccountInfoCell
        infoDOB = AccountInfoCell(withData: ("出生日期", StaffLoginModel.getOne().getDOB(), AccountInfoType.DOB))
        contentView.addSubview(infoDOB)
        infoDOB.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(contentView)
            make.height.equalTo(50)
            make.top.equalTo(infoEmail.snp.bottom).offset(0)
        })
        
        let infoDevice:AccountInfoCell
        infoDevice = AccountInfoCell(withData: ("UUID", StaffLoginModel.getOne().device_code, AccountInfoType.DeviceCode))
        contentView.addSubview(infoDevice)
        infoDevice.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(contentView)
            make.height.equalTo(50)
            make.top.equalTo(infoDOB.snp.bottom).offset(0)
            
            make.bottom.equalTo(contentView).offset(-KEY_MARGIN*2)
        })
        
//        btnSend = MyButton(withData: ("更新",UIColor.white,UIColor.init(hex: "c2b8b5")))
//        self.addSubview(btnSend)
//        btnSend.snp.makeConstraints({ (make)->Void in
//            make.width.equalTo(120)
//            make.centerX.equalToSuperview()
//            make.top.equalTo(infoDOB.snp.bottom).offset(KEY_MARGIN*2)
//            make.height.equalTo(40)
//            make.bottom.equalTo(contentView).offset(-KEY_MARGIN*2)
//        })
//        btnSend.isHidden = true
//        btnSend.onClick = {
//
//        }

        
        
//        QR code overlay
        sv.addSubview(qrImageOverlay)
        qrImageOverlay.snp.makeConstraints({ (make)->Void in
            make.trailing.leading.equalTo(self)
            make.top.equalTo(sv)
            make.bottom.equalTo(sv)
        })
        qrImageOverlay.backgroundColor = UIColor.white
        qrImageOverlay.isHidden = true

        let qrCloseBtn = UIImageView()
        qrCloseBtn.image = UIImage(named: "btn-cls@3x.png");
        qrImageOverlay.addSubview(qrCloseBtn);
        qrCloseBtn.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(5)
            make.right.equalTo(-5)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.bottomMargin.equalTo(5)
        })
        qrCloseBtn.isUserInteractionEnabled = true
        qrCloseBtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.qrImageOverlayCloseTap)))

//        let encodedImageData = StaffLoginModel.getOne().getQRCodeBase64()
//        let imageData:Data = Data(base64Encoded: encodedImageData, options: .ignoreUnknownCharacters)!

//        let qrCode = UIImageView()
//        qrCode.image = UIImage(data: imageData);
//        qrImageOverlay.addSubview(qrCode);
//        qrCode.snp.makeConstraints({ (make)->Void in
//            make.top.equalTo(qrCloseBtn.snp.bottom).offset(25)
//            make.leftMargin.equalTo(20)
//            make.rightMargin.equalTo(-20)
//            make.height.equalTo(qrCode.snp.width)
//        })
    }
    
    // Func in your UIViewController
    @objc func qrImageTap() {
        print("Opening QR code image overlay")
        
        self.qrImageOverlay.isHidden = false
    }
    
    @objc func qrImageOverlayCloseTap() {
        print("Closing QR code image overlay")
        
        self.qrImageOverlay.isHidden = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing")
        AccountEditHelper.email = textField.text!
        if AccountEditHelper.email != StaffLoginModel.getOne().email {
            self.btnUpdate.isHidden = false
        }else{
            self.btnUpdate.isHidden = true
        }
    }
    
    @objc func onTap_btnUpdate(_ sender: UITapGestureRecognizer) {
        
        if !isValidEmail(testStr: AccountEditHelper.email) {
            showError(errorMessage: "請輸入有效的電郵格式")
            return
        }
        
        showInfo(message: "確定要更新嗎？", didOk: {
            let lp = LoadingPopUp.addTo(vc: self.parentViewController!)
            APIHelper.update_profile(completion: {
                lp.removeFromSuperview()
                self.btnUpdate.isHidden = true
            }, fail: { lp.removeFromSuperview() })
        }, didCancel: {})
    }
    
    @objc func onTap_ProfilePic(_ sender: UITapGestureRecognizer) {

        let alert = UIAlertController(title: "選擇圖像", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "相機", style: .default, handler: { _ in
            let cameraMediaType = AVMediaType.video
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
            switch cameraAuthorizationStatus {
            case .denied:
                let alert = UIAlertController(title:nil, message: "請點擊設定並打開相機功能", preferredStyle: UIAlertControllerStyle.alert)
                let cancel = UIAlertAction(title: "取消", style: UIAlertActionStyle.cancel, handler: {
                    (action: UIAlertAction!) in
                })
                alert.addAction(cancel)
                let setting = UIAlertAction(title: "設定", style: UIAlertActionStyle.default, handler: {
                    (action: UIAlertAction!) in
                    if let url = NSURL(string: UIApplicationOpenSettingsURLString){
                        UIApplication.shared.openURL(url as URL)
                    }
                })
                alert.addAction(setting)
                self.parentViewController!.present(alert, animated: true, completion: nil)
                break
            case .authorized:
                self.openCamera()
                break
            case .restricted: break
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                    if granted {
                        print("Granted access to \(cameraMediaType)")
                        DispatchQueue.main.async {
                            self.openCamera()
                        }
                    } else {
                        print("Denied access to \(cameraMediaType)")
                    }
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "相片", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "取消", style: .cancel, handler: nil))
        self.parentViewController!.present(alert, animated: true, completion: nil)
        picker.sourceType = .photoLibrary
        picker.delegate = self
        
    }
    
    func openCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.allowsEditing = true
            self.parentViewController!.present(picker, animated: true, completion: nil)
        }
    }
    func openGallary(){
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.allowsEditing = true
        self.parentViewController!.present(picker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        if let chosenImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            print("\(chosenImage.size.width)")
//            self.profileIcon.image = chosenImage
//            ProfileEditHelper.image = chosenImage
//            self.content.info.saveBtn.backgroundColor = MainRedColor
//            //Toast(text: "請按儲存更改個人頭像").show()
//            Toast(text: "請按儲存更改個人頭像", delay: 0, duration: 2).show()
//            self.topMenu.scrollToFirstItem()
            
              self.profilePic.image = chosenImage
            
        }
        self.parentViewController!.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.parentViewController!.dismiss(animated: true, completion: nil)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
