//
//  AccountNotiCell.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 13/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

class AccountNotiCell: UITableViewCell {

    let lb1 = UILabel(fontSize: 16, color: UIColor.darkText, str: "AAA")
    let lb2 = UILabel(fontSize: 16, color: UIColor.darkText, str: "AAA")
    
    let margin:CGFloat = 20
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let arr = [lb1,lb2]
        
        var holder:UIView?
        for n in 0 ..< arr.count {
            contentView.addSubview(arr[n])
            arr[n].snp.makeConstraints({ (make)->Void in
                make.leading.trailing.equalToSuperview().inset(margin)
                if n == 0 {
                    make.top.equalToSuperview().offset(margin)
                }else{
                    make.top.equalTo(holder!.snp.bottom).offset(margin)
                }
                
                if n == arr.count - 1 {
                    make.bottom.equalToSuperview().offset(-margin)
                }
            })
            holder = arr[n]
        }
        
        let line = UIView()
        line.backgroundColor = UIColor.init(hex: "eeeeee")
        contentView.addSubview(line)
        line.snp.makeConstraints({ (make)->Void in
            make.bottom.trailing.equalToSuperview()
            make.leading.equalToSuperview().offset(margin)
            make.height.equalTo(1)
        })

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setData(data:ShopModel) {
        
    }

}
