//
//  AccountNoti.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 13/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

class AccountNoti: UIView {
    
    var inputDate:MyInputField!
    var btnSubmit:MyButton!
    var data = [NotificationInProfileModel]()
    func loadData() {
        
        lbNoResult.isHidden = true
        
        let lbdate = UILabel(fontSize: 20, color: UIColor.black,str: "日期: ")
        self.addSubview(lbdate)
        lbdate.snp.makeConstraints({ (make)->Void in
            make.top.equalToSuperview().offset(15*4)
            make.left.equalToSuperview().offset(15)
            make.height.equalTo(40)
        })
        
        inputDate = MyInputField(withType: .Date)
        self.addSubview(inputDate)
        inputDate.snp.makeConstraints({ (make)->Void in
//            make.leading.trailing.equalToSuperview().inset(15)
            make.left.equalTo(lbdate.snp_right).offset(5)
            make.top.equalToSuperview().offset(15*4)
            make.height.equalTo(40)
        })
        
        btnSubmit = MyButton(withData: ("申請請假",UIColor.white,UIColor.black))
        self.addSubview(btnSubmit)
        btnSubmit.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(inputDate.snp.bottom).offset(15*2)
            make.leading.trailing.equalToSuperview().inset(15)
            make.height.equalTo(40)
        })
        btnSubmit.onClick = {
            if (self.inputDate.text!.isEmptyStr()){
                showError(errorMessage: "請選擇請假日期")
                return
            }
            showInfo(message: "你確定要申請此假期嗎？", didOk: {
                var params = [String: Any]()
                params["date"] = self.inputDate.text
                APIHelper.apply_dayoff(withParams: params, completion: { (apply_id) in
                    showError(errorMessage: "申請成功！\n你的申請ID為:"+apply_id)
                }, fail: {
                    showError(errorMessage: "申請失敗！")
                })
            }, didCancel: {
                return
            })
        }
        
        
//        APIHelper.notificationList(withParams: [String: Any](), completion: {
//            result in
//            
//            if result.count == 0 {
//                self.tableView.isHidden = true
//                self.lbNoResult.isHidden = false
//            }else{
//                self.data = result
//                self.tableView.reloadData()
//                self.tableView.isHidden = false
//                self.lbNoResult.isHidden = true
//            }
//
//        }, fail: {})
    }

    let lbNoResult = UILabel(fontSize: 16, color: UIColor.darkText, str: "沒有結果")
    let tableView = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        
        tableView.register(AccountNotiCell.self, forCellReuseIdentifier: "AccountNotiCell")
        self.addSubview(tableView)
        tableView.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        
        lbNoResult.textAlignment = .center
        self.addSubview(lbNoResult)
        lbNoResult.snp.makeConstraints({ (make)->Void in
            //make.leading.top.trailing.equalToSuperview().inset(KEY_MARGIN*2)
            make.center.equalTo(tableView.snp.center)
        })
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
}

extension AccountNoti: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell(style: .value1, reuseIdentifier: "Cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountNotiCell", for: indexPath) as! AccountNotiCell
        cell.selectionStyle = .none
        
        let data = self.data[indexPath.row]
        cell.lb1.text = data.message
        cell.lb2.text = data.pushed
        
        return cell
    }
    
}
