//
//  ApplyLeaveViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 16/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit
import MultilineTextField
import DateTimePicker

class ApplyLeaveViewController : UIViewController, UITextViewDelegate{
    var topic:String!
    
    // datetime set default var
    var dateTimeFrom:MyInputField!
    var dateTimeTo:MyInputField!
    
    var Reason:UITextView!
//    var Reason:UITextField!
//    var Reason:MultilineTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let font = UIFont.systemFont(ofSize: UIFont.systemFontSize)

        let btnBack = "btn-back".imageView()
        self.view.addSubview(btnBack)
        btnBack.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
        })
        let backArea = UIView()
        self.view.addSubview(backArea)
        backArea.snp.makeConstraints({ (make)->Void in
            make.center.equalTo(btnBack)
            make.width.height.equalTo(44)
        })
        backArea.randomBorder()
        let back_tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
        backArea.addGestureRecognizer(back_tap)
        backArea.isUserInteractionEnabled = true
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black)
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        if let topic = topic{
            lbTitle.text = topic
        }
        
        let lbLeaveFrom = UILabel(fontSize: 16, color: .black,str: "Leave Start:")
        self.view.addSubview(lbLeaveFrom)
        lbLeaveFrom.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*8)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        

        dateTimeFrom = MyInputField(withType: .TimeFrom,view: self)
        self.view.addSubview(dateTimeFrom)
        dateTimeFrom.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*8)
//            make.left.equalTo(lbLeaveFrom.snp.right).offset(KEY_MARGIN*2)
            make.right.equalToSuperview().inset(KEY_MARGIN*4)
        })
        
        let lbLeaveTo = UILabel(fontSize: 16, color: .black,str: "Leave End:")
        self.view.addSubview(lbLeaveTo)
        lbLeaveTo.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveFrom.snp.bottom).offset(KEY_MARGIN*4)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        dateTimeTo = MyInputField(withType: .TimeTo,view: self)
        dateTimeTo.selectedDate = dateTimeFrom.selectedDate
        self.view.addSubview(dateTimeTo)
        dateTimeTo.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveFrom.snp.bottom).offset(KEY_MARGIN*4)
//            make.left.equalTo(lbLeaveTo.snp.right).offset(KEY_MARGIN*2)
            make.right.equalToSuperview().inset(KEY_MARGIN*4)
        })
        
        let lbLeaveType = UILabel(fontSize: 16, color: .black,str: "Leave Type:")
        self.view.addSubview(lbLeaveType)
        lbLeaveType.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveTo.snp.bottom).offset(KEY_MARGIN*4)
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
        })
        
        let leaveType = MyInputField(withType: .Leave)
        self.view.addSubview(leaveType)
        leaveType.textColor = UIColor.black
        leaveType.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(dateTimeTo.snp.bottom).offset(KEY_MARGIN*4)
            make.left.equalTo(lbLeaveType.snp.right).offset(KEY_MARGIN*4)
//            make.right.equalToSuperview().inset(KEY_MARGIN*4)
        })
        
        let lbReason = UILabel(fontSize: 16, color: .black,str: "Leave Reason:")
        self.view.addSubview(lbReason)
        lbReason.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(lbLeaveType.snp.bottom).offset(KEY_MARGIN*4)
            make.leading.equalToSuperview().inset(KEY_MARGIN*2)
        })
        
        Reason = UITextView()
        Reason.textColor = UIColor.lightGray
        Reason.text = "Please Input Reason..."
        Reason.textAlignment = .right
        Reason.font = UIFont.systemFont(ofSize: 16)
        Reason.delegate = self
        self.view.addSubview(Reason)
        Reason.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(leaveType.snp.bottom).offset(KEY_MARGIN*3)
            make.left.equalTo(lbReason.snp.right).offset(KEY_MARGIN*4)
            make.right.equalToSuperview().inset(KEY_MARGIN*4)
            make.height.equalTo(100)
        })
        
        
        let submitBtn = MyButton(withData: ("Apply Leave",UIColor.white,UIColor.black))
        self.view.addSubview(submitBtn)
        submitBtn.snp.makeConstraints({ (make)-> Void in
            make.top.equalTo(Reason.snp.bottom).offset(KEY_MARGIN*4)
            make.left.right.equalToSuperview().inset(KEY_MARGIN*4)
        })
        
        submitBtn.onClick = {
            if(self.dateTimeFrom.text!.isEmptyStr()){
                showError(errorMessage: "Leave Start Date...")
                return
            }
            if(self.dateTimeTo.text!.isEmptyStr()){
                showError(errorMessage: "Leave End Date...")
                return
            }
            if(leaveType.selectedLeave == nil){
                showError(errorMessage: "Leave Type...")
                return
            }
            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let tempTo:Date = self.dateTimeTo.selectedDate
            let tempFrom:Date = self.dateTimeFrom.selectedDate
            
            if(tempTo < tempFrom || tempTo == tempFrom){
                showError(errorMessage: "Start date must eailer than the end date!")
                return
            }
            showInfo(message: "Confirm to apply？\n\(self.dateTimeFrom.text!) To \(self.dateTimeTo.text!)", didOk: {
                var params = [String:Any]()
                params["operation"] = "insert"
                params["leave_type"] = leaveType.selectedLeave?.id
                //change format
                params["leave_from"] = self.dateTimeFrom.text!+":00"
                params["leave_to"] = self.dateTimeTo.text!+":00"
                if(self.Reason.text == "Please Input Reason..."){
                    params["leave_remarks"] = ""
                }else{
                    params["leave_remarks"] = self.Reason.text!
                }
                
                let lp = LoadingPopUp.addTo(vc: self)
                APIHelper.manageLeave(withParams: params, completion: { (application_id) in
                    showInfo(message: "Apply Success！\nYour Application ID is: "+application_id, didCompletion:{
                        MainViewController.me.nav.popViewController(animated: false)
                        lp.removeFromSuperview()
                    })
                    return
                }, fail: {
                    showInfo(message: "Apply Failed！", didCompletion: {
                    MainViewController.me.nav.popViewController(animated: false)
                        lp.removeFromSuperview()
                    })
                    return
                })
            }, didCancel: {
                return
            })

        }
        
        
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
    
    func textViewDidBeginEditing (_ textView: UITextView) {
        if Reason.textColor == UIColor.lightGray && Reason.isFirstResponder {
            Reason.text = nil
            Reason.textColor = .black
        }
    }
    
    func textViewDidEndEditing (_ textView: UITextView) {
        if Reason.text.isEmpty || Reason.text == "" {
            Reason.textColor = .lightGray
            Reason.text = "Please Input Reason..."
        }
    }
    
}
