//
//  AccountSettings.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 13/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

class AccountSettings: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let lbLogout = UILabel(fontSize: 18, color: UIColor.darkText, str: "登出")
        self.addSubview(lbLogout)
        lbLogout.snp.makeConstraints({ (make)->Void in
            make.top.equalToSuperview().offset(0)
            make.leading.equalToSuperview().offset(KEY_MARGIN)
            make.height.equalTo(50)
        })
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        lbLogout.addGestureRecognizer(tap)
        lbLogout.isUserInteractionEnabled = true
        
        let line = UIView()
        self.addSubview(line)
        line.backgroundColor = UIColor.lightGray
        line.snp.makeConstraints({ (make)->Void in
            make.trailing.equalToSuperview()
            make.bottom.equalTo(lbLogout)
            make.height.equalTo(1)
            make.leading.equalToSuperview().offset(KEY_MARGIN)
        })
        
        //derek
//        let lbQrcode = UILabel(fontSize: 18, color: UIColor.darkText, str: "QR Code")
//        self.addSubview(lbQrcode)
//        lbQrcode.snp.makeConstraints({ (make)->Void in
//            make.trailing.equalToSuperview()
//            make.top.equalTo(line)
//            make.height.equalTo(50)
//        })
//        let qr_tap = UITapGestureRecognizer(target: self, action: #selector(self.onQrPage(_:)))
//        lbQrcode.addGestureRecognizer(qr_tap)
//        lbQrcode.isUserInteractionEnabled = true
        
        
        let versionCode = UILabel(fontSize: 12, color: UIColor.lightGray)
        self.addSubview(versionCode)
        versionCode.snp.makeConstraints({ (make)->Void in
            make.bottom.leading.equalToSuperview().inset(KEY_MARGIN)
        })
        versionCode.text = "v\(Bundle.main.releaseVersionNumber!) (\(Bundle.main.buildVersionNumber!))"
        
//        let buildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
//        print("buildNumber - \(buildNumber)")
//        let lbBuildNumber = UILabel(fontSize: 10, color: UIColor.lightGray, str: "build number : " + buildNumber)
//        self.addSubview(lbBuildNumber)
//        lbBuildNumber.snp.makeConstraints({ (make)->Void in
//            make.leading.bottom.equalToSuperview()
//        })
        
//        let infoEmail = AccountInfoCell(withData: ("登出", "", AccountInfoType.Phone))
//        self.addSubview(infoEmail)
//        infoEmail.snp.makeConstraints({ (make)->Void in
//            make.leading.trailing.equalTo(self)
//            make.height.equalTo(50)
//            make.top.equalToSuperview().offset(KEY_MARGIN)
//        })
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        let lp = LoadingPopUp.addTo(vc: self.parentViewController!)
        APIHelper.logout(completion: {
            lp.removeFromSuperview()
            StaffLoginModel.getOne().clear()
            let defaults = UserDefaults.standard
//            defaults.removeObject(forKey: "game_play_count")
//            defaults.removeObject(forKey: "number")
//            defaults.removeObject(forKey: "name_zho")
//            ChatDataHelper.clear()
            let vc = LoginViewController()
            ViewController.me.nav.viewControllers = [vc]
        }, fail: {
            lp.removeFromSuperview()
        })
    }

    //derek- test device login
//    @objc func onQrPage(_ sender: UITapGestureRecognizer) {
//        let lp = LoadingPopUp.addTo(vc: self.parentViewController!)
//        APIHelper.checkDevice(completion: {
//            lp.removeFromSuperview()
//            let vc = QRCodeViewController()
//            MainViewController.me.nav.viewControllers = [vc]
//        }, fail: {
//            lp.removeFromSuperview()
//            APIHelper.logout(completion: {
//                showInfo(message: "重覆登入，你已被登出", didCompletion: {
//                    let vc = LoginViewController()
//                    ViewController.me.nav.viewControllers = [vc]
//                    StaffLoginModel.getOne().clear()
//                    ChatDataHelper.clear()
//                })
//            }, fail: { lp.removeFromSuperview() })
//        })
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
