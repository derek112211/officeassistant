//
//  AccountViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 16/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import SwiftIcons
import MessageUI

class AccountViewController:UIViewController,UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate{
    
    var array = [IconListModel]()
    //var array = ["申請請假","審批請假"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // init data
        let obj1 = IconListModel(name: "Apply Leave",icon: FontType.fontAwesomeSolid(.calendarMinus),color: UIColor.red)
        let obj2 = IconListModel(name: "Approve Leave",icon: FontType.fontAwesomeSolid(.fileSignature),color: UIColor.red)
        let obj3 = IconListModel(name: StaffLoginModel.getOne().phone,icon: FontType.fontAwesomeSolid(.phone),color: UIColor.black)
        let obj4 = IconListModel(name: StaffLoginModel.getOne().shop_phone,icon: FontType.fontAwesomeSolid(.building),color: UIColor.black)
        let obj5 = IconListModel(name: StaffLoginModel.getOne().shop_name,icon: FontType.fontAwesomeSolid(.shoppingBag),color: UIColor.black)
        let obj6 = IconListModel(name: StaffLoginModel.getOne().email,icon: FontType.fontAwesomeSolid(.envelopeOpen),color: UIColor.black)
        array.append(obj3)
        array.append(obj4)
        array.append(obj5)
        array.append(obj6)
        array.append(obj1)
        array.append(obj2)

        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black, str: "Account")
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })

        let lbname = UILabel(fontSize: BIG_TEXT, color: UIColor.black, str: StaffLoginModel.getOne().getName())
        lbname.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbname)
        lbname.snp.makeConstraints({ (make)->Void in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*4)
            
        })
        
        let lbshop = UILabel(fontSize: BIG_TEXT, color: UIColor.black, str: StaffLoginModel.getOne().shop_name)
        lbshop.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbshop)
        lbshop.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbname.snp.bottom).offset(KEY_MARGIN*4)
            make.centerX.equalTo(self.view.snp.centerX)
        })
        
        //init table view
        let fullScreenSize = self.view.bounds.size
        let tbview = UITableView(frame:CGRect(x:0, y:20, width: fullScreenSize.width, height: fullScreenSize.height-20),style: .grouped)

        //cells setting
        tbview.register(ListCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        tbview.delegate =  self
        tbview.dataSource = self
        
        tbview.separatorStyle = .singleLine
        tbview.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        tbview.allowsSelection = true
        tbview.allowsMultipleSelection = false
        
        self.view.addSubview(tbview)
        tbview.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbshop.snp.bottom).offset(5)
//            make.height.equalTo(fullScreenSize.height-20)
            make.width.equalTo(fullScreenSize.width)
            make.bottom.equalToSuperview()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ListCell
        cell.selectionStyle = .none
        
        cell.setData(data: array[indexPath.row])
        
        print("CALLED " + (String)(indexPath.row))
        
        return cell
    }
    
    //on tab send info
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = indexPath.row
        if(p == 0){
            let number = StaffLoginModel.getOne().phone
            // null check
            if(number == "NA"){
                return
            }
            if let url = URL(string: "tel://\(number)") {
                           UIApplication.shared.openURL(url)
                       }
        }else if(p==1){
            let number = StaffLoginModel.getOne().shop_phone
            // null check
            if(number == "NA"){
                return
            }
            if let url = URL(string: "tel://\(number)") {
                           UIApplication.shared.openURL(url)
                       }
        }else if (p == 3){
            // null check
            if(StaffLoginModel.getOne().email == "" || StaffLoginModel.getOne().email == "NA"){
                return
            }
            let mailComposeViewController = configureMailComposer()
              if MFMailComposeViewController.canSendMail(){
                  self.present(mailComposeViewController, animated: true, completion: nil)
              }else{
                  print("Can't send email")
              }
        }
        else if(p == 4){
            let controller = ApplyLeaveViewController()
            controller.topic  = array[p].name
            MainViewController.me.nav.pushViewController(controller, animated: false)
        }else if p == 5{
            let controller = ApproveLeaveViewController()
            controller.topic  = array[p].name
            var params = [String:Any]()
            params["operation"] = "check"
            let lp = LoadingPopUp.addTo(vc: self)
            APIHelper.manageLeave(withParams: params, completion:  { (pos) in
                params["position"] = pos
                params["operation"] = "list"
                APIHelper.getLeaveUserList(withParams: params, completion: { (array) in
                    if(array.count > 0){
                        controller.array = array
                    }else{
                        controller.array.append(LeaveUserModel())
                    }
                    
                    controller.position = pos
                    MainViewController.me.nav.pushViewController(controller, animated: false)
                    lp.removeFromSuperview()
                }, fail: {
                    lp.removeFromSuperview()
                    return
                })
            }, fail: {
                lp.removeFromSuperview()
                return
            })
        }

    }

    func configureMailComposer() -> MFMailComposeViewController{
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients([StaffLoginModel.getOne().email])
        return mailComposeVC
    }
    //MARK: - MFMail compose method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
