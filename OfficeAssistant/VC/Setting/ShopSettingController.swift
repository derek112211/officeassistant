//
//  ShopViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 11/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation

class ShopSettingViewController : UIViewController{
//    let myContent = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.view.addSubview(myContent)
//        myContent.snp.makeConstraints({ (make)->Void in
//            make.leading.trailing.equalTo(view)
//            make.top.equalTo(topBG.snp.bottom)
//            make.bottom.equalToSuperview().offset(-(BOTTOM_TAB_H+bottomPadding))
//        })
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.gray, str: "Setting")
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        let infoid:AccountInfoCell
        infoid = AccountInfoCell(withData: ("Shop ID", ShopLoginModel.getOne().id, AccountInfoType.Host))
        self.view.addSubview(infoid)
        infoid.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(view)
            make.top.equalTo(lbTitle.snp.bottom).offset(15*2)
        })
        
        let infoNo:AccountInfoCell
        infoNo = AccountInfoCell(withData: ("Shop No.", ShopLoginModel.getOne().shop_no, AccountInfoType.Host))
        self.view.addSubview(infoNo)
        infoNo.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(view)
            make.top.equalTo(infoid.snp.bottom).offset(15)
        })
        
        let infoIP:AccountInfoCell
        infoIP = AccountInfoCell(withData: ("Shop IP", ShopLoginModel.getOne().ip_address, AccountInfoType.Host))
        self.view.addSubview(infoIP)
        infoIP.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(view)
            make.top.equalTo(infoNo.snp.bottom).offset(15)
        })
        
        let infodevice:AccountInfoCell
        infodevice = AccountInfoCell(withData: ("UUID", ShopLoginModel.getOne().device_code, AccountInfoType.Host))
        self.view.addSubview(infodevice)
        infodevice.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(view)
            make.top.equalTo(infoIP.snp.bottom).offset(15)
        })
        
        let lbLogout = UILabel(fontSize: 18, color: UIColor.darkText, str: "Logout")
        self.view.addSubview(lbLogout)
        lbLogout.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(infodevice.snp.bottom).offset(15*2)
            make.leading.equalToSuperview().offset(KEY_MARGIN)
            make.height.equalTo(50)
        })
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        lbLogout.addGestureRecognizer(tap)
        lbLogout.isUserInteractionEnabled = true
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        let lp = LoadingPopUp.addTo(vc: self)
        APIHelper.logout(completion: {
            lp.removeFromSuperview()
            ShopLoginModel.getOne().clear()
            StaffLoginModel.getOne().clear()
            let vc = LoginViewController()
            ViewController.me.nav.viewControllers = [vc]
        }, fail: {
            lp.removeFromSuperview()
        })
    }
}
