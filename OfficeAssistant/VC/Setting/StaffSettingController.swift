//
//  StaffSettingController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 16/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//


import Foundation

class StaffSettingViewController : UIViewController{
    //    let myContent = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black, str: "Setting")
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        let infoid:AccountInfoCell
        let device_code = StaffLoginModel.getOne().device_code
        infoid = AccountInfoCell(withData: ("UUID", device_code, AccountInfoType.DeviceCode))
        self.view.addSubview(infoid)
        infoid.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(view)
            make.top.equalTo(lbTitle.snp.bottom).offset(15*2)
        })
        
//        let infoid2 = AccountInfoCell(withData: ("UUID資訊", device_code, AccountInfoType.DeviceCode))
//        self.view.addSubview(infoid2)
//        infoid2.snp.makeConstraints({ (make)->Void in
//            make.leading.trailing.equalTo(view)
//            make.height.equalTo(30)
//            make.top.equalTo(infoid.snp.bottom).offset(15*2)
//        })
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let infoNo:AccountInfoCell
        infoNo = AccountInfoCell(withData: ("App Version", appVersion!, AccountInfoType.Staff))
        self.view.addSubview(infoNo)
        infoNo.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(view)
            make.top.equalTo(infoid.snp.bottom).offset(15)
        })
        
        let lbLogout = UILabel(fontSize: 18, color: UIColor.darkText, str: "Logout")
        self.view.addSubview(lbLogout)
        lbLogout.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(infoNo.snp.bottom).offset(15)
            make.trailing.equalToSuperview().offset(-KEY_MARGIN)
            make.height.equalTo(50)
        })
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        lbLogout.addGestureRecognizer(tap)
        lbLogout.isUserInteractionEnabled = true
        
        let line = UIView()
        self.view.addSubview(line)
        line.backgroundColor = UIColor.lightGray
        line.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(view).offset(KEY_MARGIN)
            make.top.equalTo(lbLogout.snp.bottom).offset(KEY_MARGIN)
            make.height.equalTo(1)
        })
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        let lp = LoadingPopUp.addTo(vc: self)
        APIHelper.logout(completion: {
            lp.removeFromSuperview()
            ShopLoginModel.getOne().clear()
            StaffLoginModel.getOne().clear()
            let vc = LoginViewController()
            ViewController.me.nav.viewControllers = [vc]
        }, fail: {
            lp.removeFromSuperview()
        })
    }
}
