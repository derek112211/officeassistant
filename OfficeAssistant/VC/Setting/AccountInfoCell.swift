//
//  AccountInfoCell.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 12/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

enum AccountInfoType {
    case Phone
    case Email
    case DOB
    case DeviceCode
    case Host
    case Staff
}

typealias AccountInfoItem = (key: String, name: String, type: AccountInfoType)

class AccountInfoCell: UIView {

    let tf = UITextField()
    var lb2:UILabel!
    
    init(withData data:AccountInfoItem) {
        super.init(frame: CGRect.zero)
        
        let lb = UILabel(fontSize: 16, color: UIColor.gray, str: data.key)
        self.addSubview(lb)
        lb.snp.makeConstraints({ (make)->Void in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().offset(KEY_MARGIN)
        })
        
//        if(data.type == AccountInfoType.DeviceCode){
//            lb2 = UILabel(fontSize: 5, color: UIColor.gray, str: data.name)
//        }else{
//            lb2 = UILabel(fontSize: 16, color: UIColor.gray, str: data.name)
//        }
        let lb2 = UILabel(fontSize: 16, color: UIColor.gray, str: data.name)
        self.addSubview(lb2)
        lb2.snp.makeConstraints({ (make)->Void in
//            make.top.bottom.equalToSuperview()
            make.top.equalTo(lb.snp.bottom).offset(KEY_MARGIN*2)
            make.trailing.equalToSuperview().offset(-KEY_MARGIN)
        })
        
        tf.textAlignment = .right
        tf.text = data.name
        self.addSubview(tf)
        tf.snp.makeConstraints({ (make)->Void in
//            make.top.bottom.equalToSuperview()
            make.top.equalTo(lb.snp.bottom).offset(KEY_MARGIN)
            make.trailing.equalToSuperview().offset(-KEY_MARGIN)
            //make.leading.equalTo(lb.snp.trailing)
        })
        tf.isHidden = true
        tf.randomBorder()
        
        if data.type == .Email {
            lb2.isHidden = true
            tf.isHidden = false
        }
        
        if data.type == .DOB {
            if data.name.isEmptyStr() {
                lb2.isHidden = true
                tf.isHidden = false
            }
        }
        
        let line = UIView()
        self.addSubview(line)
        line.backgroundColor = UIColor.lightGray
        line.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lb2.snp.bottom).offset(KEY_MARGIN)
            make.bottom.equalToSuperview().offset(KEY_MARGIN*2)
            make.bottom.trailing.equalToSuperview()
            make.height.equalTo(1)
            make.leading.equalToSuperview().offset(KEY_MARGIN)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
