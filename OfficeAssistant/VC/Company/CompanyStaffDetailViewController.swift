//
//  CompanyStaffDetail.swift
//  EM_Ios_new
//
//  Created by SocialMind on 31/12/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import SwiftIcons
import MessageUI

class CompanyStaffDetailViewController:UIViewController,UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate{
    var staff_detail:CompanyStaffModel!
    var array = [IconListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // init data
        array.append(IconListModel(name: staff_detail.phone,icon: FontType.fontAwesomeSolid(.phone),color: UIColor.black))
        array.append(IconListModel(name: staff_detail.shop_phone,icon: FontType.fontAwesomeSolid(.phone),color: UIColor.black))
        array.append(IconListModel(name: staff_detail.shop_name,icon: FontType.fontAwesomeSolid(.shoppingBag),color: UIColor.black))
        array.append(IconListModel(name: staff_detail.email,icon: FontType.fontAwesomeSolid(.envelopeOpen),color: UIColor.black))
        
        let btnBack = "btn-back".imageView()
        self.view.addSubview(btnBack)
        btnBack.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
        })
        let backArea = UIView()
        self.view.addSubview(backArea)
        backArea.snp.makeConstraints({ (make)->Void in
            make.center.equalTo(btnBack)
            make.width.height.equalTo(44)
        })
        backArea.randomBorder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
        backArea.addGestureRecognizer(tap)
        backArea.isUserInteractionEnabled = true
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black,str: staff_detail.staff_number)
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        lbTitle.sizeToFit()
        lbTitle.adjustsFontSizeToFitWidth = true
        
        let lbname = UILabel(fontSize: BIG_TEXT, color: UIColor.black, str: staff_detail.getFullName())
        lbname.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbname)
        lbname.snp.makeConstraints({ (make)->Void in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*4)
            
        })
        
        //init table view
        let fullScreenSize = self.view.bounds.size
        let tbview = UITableView(frame:CGRect(x:0, y:20, width: fullScreenSize.width, height: fullScreenSize.height-20),style: .grouped)
        
        //cells setting
        tbview.register(ListCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        tbview.delegate =  self
        tbview.dataSource = self
        
        tbview.separatorStyle = .singleLine
        tbview.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        tbview.allowsSelection = true
        tbview.allowsMultipleSelection = false
        
        self.view.addSubview(tbview)
        tbview.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbname.snp.bottom).offset(KEY_MARGIN*4)
            make.width.equalTo(fullScreenSize.width)
            make.bottom.equalToSuperview()
        })
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ListCell
        cell.selectionStyle = .none
        
        cell.setData(data: array[indexPath.row])
        
        print("CALLED " + (String)(indexPath.row))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = indexPath.row
        var number = ""
        if(p == 0 || p == 1){
            switch p {
            case 0:
                number = staff_detail.phone
                break
            case 1:
                number = staff_detail.shop_phone
                break
            default:
                break
            }
            if(number == "NA"){
                return
            }
            if let url = URL(string: "tel://\(number)") {
                           UIApplication.shared.openURL(url)
            }
        }else if(p == 3){
            let mailComposeViewController = configureMailComposer()
              if MFMailComposeViewController.canSendMail(){
                  self.present(mailComposeViewController, animated: true, completion: nil)
              }else{
                  print("Can't send email")
              }
        }
    }
    
    func configureMailComposer() -> MFMailComposeViewController{
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients([staff_detail.email])
        return mailComposeVC
    }
    
    //MARK: - MFMail compose method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

}
