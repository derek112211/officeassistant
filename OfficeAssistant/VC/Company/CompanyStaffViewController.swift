//
//  CompanyStaffViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 30/12/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
class CompanyStaffViewController: UIViewController,UITableViewDelegate, UITableViewDataSource{
    var lbTitle:UILabel!
    var array = [CompanyStaffModel]()
    var company_title:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btnBack = "btn-back".imageView()
        self.view.addSubview(btnBack)
        btnBack.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
        })
        let backArea = UIView()
        self.view.addSubview(backArea)
        backArea.snp.makeConstraints({ (make)->Void in
            make.center.equalTo(btnBack)
            make.width.height.equalTo(44)
        })
        backArea.randomBorder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
        backArea.addGestureRecognizer(tap)
        backArea.isUserInteractionEnabled = true
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black,str: company_title)
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        //init table view
        let fullScreenSize = self.view.bounds.size
        let tbview = UITableView(frame:CGRect(x:0, y:20, width: fullScreenSize.width, height: fullScreenSize.height-20),style: .grouped)
        
        //cells setting
        tbview.register(ListCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        tbview.delegate =  self
        tbview.dataSource = self
        
        tbview.separatorStyle = .singleLine
        tbview.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        tbview.allowsSelection = true
        tbview.allowsMultipleSelection = false
        
        self.view.addSubview(tbview)
        tbview.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*4)
            make.width.equalTo(fullScreenSize.width)
            make.bottom.equalToSuperview()
        })
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ListCell
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = array[indexPath.row].getDisplayName()
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        
        print("CALLED " + (String)(indexPath.row))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = indexPath.row
        let staff_obj = array[p]
        var params = [String: Any]()
        
//        print(staff_obj.phone)
        let lp = LoadingPopUp.addTo(vc: self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let controller = CompanyStaffDetailViewController()
            controller.staff_detail = staff_obj
            MainViewController.me.nav.pushViewController(controller, animated: false)
            lp.removeFromSuperview()
        }

    }
}
