//
//  CompanyViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 30/12/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
class CompanyViewController : UIViewController,UITableViewDelegate, UITableViewDataSource{
    
    var array = [CompanyShopModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black, str: "Company")
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        let lbbreak = UILabel(fontSize: BIG_TEXT, color: UIColor.black, str:" ")
        self.view.addSubview(lbbreak)
        lbbreak.snp.makeConstraints({ (make)->Void in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*4)
        })
        
        //init table view
        let fullScreenSize = self.view.bounds.size
        let tbview = UITableView(frame:CGRect(x:0, y:20, width: fullScreenSize.width, height: fullScreenSize.height-20),style: .grouped)
        
        //cells setting
        tbview.register(ListCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        tbview.delegate =  self
        tbview.dataSource = self
        
        tbview.separatorStyle = .singleLine
        tbview.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        tbview.allowsSelection = true
        tbview.allowsMultipleSelection = false
        
        self.view.addSubview(tbview)
        tbview.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbbreak.snp.bottom).inset(KEY_MARGIN*4)
            make.width.equalTo(fullScreenSize.width)
            make.bottom.equalToSuperview()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ListCell
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = array[indexPath.row].name
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        
        print("CALLED " + (String)(indexPath.row))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = indexPath.row
        let shop_id = array[p].shop_id
        var params = [String: Any]()
        params["action"] = "list_staff"
        params["shop_id"] = shop_id
        
        let controller = CompanyStaffViewController()
        let lp = LoadingPopUp.addTo(vc: self)
        
        APIHelper.getCompanyShop(withParams: params, success: { (staffs) in
            controller.array = staffs as! [CompanyStaffModel]
            controller.company_title  = self.array[p].name
            MainViewController.me.nav.pushViewController(controller, animated: false)
            lp.removeFromSuperview()
        }, fail: {
            showError(errorMessage: "Get Available Staff Fail")
            lp.removeFromSuperview()
        })

    }

}
