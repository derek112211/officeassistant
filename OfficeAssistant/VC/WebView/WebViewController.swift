//
//  WebViewController.swift
//  CIC_2
//
//  Created by kelvin.lee(VTL) on 19/03/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    var toLoadURLStr:String?
    let webView = WKWebView()
    
    var lp:UIView?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        lp = LoadingPopUp.addTo(view: self.webView)
        
        //toLoadURLStr = "http://www.expertmedical.com.hk/enquires"
        if let urlStr = self.toLoadURLStr, let url = URL(string: urlStr) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.webView.navigationDelegate = self
                self.webView.load(URLRequest(url: url))
            }
        }else{
            lp?.removeFromSuperview()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        
        self.view.insertSubview(webView, at: 0)
        webView.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        
        webView.backgroundColor = UIColor.clear
        //webView.randomBorder()
        
//        let btnBack = "btn-back".imageView()
//        self.view.addSubview(btnBack)
//        btnBack.snp.makeConstraints({ (make)->Void in
//            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
//            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
//        })
//        var tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
//        btnBack.addGestureRecognizer(tap)
//        btnBack.isUserInteractionEnabled = true
        
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: {})
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        lp?.removeFromSuperview()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        lp?.removeFromSuperview()
    }
    
    func webView(_ webView: WKWebView,
                 didReceive challenge: URLAuthenticationChallenge,
                 completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)
    {
        if(challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust)
        {
            let cred = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(.useCredential, cred)
        }
        else
        {
            completionHandler(.performDefaultHandling, nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
