//
//  ListViewController.swift
//  ExpertMedical
//
//  Created by SocialMind on 25/6/2019.
//  Copyright © 2019 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

class ListViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{

    
    let tableView = PagingTableView()
    //init table view data
//    var info = [String]()
//    info.append("QR 編碼")
//    info.append("打卡")
//    var info = ["QR 編碼","打卡1","打卡2","打卡3","打卡4","打卡5"]
    var array = [StaffDataModel]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let fullScreenSize = UIScreen.main.bounds.size
        //print("Main: "+StaffLoginModel.getOne().id)
        //test data
        let a = StaffDataModel(id: "1", date: "8964", name: "Object A", description: "This is A")
        array.append(a)
        let b = StaffDataModel(id: "2", date: "8964", name: "Object B", description: "This is B")
        array.append(b)
        let c = StaffDataModel(id: "3", date: "8964", name: "Object C", description: "This is C")
        array.append(c)
        let d = StaffDataModel(id: "4", date: "8964", name: "Object D", description: "This is D")
        array.append(d)
        let e = StaffDataModel(id: "5", date: "8964", name: "Object E", description: "This is E")
        array.append(e)
        let f = StaffDataModel(id: "6", date: "8964", name: "Object F", description: "This is F")
        array.append(f)
        let g = StaffDataModel(id: "7", date: "8964", name: "Object G", description: "This is G")
        array.append(g)
        let h = StaffDataModel(id: "8", date: "8964", name: "Object H", description: "This is H")
        array.append(h)
        let i = StaffDataModel(id: "9", date: "8964", name: "Object I", description: "This is I")
        array.append(i)
        // Do any additional setup after loading the view.
        
        //        let box = UIView()
        //        box.backgroundColor = UIColor.red
        //        self.view.addSubview(box)
        //        box.snp.makeConstraints({ (make)->Void in
        //            make.width.height.equalTo(100)
        //            make.top.equalTo(self.topLayoutGuide.snp.bottom)
        //        })
        

        
        //init table view
        let tbview = UITableView(frame:CGRect(x:0, y:20, width: fullScreenSize.width, height: fullScreenSize.height-20),style: .grouped)
        
        //cells setting
        tbview.register(StaffCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        tbview.delegate =  self
        tbview.dataSource = self
        
        tbview.separatorStyle = .singleLine
        tbview.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        tbview.allowsSelection = true
        tbview.allowsMultipleSelection = false
        
        self.view.addSubview(tbview)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! StaffCell
        cell.selectionStyle = .none
        
        cell.setData(data: array[indexPath.row])
        
        // accessory Type setting
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                cell.accessoryType = .checkmark
            } else if indexPath.row == 1 {
                cell.accessoryType = .detailButton
            } else if indexPath.row == 2 {
                cell.accessoryType =
                    .detailDisclosureButton
            } else if indexPath.row == 3 {
                cell.accessoryType = .disclosureIndicator
            }
        }
        
        print("CALLED " + (String)(indexPath.row))
        
        return cell
    }
    
    //segue
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if(segue.identifier == "segue"){
//            let controller = segue.destination as? ListCellController
//            let indexPath = self.tableView.indexPathForSelectedRow
//            controller?.id = array[indexPath!.row].id
//            controller?.name  = array[indexPath!.row].name
//            controller?.my_description = array[indexPath!.row].my_description
//            controller?.date = array[indexPath!.row].date
//        }
//    }
    
    //on tab send info
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = indexPath.row
        let controller = SingleCellController()

        controller.id = array[p].id
        controller.name  = array[p].name
        controller.my_description = array[p].my_description
        controller.date = array[p].date
        MainViewController.me.nav.pushViewController(controller, animated: true)
    }
}
