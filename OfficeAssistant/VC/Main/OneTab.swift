//
//  OneTab.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 11/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import SwiftIcons

//typealias OneTabData = (on_icon: FontType,off_icon:FontType, name: String)
typealias OneTabData = (icon: FontType,color: UIColor, name: String)

class OneTab: UIView {
    
    var data:OneTabData?
    var onImg:UIImage?
    var offImg:UIImage?
    var onColor = UIColor.init(hex: "8D847F")
    var offColor = UIColor.init(hex: "D3C3BB")
    
    var icon:UIImageView!
    var lb:UILabel!
    
    init(withData data:OneTabData) {
        super.init(frame: CGRect.zero)
        
        self.data = data
        onImg = UIImage.init(icon: data.icon, size: CGSize(width: 70, height: 70),textColor: data.color)
        offImg = UIImage.init(icon: data.icon, size: CGSize(width: 70, height: 70),textColor: UIColor.black)
        
        icon = UIImageView()
        self.addSubview(icon)
        icon.snp.makeConstraints({ (make)->Void in
            make.width.height.equalTo(25)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(KEY_MARGIN)
        })
        icon.image = offImg
        
        lb = UILabel(fontSize: 10, color: offColor)
        lb.numberOfLines = 0
        lb.lineBreakMode = .byWordWrapping
        lb.textAlignment = .center
        lb.text = data.name
        self.addSubview(lb)
        lb.snp.makeConstraints({ (make)->Void in
            make.centerX.equalToSuperview()
            make.top.equalTo(icon.snp.bottom).offset(0)
            make.width.equalTo(sw/5)
            make.bottom.equalToSuperview().offset(-bottomPadding)
        })
        
    }
    
    func setActive(f:Bool) {
        if f {
            icon.image = onImg
            lb.textColor = onColor
        }else{
            icon.image = offImg
            lb.textColor = offColor
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
