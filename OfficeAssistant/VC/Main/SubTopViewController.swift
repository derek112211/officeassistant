//
//  SubTopViewController.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 10/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

typealias SubTopData = (title: String, list: [String])

class SubTopViewController: UIViewController{
    
    var topConstraint_landscape: [NSLayoutConstraint]! // for handling status bar
    var topConstraint_portrait: [NSLayoutConstraint]! // for handling status bar
    
    let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.darkText)
    var sv:UIScrollView!
    var topBG:UIView!
    var data:SubTopData?
    
    let contentView = UIView()
    var tabList = [TopTabLabel]()
    
    func initTop() {
        var arr = data!.list //[String]()
        //        arr.append(("全部"))
        //        arr.append(("媒體分享"))
        //        arr.append(("集團信息"))
        //        arr.append(("社會責任"))
        
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        //lbTitle.font = UIFont(name:"HelveticaNeue-Bold", size: BIG_TEXT)
        
        var holder:UIView?
        for n in 0 ..< arr.count {
            let box = TopTabLabel(withText: arr[n])
            contentView.addSubview(box)
            box.snp.makeConstraints({ (make)->Void in
                make.top.equalToSuperview()
                if n == 0 {
                    make.leading.equalToSuperview().offset(KEY_MARGIN*2)
                }else{
                    make.leading.equalTo(holder!.snp.trailing).offset(KEY_MARGIN*3)
                }
                
                if n == arr.count-1 {
                    make.trailing.equalToSuperview()
                }
                make.height.equalToSuperview()
            })
            holder = box
            
            tabList.append(box)
            
            box.tag = n
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
            box.addGestureRecognizer(tap)
            box.isUserInteractionEnabled = true
        }
        
        tabList[0].setActive(f: true)
        
        lbTitle.text = data?.title //"資訊科技"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        topBG = UIView()
        self.view.addSubview(topBG)
        
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints{
            $0.leading.equalToSuperview().offset(KEY_MARGIN*2)
            //$0.top.equalTo(0)//+ KEY_MARGIN*2 )
            $0.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
            
            //.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
            //.equalTo(self.topLayoutGuide.length) //.equalToSuperview().offset(0) //statusBarH + KEY_MARGIN*2)
            
        }
        
        sv = UIScrollView()
        self.view.addSubview(sv)
        sv.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(view)
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN)
        })
        sv.showsHorizontalScrollIndicator = false
        
        sv.addSubview(contentView)
        contentView.snp.makeConstraints({ (make)->Void in
            make.trailing.leading.equalTo(sv)
            make.top.equalTo(sv)
            make.height.equalToSuperview()
        })
        
        topBG.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(sv.snp.bottom).offset(KEY_MARGIN*2)
        })
        topBG.backgroundColor = UIColor.init(hex: "F7F7F7")
        
//        let box = UIView()
//        box.layer.borderColor = UIColor.red.cgColor
//        box.layer.borderWidth = 10
//        self.view.addSubview(box)
//        box.snp.makeConstraints({ (make)->Void in
//            make.top.equalTo(self.topLayoutGuide.snp.bottom)
//            make.leading.trailing.equalToSuperview()
//            make.bottom.equalTo(self.bottomLayoutGuide.snp.top)
//
//        })

    }
    
    func setTap(tagIndex:Int) {
        print("onTap")
        changeTab(index: tagIndex, didNeedChange: {
            for one in self.tabList {
                one.setActive(f: false)
            }
            self.tabList[tagIndex].setActive(f: true)
        })
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        print("onTap")
        changeTab(index: sender.view!.tag, didNeedChange: {
            for one in self.tabList {
                one.setActive(f: false)
            }
            (sender.view! as! TopTabLabel).setActive(f: true)
        })
    }
    
    func changeTab(index:Int, didNeedChange:@escaping (()->())) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
