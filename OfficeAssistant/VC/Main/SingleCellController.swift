//
//  ListCellController.swift
//  ExpertMedical
//
//  Created by SocialMind on 26/6/2019.
//  Copyright © 2019 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class SingleCellController : UIViewController, AVCaptureMetadataOutputObjectsDelegate{
    var id:String!
    var name:String!
    var date:String!
    var my_description: String!
    //var Scanner:UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lbid = UILabel(fontSize: 20, color: UIColor.black)
        self.view.addSubview(lbid)
        lbid.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(40)
            make.centerY.equalTo(statusBarH + 100)
            make.height.equalTo(50)
            make.left.equalToSuperview().offset(10);
            make.right.equalToSuperview().offset(-10);
        })
        
        let lbname = UILabel(fontSize: 40, color: UIColor.black)
        self.view.addSubview(lbname)
        lbname.snp.makeConstraints({(make)->Void in
            make.top.equalTo(lbid.snp.bottom).offset(KEY_MARGIN*2)
            make.height.equalTo(30)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(10)
        })
        
        let lbdesc = UILabel(fontSize: 40, color: UIColor.black)
        self.view.addSubview(lbdesc)
        lbdesc.snp.makeConstraints({(make)->Void in
            make.top.equalTo(lbname.snp.bottom).offset(KEY_MARGIN*2)
            make.height.equalTo(30)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(10)
        })
        
        let btnList = MyButton(withData: ("Show More List",UIColor.white,UIColor.black))
        self.view.addSubview(btnList)
        btnList.snp.makeConstraints({(make)->Void in
            make.top.equalTo(lbdesc.snp.bottom).offset(KEY_MARGIN*2)
            make.height.equalTo(30)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(10)
        })
        btnList.onClick = {
            let controller = ListCellController()
            MainViewController.me.nav.pushViewController(controller, animated: true)
        }
        
        
        let btnBack = "btn-back".imageView()
        self.view.addSubview(btnBack)
        btnBack.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
        })
        let backArea = UIView()
        self.view.addSubview(backArea)
        backArea.snp.makeConstraints({ (make)->Void in
            make.center.equalTo(btnBack)
            make.width.height.equalTo(44)
        })
        backArea.randomBorder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
        backArea.addGestureRecognizer(tap)
        backArea.isUserInteractionEnabled = true
        
        if let id = id, let name = name, let date = date, let my_description = my_description{
            lbid.text = id
            lbname.text = name
            lbdesc.text = my_description
        }

    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }

    
}
