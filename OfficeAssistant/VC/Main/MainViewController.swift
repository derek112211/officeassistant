//
//  MainViewController.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 10/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import QRCodeReader

class MainViewController: UIViewController, HomeTabViewDelegate,StaffTabViewDelegate {
    let nav = UINavigationController()
    var utabBar:HomeTabView!
    var stabBar:StaffTabView!
    static var me:MainViewController!
    
    override func viewDidAppear(_ animated: Bool) {
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        MainViewController.me = self
        
        
        
        nav.isNavigationBarHidden = true
        self.addChildViewController(nav)
        self.view.addSubview(nav.view)
        nav.view.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        nav.didMove(toParentViewController: self)

        let lp = LoadingPopUp.addTo(vc: self)
        if(StaffLoginModel.isLoggedIn()){
            let vc = AccountViewController()
            self.nav.viewControllers = [vc]
            lp.removeFromSuperview()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.stabBar = StaffTabView()
                self.view.addSubview(self.stabBar)
                self.stabBar.snp.makeConstraints{
                    $0.leading.trailing.bottom.equalToSuperview()
                    $0.height.equalTo(BOTTOM_TAB_H+bottomPadding)
                }
                self.stabBar.delegate = self
                self.stabBar.setActive(index: 3)
            }
        }else if(ShopLoginModel.isLoggedIn()){
            let vc = QRCodeViewController()
            self.nav.viewControllers = [vc]
            lp.removeFromSuperview()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.utabBar = HomeTabView()
                self.view.addSubview(self.utabBar)
                self.utabBar.snp.makeConstraints{
                    $0.leading.trailing.bottom.equalToSuperview()
                    $0.height.equalTo(BOTTOM_TAB_H+bottomPadding)
                }
                self.utabBar.delegate = self
            }
        }else{
            let vc = LoginViewController()
            self.nav.viewControllers = [vc]
            lp.removeFromSuperview()
        }


    }
    func didClick_shop(index: Int) {
        if index == 0 {
            let lp = LoadingPopUp.addTo(vc: self)
            let vc = QRCodeViewController()
            self.nav.viewControllers = [vc]
            self.utabBar.setActive(index: 0)
            lp.removeFromSuperview()
        }
        if index == 1{
            let lp = LoadingPopUp.addTo(vc: self)
            let vc = QRScannerView()
            self.nav.viewControllers = [vc]
            lp.removeFromSuperview()
            self.utabBar.setActive(index: 1)
        }
        if index == 2{
            let lp = LoadingPopUp.addTo(vc: self)
            let vc = ShopSettingViewController()
            self.nav.viewControllers = [vc]
            lp.removeFromSuperview()
            self.utabBar.setActive(index: 2)
        }
    }
    
    func didClick(index: Int) {
        if index == 0 {
            let lp = LoadingPopUp.addTo(vc: self)
            var params = [String: Any]()
            params["action"] = "list_shop"
            let vc = CompanyViewController()
            self.stabBar.setActive(index: 0)
            APIHelper.getCompanyShop(withParams: params, success: { (shops) in
                vc.array = shops as! [CompanyShopModel]
                self.nav.viewControllers = [vc]
                lp.removeFromSuperview()
            }, fail: {
                showError(errorMessage: "Get Shop List Failed")
                lp.removeFromSuperview()
            })
        }
        if index == 1{
            let lp = LoadingPopUp.addTo(vc: self)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                let vc = ReportViewController()
                self.nav.viewControllers = [vc]
                lp.removeFromSuperview()
                self.stabBar.setActive(index: 1)
            }
        }
        if index == 2{
            let lp = LoadingPopUp.addTo(vc: self)
            if #available(iOS 10.2, *) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    let vc = CheckinViewController()
                    self.nav.viewControllers = [vc]
                    self.stabBar.setActive(index: 2)
                    lp.removeFromSuperview()
                }
            } else {
                // Fallback on earlier versions
                print("Fail to load CAMERA Page - Please check iOS version")
                let alert = UIAlertController(title: "Fail to load CAMERA Page", message: "Please check iOS version. (>10.2)", preferredStyle: .alert)
                self.present(alert, animated: true)
            }
            
        }
        if index == 3{
            let lp = LoadingPopUp.addTo(vc: self)
            APIHelper.profile(completion: {
                let vc = AccountViewController()
                self.nav.viewControllers = [vc]
                lp.removeFromSuperview()
                self.stabBar.setActive(index: 3)
            }, fail: {
                StaffLoginModel.getOne().clear()
                let vc = LoginViewController()
                ViewController.me.nav.viewControllers = [vc]
                lp.removeFromSuperview() })
        }
        if index == 4 {
            let lp = LoadingPopUp.addTo(vc: self)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                let vc = StaffSettingViewController()
                self.nav.viewControllers = [vc]
                lp.removeFromSuperview()
                self.stabBar.setActive(index: 4)
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
