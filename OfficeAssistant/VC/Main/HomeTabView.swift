//
//  HomeTabView.swift
//  CIC_2
//
//  Created by kelvin.lee(VTL) on 02/03/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import SwiftIcons

//typealias HomeTabsItem = (on_icon: FontType,off_icon:FontType, name: String)
typealias HomeTabsItem = (icon: FontType,tab_color: UIColor, name: String)

protocol HomeTabViewDelegate {
    func didClick_shop(index:Int)
}

class HomeTabView: UIView {
    
    var delegate:HomeTabViewDelegate?
    var arr = [HomeTabsItem]()
    var list = [OneTab]()
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        
        self.backgroundColor = UIColor.white
        
//        arr.append((FontType.fontAwesomeSolid(.compass),FontType.fontAwesomeRegular(.compass),"QRCode"))
//        arr.append((FontType.fontAwesomeSolid(.image),FontType.fontAwesomeRegular(.image),"打卡"))
//        arr.append((FontType.fontAwesomeSolid(.questionCircle),FontType.fontAwesomeRegular(.questionCircle),"設定"))

        arr.append((FontType.fontAwesomeSolid(.qrcode),UIColor.red,"QRCode"))
        arr.append((FontType.fontAwesomeSolid(.streetView),UIColor.red,"Check-in"))
        arr.append((FontType.fontAwesomeSolid(.cog),UIColor.red,"Setting"))
        let totalObject = (Int)(arr.count)
        
        for n in 0 ..< totalObject {
//            let view = OneTab(withData: (arr[n].on_icon,arr[n].off_icon, arr[n].name))
            let view = OneTab(withData: (arr[n].icon,arr[n].tab_color, arr[n].name))
            self.addSubview(view)
            view.snp.makeConstraints({ (make)->Void in
                make.width.equalTo(Double(sw)/Double(totalObject))
                make.height.bottom.equalToSuperview()
                make.leading.equalToSuperview().offset(0+Double(CGFloat(n))*(Double(sw)/Double(totalObject)))
            })
            view.tag = n
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
            view.addGestureRecognizer(tap)
            view.isUserInteractionEnabled = true
            
            view.tag = n
            
            list.append(view)
            
            if n == 0 {
                view.setActive(f: true)
            }
        }
        
        let line = UIView()
        self.addSubview(line)
        line.backgroundColor = UIColor.init(hex: "D3C3BB")
        line.snp.makeConstraints({ (make)->Void in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalTo(1)
        })
        
        let badge = BadgeView()
        self.addSubview(badge)
        badge.snp.makeConstraints({ (make)->Void in
            make.centerX.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(10)
            make.width.height.equalTo(10)
        })
        NotificationCenter.default.addObserver(badge,
                                               selector:  #selector(BadgeView.didUpdateUnRead),
                                               name: NSNotification.Name(rawValue: "didUpdateUnRead"),
                                               object: nil)
        
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        APIHelper.checkDevice(completion: {
            self.delegate?.didClick_shop(index: Int(sender.view!.tag))
        }, fail: {
            let lp = LoadingPopUp.addTo(vc:MainViewController.me)
            APIHelper.logout(completion: {
                showInfo(message: "Login Repeated, You have been logout.", didCompletion: {
                    let vc = LoginViewController()
                    ViewController.me.nav.viewControllers = [vc]
                    StaffLoginModel.getOne().clear()
                    //                    ChatDataHelper.clear()
                })
            }, fail: { 
                lp.removeFromSuperview()
            })
        })
    }
    
    func setActive(index:Int) {
        for n in 0 ..< self.list.count {
            self.list[n].setActive(f: false)
            if n == index {
                self.list[n].setActive(f: true)
            }
        }
        
//        for one in self.list {
//            one.setActive(f: false)
//        }
//        (sender.view! as! OneTab).setActive(f: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
