//
//  QRScannerView.swift
//  ExpertMedical
//
//  Created by SocialMind on 28/6/2019.
//  Copyright © 2019 VTL-Solutions Ltd. All rights reserved.
//
import AVFoundation
import UIKit
import SwiftyJSON

class QRScannerView: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
//    required init?(coder aDecoder: NSCoder){
//        super.init(coder: aDecoder)
//        tabBarItem = UITabBarItem(title: "QR Code", image: UIImage(named: "if_shopping"), tag: 2)
//    }
    var qrCodeFrameView:UIView?
    var videoDevice:AVCaptureDevice!
    
    class CameraView: UIView {
        override class var layerClass: AnyClass {
            get {
                return AVCaptureVideoPreviewLayer.self
            }
        }
        
        override var layer: AVCaptureVideoPreviewLayer {
            get {
                return super.layer as! AVCaptureVideoPreviewLayer
            }
        }
    }
    
    var cameraView: CameraView!
    override func loadView() {
        cameraView = CameraView()
        
        view = cameraView
    }
    let session = AVCaptureSession()
    let sessionQueue = DispatchQueue(label: AVCaptureSession.self.description(), attributes: [], target: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // check camera permissi
        checkCameraAccess()
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubview(toFront: qrCodeFrameView)
        }
        
        if(StaffLoginModel.isStaff()){
            let btnBack = "btn-back".imageView()
            self.view.addSubview(btnBack)
            btnBack.snp.makeConstraints({ (make)->Void in
                make.leading.equalToSuperview().offset(KEY_MARGIN*2)
                make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
            })
            let backArea = UIView()
            self.view.addSubview(backArea)
            backArea.snp.makeConstraints({ (make)->Void in
                make.center.equalTo(btnBack)
                make.width.height.equalTo(44)
            })
            backArea.randomBorder()
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
            backArea.addGestureRecognizer(tap)
            backArea.isUserInteractionEnabled = true
        }
        

        
        // Do any additional setup after loading the view, typically from a nib.
        session.beginConfiguration()
        //videoDevice = AVCaptureDevice.default(for: .video)
        if(StaffLoginModel.getOne().login_type == "host"){
            self.videoDevice = AVCaptureDevice.default(
                .builtInWideAngleCamera,
                for: AVMediaType.video,
                position: .front)

        }else{
            self.videoDevice = AVCaptureDevice.default(
                .builtInWideAngleCamera,
                for: AVMediaType.video,
                position: .back)
        }
        
        
        if (videoDevice != nil) {
            let videoDeviceInput = try? AVCaptureDeviceInput(device: videoDevice!)
            
            if (videoDeviceInput != nil) {
                if (session.canAddInput(videoDeviceInput!)) {
                    session.addInput(videoDeviceInput!)
                }
            }
            
            let metadataOutput = AVCaptureMetadataOutput()
            
            if (session.canAddOutput(metadataOutput)) {
                session.addOutput(metadataOutput)
                
                metadataOutput.metadataObjectTypes = [
                    //type of acceptable image
                    //.ean13,
                    //.code128,
                    .qr
                ]
                
                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            }
        }
        
        session.commitConfiguration()
        
        cameraView.layer.session = session
        cameraView.layer.videoGravity = .resizeAspectFill
        let videoOrientation: AVCaptureVideoOrientation
        switch UIApplication.shared.statusBarOrientation {
        case .portrait:
            videoOrientation = .portrait
            
        case .portraitUpsideDown:
            videoOrientation = .portraitUpsideDown
            
        case .landscapeLeft:
            videoOrientation = .landscapeLeft
            
        case .landscapeRight:
            videoOrientation = .landscapeRight
            
        default:
            videoOrientation = .portrait
        }
        
        cameraView.layer.connection?.videoOrientation = videoOrientation
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sessionQueue.async {
            self.session.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        sessionQueue.async {
            self.session.stopRunning()
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Update camera orientation
        let videoOrientation: AVCaptureVideoOrientation
        switch UIDevice.current.orientation {
        case .portrait:
            videoOrientation = .portrait
            
        case .portraitUpsideDown:
            videoOrientation = .portraitUpsideDown
            
        case .landscapeLeft:
            videoOrientation = .landscapeRight
            
        case .landscapeRight:
            videoOrientation = .landscapeLeft
            
        default:
            videoOrientation = .portrait
        }
        
        cameraView.layer.connection?.videoOrientation = videoOrientation
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            print("No QR code is detected")
        }
        
        if (metadataObjects.count > 0 && metadataObjects.first is AVMetadataMachineReadableCodeObject) {
            let lp = LoadingPopUp.addTo(vc: self)
            self.session.stopRunning()
            let scan = metadataObjects.first as! AVMetadataMachineReadableCodeObject
            
            let barCodeObject = cameraView.layer.transformedMetadataObject(for: scan)
            //green box
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            
            var encodedString : Data = scan.stringValue!.data(using: .utf8)!
            let json = JSON(encodedString)
            var params = [String: Any]()
            //print(json)
            if(StaffLoginModel.getOne().login_type == "host"){
                params["staff_id"] = json["id"].stringValue
                params["device"] = json["device"].stringValue
                params["timestamp"] = json["timestamp"].stringValue
                params["shop_id"] = ShopLoginModel.getOne().id
                params["shop_device"] = ShopLoginModel.getOne().device_code
                params["shop_ip"] = ShopLoginModel.getOne().ip_address
                params["qr_type"] = "host"
                params["staff_ip"] = json["ip"].stringValue
            }else if(StaffLoginModel.getOne().login_type == "staff"){
                params["staff_id"] = StaffLoginModel.getOne().id
                params["device"] = StaffLoginModel.getOne().device_code
                params["timestamp"] = json["timestamp"].stringValue
                params["shop_id"] = json["id"].stringValue
                params["shop_ip"] = json["ip"].stringValue
                params["shop_device"] = json["device"].stringValue
                params["qr_type"] = "staff"
            }
            params["type"] = 2 //QRCODE
            if(json["id"].stringValue == nil || json["device"] == nil || json["timestamp"] == nil){
                showInfo(message: "QR Code Incorrect", didCompletion: {
                    self.session.startRunning()
                    lp.removeFromSuperview()
                })
            }else{
//                showInfo(message: "你的ID是: \(params["staff_id"]!)\n確定要打卡嗎?", didOk: {
//                    APIHelper.checkin(withParams: params, completion: {
//                        showInfo(message: "打卡成功", didCompletion: {
//                            let vc = MainViewController()
//                            ViewController.me.nav.viewControllers = [vc]
//                            lp.removeFromSuperview()
//                        })
//
//                    }, fail: {
//                        showInfo(message: "打卡失敗，請重試", didCompletion: {
//                            let vc = MainViewController()
//                            ViewController.me.nav.viewControllers = [vc]
//                            lp.removeFromSuperview()
//                        })
//                        lp.removeFromSuperview() })
//                },didCancel: {
//                    self.session.startRunning()
//                })
                APIHelper.checkin(withParams: params, completion: {
                    lp.removeFromSuperview()
                    let tp = ToastPopUpSuccess.addTo(vc: self)
                    showInfo(message: "Check in success", didCompletion: {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            let vc = MainViewController()
                            ViewController.me.nav.viewControllers = [vc]
                            tp.removeFromSuperview()
                        }
                    })
                }, fail: {
                    lp.removeFromSuperview()
                    let tp = ToastPopUpFail.addTo(vc: self)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let vc = MainViewController()
                        ViewController.me.nav.viewControllers = [vc]
                        tp.removeFromSuperview()
                    }
                })
                
                lp.removeFromSuperview()
            }

        }
    }
    
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            print("Denied, request permission from settings")
            presentCameraSettings()
        case .restricted:
            print("Restricted, device owner must approve")
        case .authorized:
            print("Authorized, proceed")
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    print("Permission granted, proceed")
                } else {
                    print("Permission denied")
                }
            }
        }
    }

    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Error",
                                      message: "Camera access is denied",
                                      preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })

        present(alertController, animated: true)
    }
    
}
