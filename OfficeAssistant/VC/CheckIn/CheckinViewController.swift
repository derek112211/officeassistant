//
//  CheckinViewController.swift
//  EM_Ios_new
//
//  Created by SocialMind on 15/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit
import SwiftIcons

class CheckinViewController:UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black, str: "Check-in")
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        let btn1 = MySquareButton(withData: ("QRCode",UIColor.white,UIColor.black,FontType.fontAwesomeSolid(.qrcode)),pos: "left")
        self.view.addSubview(btn1)
        btn1.snp.makeConstraints({(make)->Void in
            make.leading.trailing.equalToSuperview().inset(KEY_MARGIN*2)
            make.height.equalTo(100)
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        btn1.onClick = {
            let lp = LoadingPopUp.addTo(vc: self)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let vc = QRCodeViewController()
                MainViewController.me.nav.pushViewController(vc, animated: false)
                lp.removeFromSuperview()
            }
        }
        
        let btn2 = MySquareButton(withData: ("Scanner",UIColor.white,UIColor.black,FontType.fontAwesomeSolid(.search)),pos: "left")
        self.view.addSubview(btn2)
        btn2.snp.makeConstraints({(make)->Void in
            make.leading.trailing.equalToSuperview().inset(KEY_MARGIN*2)
            make.height.equalTo(100)
            make.top.equalTo(btn1.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        btn2.onClick = {
            let lp = LoadingPopUp.addTo(vc: self)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let vc = QRScannerView()
                MainViewController.me.nav.pushViewController(vc, animated: false)
                lp.removeFromSuperview()
            }
        }
        
        let btn3 = MySquareButton(withData: ("GPS",UIColor.white,UIColor.black,FontType.fontAwesomeSolid(.mapMarkedAlt)),pos:"left")
        self.view.addSubview(btn3)
        btn3.snp.makeConstraints({(make)->Void in
            make.leading.trailing.equalToSuperview().inset(KEY_MARGIN*2)
            make.height.equalTo(100)
            make.top.equalTo(btn2.snp.bottom).offset(KEY_MARGIN*2)
        })
        
        btn3.onClick = {
            let lp = LoadingPopUp.addTo(vc: self)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let vc = GeoLocationViewController()
                MainViewController.me.nav.pushViewController(vc, animated: false)
                lp.removeFromSuperview()
            }
        }
        
//        let btn4 = MySquareButton(withData: ("Host",UIColor.white,UIColor.black))
//        self.view.addSubview(btn4)
//        btn4.snp.makeConstraints({(make)->Void in
//            make.left.equalTo(btn3.snp.right).offset(margin)
//            make.height.equalTo(150)
//            make.width.equalTo(150)
//            make.top.equalTo(btn2.snp.bottom).offset(margin)
//        })
//
//        btn4.onClick = {
//            print("HOST? NOT FINISH")
//        }
    }
}
