//
//  GeoLocationViewController.swift
//  ExpertMedical
//
//  Created by SocialMind on 21/6/2019.
//  Copyright © 2019 VTL-Solutions Ltd. All rights reserved.
//
import MapKit
import UIKit
import SwiftIcons

class GeoLocationViewController: UIViewController,CLLocationManagerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    //geo location initialize
    var locationManager = CLLocationManager()
    var lbtime:UILabel!
    var lbcoordinate:UILabel!
    var lbloc:UILabel!
    var lbaddress:UILabel!
    var timer:Timer!
    var _updateTimer:Timer!
    var seconds:Double = 30
    
    //image handle attribute
    var imageView: UIImageView!
    var imagePicker = UIImagePickerController()
    var imagePhoto:UIImage!
    
    var lant:Double!
    var long:Double!
    
    var btnSubmit:MyButton!
    var btnRefresh:MyButton!
    
    enum GeoError: Error {
        case noLocation
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters;
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
            timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
            _updateTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
            
        }
        
        
        let sv = UIScrollView()
        self.view.addSubview(sv)
        sv.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.bottom.equalTo(view)
            make.top.equalToSuperview().offset(statusBarH + 120)
            make.bottom.equalToSuperview().offset(0)
        })
        
        let contentView = UIView()
        sv.addSubview(contentView)
        contentView.snp.makeConstraints({ (make)->Void in
            make.trailing.leading.equalTo(view)
            make.top.equalTo(sv)
            make.bottom.equalTo(sv)
        })
        
        let btnBack = "btn-back".imageView()
        self.view.addSubview(btnBack)
        btnBack.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*2)
            make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
        })
        let backArea = UIView()
        self.view.addSubview(backArea)
        backArea.snp.makeConstraints({ (make)->Void in
            make.center.equalTo(btnBack)
            make.width.height.equalTo(44)
        })
        backArea.randomBorder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
        backArea.addGestureRecognizer(tap)
        backArea.isUserInteractionEnabled = true
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black)
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        lbTitle.text = "GPS"
 
        lbtime = UILabel(fontSize:40, color: UIColor.black)
        self.view.addSubview(lbtime)
        lbtime.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbTitle).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.centerY.equalTo(statusBarH + 100)
            make.height.equalTo(50)
        })
        lbtime.text = "Count Down: \(Int(self.seconds))s"
        
        //setup label
        let img_coordinate = UIImage.init(icon: FontType.fontAwesomeSolid(.locationArrow), size: CGSize(width: 50, height: 50), textColor: UIColor.black)
        let ic_coordinate = UIImageView();
        contentView.addSubview(ic_coordinate)
        ic_coordinate.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(contentView).offset(100)
            make.height.equalTo(50)
            make.leading.equalToSuperview().offset(20);
        })
        ic_coordinate.image = img_coordinate
        
        guard let latstr = locationManager.location?.coordinate.latitude else {return}
        guard let lonstr = locationManager.location?.coordinate.longitude else {return}
        lbcoordinate = UILabel(fontSize: 20, color: UIColor.black,str: "\(latstr)\n\(lonstr)")
        contentView.addSubview(lbcoordinate)
        lbcoordinate.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(contentView).offset(100)
            make.height.equalTo(50)
            make.left.equalTo(ic_coordinate.snp.right).offset(20)
        })
        lbcoordinate.numberOfLines = 3;
        lbcoordinate.adjustsFontSizeToFitWidth=true;
        
        let img_loc = UIImage.init(icon: FontType.fontAwesomeSolid(.streetView), size: CGSize(width: 50, height: 50), textColor: UIColor.black)
        let ic_loc = UIImageView();
        contentView.addSubview(ic_loc)
        ic_loc.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbcoordinate.snp.bottom).offset(30)
            make.height.equalTo(50)
            make.leading.equalToSuperview().offset(20);
        })
        ic_loc.image = img_loc
        
        guard let locationtemp: CLLocation = locationManager.location! else {return}
        lbloc = UILabel(fontSize: 16, color: UIColor.black)
        contentView.addSubview(lbloc)
        lbloc.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbcoordinate.snp.bottom).offset(30)
            make.height.equalTo(50)
            make.left.equalTo(ic_loc.snp.right).offset(20)
        })
        lbloc.lineBreakMode = .byWordWrapping
        lbloc.adjustsFontSizeToFitWidth=true;
        lbloc.minimumScaleFactor=0.5;
        lbloc.numberOfLines = 3
        
        lbaddress = UILabel(fontSize: 20, color: UIColor.white)
        self.view.addSubview(lbaddress)
        lbaddress.isHidden = true;
        
        let chooseButton = MyButton(withData: ("Upload Photo",UIColor.white,UIColor.black))
        contentView.addSubview(chooseButton)
        chooseButton.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbloc.snp.bottom).offset(15*2)
            make.left.equalToSuperview().offset(10);
            make.right.equalToSuperview().offset(-10);
            make.height.equalTo(40)
        })

        chooseButton.onClick = {
            ImagePickerManager().pickImage(self){ img in
                self.imageView.image = img
                self.imagePhoto = img
                self.checkImageView()
            }
        }
        
        btnRefresh = MyButton(withData: ("Refresh",UIColor.white,UIColor.black))
        contentView.addSubview(btnRefresh)
        btnRefresh.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(chooseButton.snp.bottom).offset(15*2)
            make.left.equalToSuperview().offset(10);
            make.right.equalToSuperview().offset(-10);
            make.height.equalTo(40)
        })
        
        btnRefresh.onClick = {
            self.updateLocation()
        }
        
        imageView = ScaledHeightImageView(image: nil)
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        //add img to current view
        contentView.addSubview(self.imageView)

        imageView.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(btnRefresh.snp.bottom).offset(15*2)
            make.left.equalToSuperview().offset(10);
            make.right.equalToSuperview().offset(-10);
            //make.edges.equalToSuperview()
        })
        
        btnSubmit = MyButton(withData: ("Check in",UIColor.white,UIColor.black))
        contentView.addSubview(btnSubmit)
        btnSubmit.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(imageView.snp.bottom).offset(15*2)
            make.left.equalToSuperview().offset(10);
            make.right.equalToSuperview().offset(-10);
            make.height.equalTo(40)
            
        })
        
        let lbEnd = UILabel(fontSize: 10, color: UIColor.white, str: "")
        contentView.addSubview(lbEnd)
        lbEnd.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(btnSubmit.snp.bottom).offset(15*2)
            make.bottom.equalToSuperview().offset(-100)
        })
        
        btnSubmit.isEnabled = false
        btnSubmit.isHidden = true
        
        if(CLLocationManager.locationServicesEnabled()){
            getLocationName(_location: locationtemp)
        }
        

        btnSubmit.onClick = {
              self.checkin()
        }

        //init locaiton
        updateLocation()
    }
    
    @objc func updateTimer() {
        if(seconds == 0){
            timer.invalidate()
            timer = nil
            seconds = 30   //init time
            timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)

        }else{
            seconds -= 1
            
        }     //This will decrement(count down)the seconds.
       lbtime.text = "Count Down: \(Int(self.seconds))s" //This will update the label.
    }
    
    @objc func updateLocation(){
//        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        do {
//            try guard let location: CLLocation = try? locationManager.location! else{
//                updateLocation()
//            }
            let location: CLLocation = locationManager.location!

            self.lant = location.coordinate.latitude
            self.long = location.coordinate.longitude
            if(lant == nil || long == nil){
                throw GeoError.noLocation
            }
            
            self.lbcoordinate.text = (String)(lant)+"\n"+(String)(long)
            getLocationName(_location: location)
            //find view and stop timer
            if(MainViewController.me.nav.topViewController is GeoLocationViewController && ViewController.me.nav.topViewController is MainViewController){
                print("In GEOLOCATION code page")
            }else {
                print("NOT In GEOLOCATION code page")
                if timer != nil {
                    timer!.invalidate()
                    timer = nil
                }
                if _updateTimer != nil{
                    _updateTimer.invalidate()
                    _updateTimer = nil
                }
                
            }
        } catch GeoError.noLocation{
            updateLocation()
        } catch let error{
            print(error)
        }
        
        

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }

//        print(clLocation)
        print("GEOLOCATION CHANGED = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        let lp = LoadingPopUp.addTo(vc: self)
        if (status == CLAuthorizationStatus.denied) {
            let vc = MainViewController()
            ViewController.me.nav.viewControllers = [vc]
            lp.removeFromSuperview()
        } else if (status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse) {
            let vc = GeoLocationViewController()
            ViewController.me.nav.viewControllers = [vc]
            lp.removeFromSuperview()
        }
    }
    
    func getLocationName(_location:CLLocation) ->Void{
        let array = NSArray(object: "zh-TW")
        UserDefaults.standard.set(array, forKey: "AppleLanguages")
        
        var addressString : String = ""
        var addressPure : String = ""
        let ceo : CLGeocoder = CLGeocoder()
        let loc = _location
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    if pm.name != nil {
                        addressString = addressString + pm.name!
                        addressPure = addressPure + pm.name!
                    }
                    if pm.locality != nil {
                        addressString = addressString +  ", " + pm.locality!
                        addressPure = addressPure + pm.locality!
                    }
                    if pm.administrativeArea != nil {
                        addressString = addressString + ", " + pm.administrativeArea!
                        addressPure = addressPure + pm.administrativeArea!
                    }
                    if pm.country != nil {
                        addressString = addressString + "\n" + pm.country!
                        addressPure = addressPure + pm.country!
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + ", " + pm.postalCode!
                        addressPure = addressPure + pm.postalCode!
                    }
//                    print("name: "+pm.name!)
//                    print("thoroughfare: "+pm.thoroughfare!)
//                    print("subThoroughfare: "+pm.subThoroughfare!)
//                    print("locality: "+pm.locality!)
//                    print("subLocality: "+pm.subLocality!)
//                    print("administrativeArea: "+pm.administrativeArea!)
//                    print("subAdministrativeArea: "+pm.subAdministrativeArea!)
//                    print("postalCode: "+pm.postalCode!)
//                    print("isoCountryCode: "+pm.isoCountryCode!)
//                    print("country: "+pm.country!)
//
                    print("Loction String: \(addressString)")
                    if(addressString != ""){
                        self.lbloc.text = "\(addressString)"
                        self.lbaddress.text = "\(addressPure)"
                    }else{
                        self.lbloc.text = "None"
                        self.lbaddress.text = ""
                    }
                    
                    
                }
        })
    }
    func checkImageView(){
        if(self.imageView.image != nil){
            print("Button on")
            self.btnSubmit.isEnabled = true
            self.btnSubmit.isHidden = false
        }
        else{
            print("Button off")
            self.btnSubmit.isEnabled = false
            self.btnSubmit.isHidden = true
        }
        
    }
    func checkin(){
        //handle image data
        if(self.imageView.image != nil){
            let photo = self.imagePhoto.resizeWith(percentage: 0.1)
            let base64String = self.convertImageTobase64(format: .jpeg(0.1), image: photo!)
            //print("base64: "+base64String!)
            
            showInfo(message: "Check-in Confirmation", img:self.imageView.image! , didCompletion: {
                let lp = LoadingPopUp.addTo(vc: self)
                
                
                var params = [String: Any]()
                params["staff_id"] = StaffLoginModel.getOne().id
                params["latitude"] = String(self.lant)
                params["longitude"] = String(self.long)
                params["address"] = self.lbaddress.text
                params["type"] = 1 //GPS
                params["timestamp"] = String(Int(Date().timeIntervalSince1970))
                
                //print(self.lbaddress.text)
                //print(params)
                APIHelper.uploadImage(img: photo!,completion: {(path) in
                    params["img_path"] = path
                    APIHelper.checkin(withParams: params, completion: {
                        showInfo(message: "Check-in Success", didCompletion: {
                            let vc = MainViewController()
                            ViewController.me.nav.viewControllers = [vc]
                            lp.removeFromSuperview()
                        })
                    },fail: {
                        showInfo(message: "Check-in Failed, Please Retry", didCompletion: {
                            let vc = MainViewController()
                            ViewController.me.nav.viewControllers = [vc]
                            lp.removeFromSuperview()
                        })
                    })
                }, fail: {
                    showInfo(message: "Check-in Failed, Please Retry", didCompletion: {
                        let vc = MainViewController()
                        ViewController.me.nav.viewControllers = [vc]
                        lp.removeFromSuperview()
                    })
                })
            })
        }else{
            print("NO IMAGE FOUND")
        }
   
    }
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = UIImagePNGRepresentation(image)
        case .jpeg(let compression): imageData = UIImageJPEGRepresentation(image, compression)
        }
        return imageData?.base64EncodedString()
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
    
}

class ScaledHeightImageView: UIImageView {
    
    override var intrinsicContentSize: CGSize {
        
        if let myImage = self.image {
            let myImageWidth = myImage.size.width
            let myImageHeight = myImage.size.height
            let myViewWidth = self.frame.size.width
            
            let ratio = myViewWidth/myImageWidth
            let scaledHeight = myImageHeight * ratio
            
            return CGSize(width: myViewWidth, height: scaledHeight)
        }
        
        return CGSize(width: -1.0, height: -1.0)
    }
    
}
