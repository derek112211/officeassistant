//
//  QRCodeViewController.swift
//  ExpertMedical
//
//  Created by SocialMind on 20/6/2019.
//  Copyright © 2019 VTL-Solutions Ltd. All rights reserved.
//
import UIKit
import SwiftyJSON
import Ipify

class QRCodeViewController: UIViewController {
    
    let btnSubmit = MyButton(withData: ("Generate",UIColor.white,UIColor.black))
    var iv_QRcode: UIImageView!
    var t: Timer? = nil
    var t_ip: Timer? = nil
    var _id:String!
    var _ip:String!
    var _device:String!
    //var _date:Date!
    var _timestamp:String!
    var temp:qrCodeJson!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.iv_QRcode = UIImageView(image: nil);
        generateQRCode()
        
        if(StaffLoginModel.isStaff()){
            let btnBack = "btn-back".imageView()
            self.view.addSubview(btnBack)
            btnBack.snp.makeConstraints({ (make)->Void in
                make.leading.equalToSuperview().offset(KEY_MARGIN*2)
                make.top.equalToSuperview().offset(statusBarH + KEY_MARGIN*2)
            })
            let backArea = UIView()
            self.view.addSubview(backArea)
            backArea.snp.makeConstraints({ (make)->Void in
                make.center.equalTo(btnBack)
                make.width.height.equalTo(44)
            })
            backArea.randomBorder()
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_Back(_:)))
            backArea.addGestureRecognizer(tap)
            backArea.isUserInteractionEnabled = true
        }
        
        let lbTitle = UILabel(fontSize: BIG_TEXT, color: UIColor.black)
        lbTitle.font = UIFont.boldSystemFont(ofSize: BIG_TEXT)
        self.view.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(KEY_MARGIN*6)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(KEY_MARGIN*2)
        })
        lbTitle.text = "QR Code"
        
        t = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.generateQRCode), userInfo: nil, repeats: true)
        t_ip = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.checkIP), userInfo: nil, repeats: true)
//        if (ViewController.me.nav.viewControllers != [vc]) {
//            //do something if it's an instance of that class
//            t = nil
//        }
        
    }
    @objc func checkIP(){
        Ipify.getPublicIPAddress { result in
            switch result {
            case .success(let ip):
                if(ip != ShopLoginModel.getOne().ip_address && ShopLoginModel.getOne().login_type == "host"){
                    DataHelper.save(obj: ip, key: "shop_ip_address")
                    print("HOST IP CHANGE: "+ShopLoginModel.getOne().ip_address)
                }else if(ip != StaffLoginModel.getOne().ip && StaffLoginModel.getOne().login_type == "staff"){
                    DataHelper.save(obj: ip, key: "ip")
                    print("STAFF IP CHANGE: "+StaffLoginModel.getOne().ip)
                }else{
                    print("IP NOT CHANGE")
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    @objc func generateQRCode(){
        //let qrString:String = qrcodeStr.text!
        //let qrString = "{\"aaa\":123}"
        //print(qrStriŚg)
        //Ciimage to Uiimage
        //            let ciImage: CIImage? = QRImage
        //            let uiImage = UIImage(ciImage: ciImage!)
        
        //replace string
        //          let temp = qrString.replacingOccurrences(of: "\"", with: "\\\"")
        //          print(temp)
        
        //QRCODE generate
        //let data = qrString.data(using: .utf8)!
        //let data = "{\"id\":\"123\",\"name\":\"Test\",\"timestamp\":\"10\"}".data(using: .utf8)!
        
        
        
        
        //JSON formatting
        do {
            
            //get data from current user model
            if(StaffLoginModel.getOne().login_type == "staff"){
                _id = StaffLoginModel.getOne().id
                _ip = StaffLoginModel.getOne().ip
                _device = UIDevice.current.identifierForVendor!.uuidString
                
                //Date time handle
                let date = Date()
                _timestamp = String(Int(date.timeIntervalSince1970))
                temp = qrCodeJson.init(id: _id, device:_device, timestamp: _timestamp, ip: _ip)
                
            }else if(StaffLoginModel.getOne().login_type == "host"){
                _id = ShopLoginModel.getOne().id
                _ip = ShopLoginModel.getOne().ip_address
                _device = UIDevice.current.identifierForVendor!.uuidString
                
                //Date time handle
                let date = Date()
                _timestamp = String(Int(date.timeIntervalSince1970))
                temp = qrCodeJson.init(id: _id, device:_device, timestamp: _timestamp, ip: _ip)
                
            }

            
            //encode obj temp to json
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(temp)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            print(json)
            //                Encode example
            //                let f = try JSONDecoder().decode(qrCodeJson.self, from: data)
            //                print(f.id)
            //                print(f.name)
            //                print(f.timestamp)
            guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return  }
            qrFilter.setValue(jsonData, forKey: "inputMessage")
            guard let qrImage = qrFilter.outputImage else { return  }
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            let scaledQrImage = qrImage.transformed(by: transform)
            let context = CIContext()
            guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return }
            let processedImage = UIImage(cgImage: cgImage)
            
            //show QRCode View
            self.iv_QRcode = UIImageView(image: processedImage);
            self.iv_QRcode.contentMode = .scaleAspectFit
            self.view.addSubview(self.iv_QRcode)
            
            self.iv_QRcode.snp.makeConstraints({ (make)->Void in
//                make.leading.equalToSuperview().offset(40)
                make.left.equalToSuperview().offset(20);
                make.right.equalToSuperview().offset(-20);
                make.centerX.centerY.equalToSuperview()
            })
            
            //find view and stop timer
            if(MainViewController.me.nav.topViewController is QRCodeViewController && ViewController.me.nav.topViewController is MainViewController){
                print("In QR code page")
            }else {
                print("NOT In QR code page")
                self.stopTimerTest()
            }
            //                if(ViewController.me.nav.topViewController is LoginViewController ||
            //                    MainViewController.me.nav.topViewController != self ||
            //                    ViewController.me.nav.topViewController is ChatViewController){
            //                    print("NOT In QR code page")
            //                    self.stopTimerTest()
            //                }
            
        } catch {
            print(error)
        }
    }
    
    func stopTimerTest() {
        if t != nil {
            t!.invalidate()
            t = nil
        }
    }
    
    @objc func runTimedCode()
    {
        if(iv_QRcode.image != nil){

            iv_QRcode.image = nil
        }
        btnSubmit.sendActions(for: .touchUpInside)
        
    }
    
    @objc func onTap_Back(_ sender: UITapGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }
    
    struct qrCodeJson: Codable {
        
        var id : String
        var device : String
        var timestamp : String
        var ip : String
    }
    
}
