//
//  RegisterViewControllerB.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 14/05/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import Ipify

class RegisterViewControllerB: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.white
        
        let bg = "bg-login".imageView()
        self.view.addSubview(bg)
        bg.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        bg.contentMode = .scaleAspectFill
        
        
        
        
        let lbLogin = UILabel(fontSize: 36, color: UIColor.white, str: "Login")
        self.view.addSubview(lbLogin)
        lbLogin.snp.makeConstraints({ (make)->Void in
            make.leading.equalToSuperview().offset(40)
            make.centerY.equalTo(statusBarH + 100)
        })
        
        let sv = UIScrollView()
        self.view.addSubview(sv)
        sv.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.bottom.equalTo(view)
            make.top.equalToSuperview().offset(statusBarH + 190)
            make.bottom.equalToSuperview().offset(0)
        })
        
        
        let contentView = UIView()
        sv.addSubview(contentView)
        contentView.snp.makeConstraints({ (make)->Void in
            make.trailing.leading.equalTo(view)
            make.top.equalTo(sv)
            make.bottom.equalTo(sv)
        })
        
        let margin:CGFloat = 40
        
        
        let seg = UISegmentedControl(items: ["Shop","Staff"])
        contentView.addSubview(seg)
        seg.tintColor = UIColor.black
        seg.backgroundColor = UIColor.white
        seg.selectedSegmentIndex = 0
        seg.addTarget(self,
                      action: #selector(onSeg_Change(segment:)),
                      for: .valueChanged)
        seg.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalToSuperview().inset(margin)
            make.top.equalTo(contentView).offset(0)
            make.height.equalTo(40)
        })
        
        
        let btnLogin = MyButton(withData: ("Login",UIColor.darkText,UIColor.init(hex: "D2D2D2")))
        contentView.addSubview(btnLogin)
        btnLogin.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalToSuperview().inset(margin)
            make.top.equalTo(seg.snp.bottom).offset(15)
            make.height.equalTo(40)
            make.bottom.equalToSuperview().offset(-margin)
        })
        btnLogin.onClick = {
            let lp = LoadingPopUp.addTo(vc: self)
            var params = [String: Any]()
            //params["login_type"] = 1
            params["device_code"] = UIDevice.current.identifierForVendor!.uuidString
            params["login_type"] = seg.selectedSegmentIndex //0 = host , 1 = staff
            Ipify.getPublicIPAddress { result in
                switch result {
                case .success(let ip):
                    params["ip"] = ip
                    print(ip)
                    APIHelper.login(withParams: params, completion: {
                            let vc = MainViewController()
                            ViewController.me.nav.viewControllers = [vc]
                            lp.removeFromSuperview()
                    }, fail: { lp.removeFromSuperview() })
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
            
        }
        
        let lbid = UILabel(fontSize: 20, color: UIColor.darkText)
        self.view.addSubview(lbid)
        lbid.snp.makeConstraints({(make) -> Void in
            make.leading.trailing.equalToSuperview().inset(margin)
            make.height.equalTo(40)
            //make.top.equalTo(btnLogin.snp.bottom).offset(15*2)
            make.bottom.equalToSuperview().offset(-margin)
        })
        lbid.numberOfLines = 2
        lbid.text = "UUID:\n"+UIDevice.current.identifierForVendor!.uuidString
        lbid.adjustsFontSizeToFitWidth = true;
        lbid.textColor = .white
        
        var tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap_lbuuid(_:)))
        lbid.addGestureRecognizer(tap)
        lbid.isUserInteractionEnabled = true
        
        
    }
    
    @objc func onTap_lbuuid(_ sender: UITapGestureRecognizer) {
        let uuid_str = UIDevice.current.identifierForVendor!.uuidString
        UIPasteboard.general.string = uuid_str
        showInfo(message: "UUID Copied", didCompletion: {
            return
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func onSeg_Change(segment : UISegmentedControl) {
       if(segment.selectedSegmentIndex == 1){
            let vc = LoginViewController()
            ViewController.me.nav.viewControllers = [vc]
        }
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
