//
//  LoadingPopUp.swift
//  Happiness
//
//  Created by VTLMACOS on 9/8/2017.
//  Copyright © 2017 vtl. All rights reserved.
//

import UIKit

class LoadingPopUp: UIView {

    static func addTo(view:UIView) -> LoadingPopUp
    {
        let v = LoadingPopUp()
        view.addSubview(v)
        return v
    }
    
    static func addTo(vc:UIViewController) -> LoadingPopUp
    {
        let v = LoadingPopUp()
        vc.view.addSubview(v)      
        return v
    }
    
    let dimLayer = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRect(x: 0, y: 0, width: sw, height: sh)
        
        dimLayer.frame = CGRect(x: 0, y: 0, width: sw, height: sh)
        dimLayer.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.1)
        self.addSubview(dimLayer)
        
        /* do animation */
        dimLayer.alpha = 0
        
        let lbl = LoadingView()
        self.addSubview(lbl)
        lbl.center = self.center

        UIView.animate(withDuration: 0.3, animations: { self.dimLayer.alpha = 1 })
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
