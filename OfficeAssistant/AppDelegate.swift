//
//  AppDelegate.swift
//  EM_Ios_new
//
//  Created by SocialMind on 8/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        FirebaseApp.configure()
        
        IQKeyboardManager.sharedManager().enable = true
        //IQKeyboardManager.sharedManager().disabledToolbarClasses = [GiftDetailViewController.self, GiftGameViewController.self]
        
        //        key changes
        //        GMSServices.provideAPIKey("AIzaSyCUxK_OuEvSSDjjRXeW1t1sgB6x3_GvpFw")
        GMSServices.provideAPIKey("AIzaSyCd65ew-GMqaui-u0hTr_jOSu_ACloLR3U")
        
//        let p = PusherManger.shared.start()
        
        //        var dateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "HH"
        //        guard let date = dateFormatter.date(from: "00") else {
        //            fatalError("ERROR: Date conversion failed due to mismatched format.")
        //        }
        //        print(date.debugDescription)
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
//        if UserLoginModel.isLoggedIn() {
//            ChatMessageStatusHelper.shared.load()
//        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
//        GovLicenseHelper.getString(completed: { result in })
//        ShakeGamePlayCountHelper.fetchServer()
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("token - "+deviceTokenString)
        
        PushTokenHelper.token(token: deviceTokenString)
        if StaffLoginModel.isLoggedIn() {
//            APIHelper.device_update()
        }
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
//        if let aps = userInfo["aps"] as? [String: Any] {
//            if let params = aps["params"] as? [String: Any] {
//                if let booking_confirmed = params["booking_confirmed"] as? Int {
//                    print(booking_confirmed)
//                    if booking_confirmed == 1 {
//                        ShakeGamePlayCountHelper.fetchServer()
//                    }
//                }
//            }
//        }
        
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        completionHandler(UIBackgroundFetchResult.noData)
        print(userInfo.debugDescription)
        
//        if let aps = userInfo["aps"] as? [String: Any] {
//            if let params = aps["params"] as? [String: Any] {
//                if let booking_confirmed = params["booking_confirmed"] as? Int {
//                    print(booking_confirmed)
//                    if booking_confirmed == 1 {
//                        ShakeGamePlayCountHelper.fetchServer()
//                    }
//                }
//            }
//        }
        
        //        [AnyHashable("aps"): {
        //            alert = "\U3010BH000021BK100101\U3011\U9810\U7d04\U6210\U529f\U3002";
        //            badge = 1;
        //            params =     {
        //                "booking_confirmed" = 1;
        //            };
        //            sound = default;
        //            }]
        
        let defaults = UserDefaults.standard
        //if defaults.bool(forKey: "enabled_preference") { }
        if ( application.applicationState == .inactive || application.applicationState == .background  )
        {
            //opened from a push notification when the app was on background
        }else{
            //showInfo(message: userInfo.debugDescription, didCompletion: {})
//            if UserLoginModel.isLoggedIn() {
//                ChatMessageStatusHelper.shared.load()
//            }
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        
////        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
//        
////        return facebookDidHandle
//    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
//        if UserLoginModel.isLoggedIn() {
//            ChatMessageStatusHelper.shared.load()
//        }
//        ShakeGamePlayCountHelper.fetchServer()
//
        if !StaffLoginModel.isLoggedIn() || !ShopLoginModel.isLoggedIn() {
            let vc = LoginViewController()
            ViewController.me.nav.viewControllers = [vc]
        }
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
//        FBSDKAppEvents.activateApp()
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
}

//extension AppDelegate: PusherDelegate {
//    func changedConnectionState(from old: ConnectionState, to new: ConnectionState) {
//        // ...
//    }
//
//    func debugLog(message: String) {
//        // ...
//
//        print("debugLog - " + message)
//    }
//
//    func subscribedToChannel(name: String) {
//        // ...
//    }
//
//    func failedToSubscribeToChannel(name: String, response: URLResponse?, data: String?, error: NSError?) {
//        // ...
//    }
//}

