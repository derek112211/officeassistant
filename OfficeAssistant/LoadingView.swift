//
//  LoadingView.swift
//  Happiness
//
//  Created by VTLMACOS on 9/8/2017.
//  Copyright © 2017 vtl. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    override init (frame : CGRect) {
        super.init(frame : frame)
        
//        let indicator = UIActivityIndicatorView()
//        indicator.activityIndicatorViewStyle  = .gray
//        self.addSubview(indicator)
//        indicator.snp.makeConstraints({ (make)->Void in
//            make.width.height.equalTo(40)
//            make.center.equalToSuperview()
//        })
//        indicator.startAnimating()
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = CGFloat(6)
        self.layer.masksToBounds = true
        self.frame = CGRect(x: 0, y: 0, width: 100, height: 40)

        let lb = UILabel()
        lb.text = "Loading".localized()
        lb.textColor = UIColor.darkText
        lb.sizeToFit()
        lb.width(w: lb.width()+20)
        lb.height(h: 40)
        lb.textAlignment = .center
        self.addSubview(lb)

        let indicator = UIActivityIndicatorView()
        indicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        indicator.activityIndicatorViewStyle  = .gray
        self.addSubview(indicator)
        indicator.leadingX(posX: lb.trailingX())
        indicator.centerY(posY: lb.center.y)
        indicator.startAnimating()

        self.width(w: indicator.trailingX())
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
