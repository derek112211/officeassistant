//
//  Config.swift
//  TestingCaqoon01
//
//  Created by VTLMACOS on 23/10/2017.
//  Copyright © 2017 VTLMACOS. All rights reserved.
//

import UIKit
//import Kingfisher

let sw = UIScreen.main.bounds.width //SCREEN_WIDTH
let sh = UIScreen.main.bounds.height //SCREEN_HEIGHT
let sr = sw/414 //SCREEN_RATIO

let BIG_TEXT:CGFloat = 28
let BOTTOM_TAB_H:CGFloat = 60

let FaceBookBlueColor = UIColor(hex: "055FA2")

class Config: NSObject {

}

var statusBarH:CGFloat = UIApplication.shared.statusBarFrame.size.height
var bottomPadding:CGFloat = {
    
    if #available(iOS 11.0, *) {
        if let window = UIApplication.shared.keyWindow {
            return window.safeAreaInsets.bottom
        }else{
            return 0.0
        }
    }
    
    return 0.0
    
}()

var KEY_MARGIN:CGFloat = 10
var KEY_RADIUS:CGFloat = 6
var PAGE_INDICATOR_MAX:Int = 5

func topMostController() -> UIViewController {
    var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
    while (topController.presentedViewController != nil) {
        topController = topController.presentedViewController!
    }
    return topController
}

func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

func isValidYear(testStr:String) -> Bool {
    let emailRegEx = "[1,2][0-9][0-9][0-9]"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

func isValidPhoneNumber(testStr:String) -> Bool {
    let PHONE_REGEX = "^[56789]{1}[0-9]{7}"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    let result =  phoneTest.evaluate(with: testStr)
    return result
}

func deviceId() -> String {
    let deviceID = UIDevice.current.identifierForVendor!.uuidString
    print(deviceID)
    return deviceID
}

func showError(errorMessage:String) {
    let alert = UIAlertController(title:nil, message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
    let cancel = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.cancel, handler: {
        (action: UIAlertAction!) in
    })
    alert.addAction(cancel)
    if let topController = UIApplication.topViewController() {
        topController.present(alert, animated: true, completion: nil)
    }else{
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

func showInfo(message:String, didOk:@escaping (()->()), didCancel:@escaping (()->())) {
    let alert = UIAlertController(title:nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
    let ok = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: {
        (action: UIAlertAction!) in
        didOk()
    })
    alert.addAction(ok)
    let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
        (action: UIAlertAction!) in
        didCancel()
    })
    alert.addAction(cancel)
    if let topController = UIApplication.topViewController() {
        topController.present(alert, animated: true, completion: nil)
    }else{
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

func showInfo(message:String, didCompletion:@escaping (()->())) {
    let alert = UIAlertController(title:nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
    let cancel = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.cancel, handler: {
        (action: UIAlertAction!) in
        didCompletion()
    })
    alert.addAction(cancel)
    if let topController = UIApplication.topViewController() {
        topController.present(alert, animated: true, completion: nil)
    }else{
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

func showInfo(message:String, img:UIImage,didCompletion:@escaping (()->())) {
    let alert = UIAlertController(title:message, message: nil, preferredStyle: UIAlertControllerStyle.alert)
    if let topController = UIApplication.topViewController() {
        let imgAction = UIAlertAction(title: "", style: .default, handler: nil)
        imgAction.isEnabled = false
        imgAction.setValue(scaleImg(image: img).withRenderingMode(.alwaysOriginal), forKey: "image")
        alert.addAction(imgAction)
        let ok = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) in
            didCompletion()
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
            (action: UIAlertAction!) in
            
        })
        alert.addAction(cancel)
        topController.present(alert, animated: true, completion: nil)
    }else{
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

func getCurrentMonth() -> Int {
    let calendar = Calendar.current
    var comp = calendar.dateComponents([.year,.month,.day,.hour,.weekday], from: Date())
    return comp.month!
}

// Return IP address of WiFi interface (en0) as a String, or `nil`

func getWiFiAddress() -> String? {
    
    var address : String?
    // Get list of all interfaces on the local machine:
    var ifaddr : UnsafeMutablePointer<ifaddrs>?
    guard getifaddrs(&ifaddr) == 0 else { return nil }
    guard let firstAddr = ifaddr else { return nil }
    
    // For each interface …
    for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
        let interface = ifptr.pointee
        // Check for IPv4 or IPv6 interface:
        let addrFamily = interface.ifa_addr.pointee.sa_family
        if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
            // Check interface name:
            let name = String(cString: interface.ifa_name)
            if  name == "en0" {
                // Convert interface address to a human readable string:
                var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                            &hostname, socklen_t(hostname.count),
                            nil, socklen_t(0), NI_NUMERICHOST)
                address = String(cString: hostname)
            }
        }
    }
    freeifaddrs(ifaddr)
    return address
}

//get Response Time
func getResponseTime(startDate: Date){
    let time = Date().timeIntervalSince(startDate)
    print("Response Time: \(time)s")
}

//scale image with fit radio
func scaleImg(image:UIImage) -> UIImage{
    let maxSize = CGSize(width: 245, height: 300)
    let imgSize = image.size
    var ratio:CGFloat!
    
    if(imgSize.width > imgSize.height){
        ratio = maxSize.width / imgSize.width
    }else{
        ratio = maxSize.height / imgSize.height
    }
    
    let scaledSize = CGSize(width: imgSize.width * ratio, height: imgSize.height * ratio)
    
    var resizedImage = image.resizeWith(scaledSize)
    
    if(imgSize.height > imgSize.width){
        let left = (maxSize.width - resizedImage.size.width)/2
        resizedImage = resizedImage.withAlignmentRectInsets(UIEdgeInsetsMake(0, -left, 0, 0))
    }
    return resizedImage
}

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}


