//
//  PageIndicator.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 18/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

class PageIndicator: UIView {
    
    var lbPage:UILabel!
    var totalPage:Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let lb = UILabel(fontSize: 14, color: UIColor.darkText, str: "")
        lb.textAlignment = .center
        self.addSubview(lb)
        lb.snp.makeConstraints({ (make)->Void in
            make.edges.equalToSuperview()
        })
        lbPage = lb
        
    }
    
    func setTotalPage(number:Int) {
        totalPage = number
        self.isHidden = true
        if totalPage > PAGE_INDICATOR_MAX {
            self.isHidden = false
        }
    }
    
    func setPage(currentPage:Int) {
        lbPage.text = "(\(currentPage)/\(totalPage))"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
