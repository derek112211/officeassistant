//
//  TopTabLabel.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 10/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

class TopTabLabel: UIView {
    
    let lb = UILabel(fontSize: 16, color: UIColor.darkText)
    let line = UIView()
    
    init(withText str:String) {
        super.init(frame: CGRect.zero)
        
        lb.text = str
        self.addSubview(lb)
        lb.snp.makeConstraints({ (make)->Void in
            make.leading.top.trailing.equalToSuperview().inset(6)
        })
        
        self.addSubview(line)
        line.snp.makeConstraints({ (make)->Void in
            make.width.centerX.equalTo(lb)
            make.top.equalTo(lb.snp.bottom).offset(6)
            make.height.equalTo(2)
            
            make.bottom.equalToSuperview()
        })
        line.backgroundColor = UIColor.darkText
        line.isHidden = true
        
    }
    
    func setActive(f:Bool) {
        if f {
            line.isHidden = false
        }else{
            line.isHidden = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
