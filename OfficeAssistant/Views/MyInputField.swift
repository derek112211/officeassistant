//
//  MyInputField.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 11/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import DateTimePicker

enum InputFieldType {
    case Phone
    case Domain
    case Password
    case Email
    case Gender
    case Date
    case TimeFrom
    case TimeTo
    case QRCODE
    
    case DOB_Y
    case DOB_M
    case DOB_D
    
    case DateOfJoin
    case Shop
    case FirstNameEN
    case LastNameEN
    case FirstNameZHO
    case LastNameZHO
    
    case Leave
    
}

class MyInputField: UITextField {
    
    var selectedBOD:UserDayOfBirthModel?
    var selectedDayAndMonth:MonthAndDayModel!
    var selectedLeave:LeaveModel?
    var applyLeaveView:ApplyLeaveViewController!
    
    var selectedDate:Date!
    var fieldType:InputFieldType!
    
//    var picker:DateTimePicker!

    init(withType type:InputFieldType) {
        
        super.init(frame: CGRect.zero)
        self.layer.cornerRadius = 20
        self.backgroundColor = UIColor.white
        
        let holder = UIView()
        holder.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        self.leftView = holder
        self.leftViewMode = .always
        
        let icon = UIImageView()
        icon.contentMode = .center
        icon.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        icon.center = CGPoint(x: holder.frame.size.width/2, y: holder.frame.size.height/2)
        holder.addSubview(icon)
        
//        self.autocorrectionType = .yes
//        self.autocapitalizationType = .none
        applyLeaveView = ApplyLeaveViewController()
        
        if type == .FirstNameEN {
            self.placeholder = "First Name (ENG)"
            icon.image = UIImage(named: "icon-name")
        }
        
        if type == .LastNameEN {
            self.placeholder = "Last Name(ENG)"
            icon.image = UIImage(named: "icon-name")
            
        }
        if type == .FirstNameZHO {
            self.placeholder = "First Name(CHI)"
            icon.image = UIImage(named: "icon-name")
            
        }
        if type == .LastNameZHO {
            self.placeholder = "Last Name(CHI)"
            icon.image = UIImage(named: "icon-name")
            
        }
        
        if type == .Phone {
            self.placeholder = "Phone..."
            icon.image = UIImage(named: "icon-phone")
            self.fieldType = .Phone
            self.keyboardType = .phonePad
        }
        
        if type == .Domain {
            self.placeholder = "Domain..."
            icon.image = UIImage(named: "icon-www")
            self.fieldType = .Domain
        }
        
        if type == .QRCODE {
            self.placeholder = "QRCODE String"
            //icon.image = UIImage(named: "icon-phone")
        }
        
        if type == .Password {
            self.placeholder = "Password..." //密碼"
            self.isSecureTextEntry = true
            icon.image = UIImage(named: "icon-pw")
            
            //self.isSecureTextEntry = true
//            self.keyboardType = .numberPad
        }
        if type == .Email {
            self.placeholder = "Please input..."
            icon.image = UIImage(named: "icon-email")
            
        }
        
        if type == .Date {
            if(self.selectedDayAndMonth == nil){
                self.selectedDayAndMonth = MonthAndDayModel()
            }
            self.selectedDate = Date()
            icon.image = UIImage(named: "icon-email")
            self.placeholder = "Please select..."
            self.layer.borderColor = UIColor.lightGray.cgColor
            //icon.image = UIImage(named: "icon-gift")
            self.isEnabled = false
            
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.pickDate(_:)))
            self.addGestureRecognizer(tap)
            self.isUserInteractionEnabled = true
            
        }
        
        if type == .Leave{
            self.placeholder = "Please select..."
            self.layer.borderColor = UIColor.lightGray.cgColor
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.pickLeaveType(_:)))
            self.addGestureRecognizer(tap)
            self.isUserInteractionEnabled = true
        }
        
//        func textField(_ textField: MyInputField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//            if(textField.fieldType == InputFieldType.Phone){
//                if ((textField.text?.length)! + (string.length - range.length)) > 8 {
//                    return false
//                }
//            }
//            return true
//        }
        
    }
    
    // constructor for only handle dateTimeTo and dateTimeFrom with model data
    init(withType type:InputFieldType,view:ApplyLeaveViewController){
        super.init(frame: CGRect.zero)
        self.fieldType = type
        
        
        self.layer.cornerRadius = 20
        self.backgroundColor = UIColor.white
        
        let holder = UIView()
        holder.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        self.leftView = holder
        self.leftViewMode = .always
        
        let icon = UIImageView()
        icon.contentMode = .center
        icon.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        icon.center = CGPoint(x: holder.frame.size.width/2, y: holder.frame.size.height/2)
        holder.addSubview(icon)
        
        self.applyLeaveView = view
        
        if type == .TimeFrom{
//            if(self.selectedDayAndMonth == nil){
//                self.selectedDayAndMonth = MonthAndDayModel()
//                self.selectedDayAndMonth.type = .TimeFrom
//            }
            self.selectedDate = Date()
            
            self.placeholder = "Please select..."
            self.layer.borderColor = UIColor.lightGray.cgColor
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.pickDate(_:)))
            self.addGestureRecognizer(tap)
            self.isUserInteractionEnabled = true
        }
        
        if type == .TimeTo{
//            if(self.selectedDayAndMonth == nil){
//                self.selectedDayAndMonth = MonthAndDayModel()
//            }
            if(self.selectedDate == nil){
                self.selectedDate = Date()
            }
            self.placeholder = "Please select..."
            self.layer.borderColor = UIColor.lightGray.cgColor
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.pickDate(_:)))
            self.addGestureRecognizer(tap)
            self.isUserInteractionEnabled = true
        }
        
    }
    
    @objc func pickLeaveType(_ sender: UITapGestureRecognizer) {
        self.parentViewController!.view.endEditing(true)
        APIHelper.getLeaveTypeList(completion: { (data) in
            _ = LeavePicker.addTo(view: self.parentViewController!.view,leaves:data,  didCompletion: { result in
                self.text = result.name
                print(self.hasText)
                self.selectedLeave = result
            })
        }, fail: {
            return;
        })
    }
    
    @objc func pickDate(_ sender: UITapGestureRecognizer) {
        self.parentViewController!.view.endEditing(true)
        
        _ = NEW_DateTimePicker.addTo(selectedDate: self.selectedDate,didCompletion: {
            result in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-MM-dd HH:mm"
            self.text = formatter.string(from: result)
            self.selectedDate = result
            print(self.selectedDate)
        })
    }
    
    @objc func debugTest(_ sender: UITapGestureRecognizer) {
        print(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
