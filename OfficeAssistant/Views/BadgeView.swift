//
//  BadgeView.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 10/05/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

class BadgeView: UIView {
    
    var channel:String = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        self.backgroundColor = UIColor.red
        self.layer.cornerRadius = 5
        self.layer.masksToBounds  = true
        
        self.isHidden = true

    }
    
    @objc func didUpdateUnRead() {
        print("didUpdateUnRead")
        
        if self.channel == "" {
            self.isHidden = true
//            for one in APIHelper.unreadData {
//                if one.unread {
//                    self.isHidden = false
//                }
//            }
        }else{
//            self.isHidden = true
//            for one in APIHelper.unreadData {
//                if one.channel == self.channel && one.unread {
//                    self.isHidden = false
//                }
//            }
        }
        
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
