//
//  ReportLateCell.swift
//  EM_Ios_new
//
//  Created by SocialMind on 18/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit

class ReportLateCell : UITableViewCell{
    let margin:CGFloat = 20
    
    var data:ReportLateModel?
    
    let lbdate = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbcount = UILabel(fontSize: 14, color: UIColor.darkText)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        contentView.addSubview(lbdate)
        lbdate.snp.makeConstraints({ (make)->Void in
            make.leading.top.trailing.equalToSuperview().inset(margin)
            make.leading.bottom.trailing.equalToSuperview().inset(margin)
            //make.height.equalTo(20) //.equalTo(sw-margin*2)
        })
        
        contentView.addSubview(lbcount)
        lbcount.snp.makeConstraints({ (make)->Void in
            make.bottom.top.equalToSuperview().inset(margin)
            make.trailing.equalToSuperview().inset(margin)
        })
        
    }
    
    func setData(data:ReportLateModel) {
        self.data = data
        
        if(data.title != "" && data.title != nil){
            lbdate.text = String(self.data!.title)
            lbcount.text = String(self.data!.total)
        }else{
            lbdate.text = String(self.data!.day)
            lbcount.text = String(self.data!.lateInMins)
        }

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
