//
//  ReportLeaveCell.swift
//  EM_Ios_new
//
//  Created by SocialMind on 13/8/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit

class ReportLeaveCell : UITableViewCell{
    let margin:CGFloat = 20
    var data:ReportLeaveModel?
    
    let lbdate = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbleaveType = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbleaveReason = UILabel(fontSize: 14, color: UIColor.darkText)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        contentView.addSubview(lbdate)
        lbdate.snp.makeConstraints({ (make)->Void in
            make.leading.top.trailing.equalToSuperview().inset(margin)
        })
        
        contentView.addSubview(lbleaveType)
        lbleaveType.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbdate.snp.bottom).offset(margin)
            make.leading.bottom.equalToSuperview().inset(margin)
        })
        
        contentView.addSubview(lbleaveReason)
        lbleaveReason.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbdate.snp.bottom).offset(margin)
            make.left.equalToSuperview().inset(200)
            make.bottom.trailing.equalToSuperview().inset(margin)
        })
        
        
    }
    
    func setData(data:ReportLeaveModel) {
        self.data = data
        if(self.data?.id == nil){
            lbdate.text = "No Record"
            lbleaveType.text = ""
            lbleaveReason.text = ""
        }else{
            lbdate.text = data.getFullDate()
            lbleaveType.text = data.leave_type
            lbleaveReason.text = data.approve_remark
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
