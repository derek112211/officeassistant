//
//  LeaveCell.swift
//  EM_Ios_new
//
//  Created by SocialMind on 8/8/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation

class LeaveCell : UITableViewCell{
    let margin:CGFloat = 20
    
    var data:LeaveUserModel?
    
    let lbAppNo = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbName = UILabel(fontSize: 14, color: UIColor.darkText)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        contentView.addSubview(lbAppNo)
        lbAppNo.snp.makeConstraints({ (make)->Void in
            make.leading.top.bottom.equalToSuperview().inset(margin)
            //make.height.equalTo(20) //.equalTo(sw-margin*2)
        })
        
        contentView.addSubview(lbName)
        lbName.snp.makeConstraints({ (make)->Void in
            make.bottom.equalToSuperview().inset(margin)
            make.left.equalTo(lbAppNo.snp.right).offset(margin)
        })
        
    }
    
    func setData(data:LeaveUserModel) {
        self.data = data
        if(self.data?.id == nil){
            self.lbAppNo.text = "No Record"
            self.lbName.text = ""
        }else{
            self.lbAppNo.text = data.id
            self.lbName.text = data.getFullName()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
