//
//  ReportDetailCell.swift
//  EM_Ios_new
//
//  Created by SocialMind on 17/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit

class ReportDetailCell : UITableViewCell{
    let margin:CGFloat = 20
    
    var data:ReportDetailsModel?
    
    let lbdate = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbschedule_start = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbschedule_end = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbattend_start = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbattend_end = UILabel(fontSize: 14, color: UIColor.darkText)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        contentView.addSubview(lbdate)
        lbdate.snp.makeConstraints({ (make)->Void in
            make.leading.top.trailing.equalToSuperview().inset(margin)
            //make.height.equalTo(20) //.equalTo(sw-margin*2)
        })
        
        contentView.addSubview(lbschedule_start)
        lbschedule_start.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(lbdate).inset(0)
            make.top.equalTo(lbdate.snp.bottom).offset(KEY_MARGIN/2)
        })
        
        contentView.addSubview(lbschedule_end)
        lbschedule_end.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbschedule_start.snp.bottom).offset(KEY_MARGIN/2)
            make.bottom.equalToSuperview()
            make.leading.bottom.trailing.equalToSuperview().inset(margin)
        })
        
        contentView.addSubview(lbattend_start)
        lbattend_start.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbdate.snp.bottom).offset(KEY_MARGIN/2)
            make.left.equalToSuperview().inset(200)
        })

        contentView.addSubview(lbattend_end)
        lbattend_end.snp.makeConstraints({ (make)->Void in
            make.top.equalTo(lbattend_start.snp.bottom).offset(KEY_MARGIN/2)
            make.left.equalToSuperview().inset(200)
            make.bottom.trailing.equalToSuperview().inset(margin)
        })
        
    }
    
    func setData(data:ReportDetailsModel) {
        self.data = data
        if(self.data?.user_staff_id == nil){
            lbdate.text = "No Record";
            lbschedule_start.text = ""
            lbschedule_end.text = ""
            lbattend_start.text = ""
            lbattend_end.text = ""
        }else{
            
            if(self.data?.leave_id != "0"){
                lbdate.text = self.data!.working_date+" (Leave)"
            }else{
                lbdate.text = self.data!.working_date
            }
            

            
            lbschedule_start.text = "Schedule Start\t"+self.data!.working_time_start!
            //print(self.data!.working_time_start!)
            lbschedule_end.text = "Schedule End\t"+self.data!.working_time_end
            lbattend_start.text = "Work Start\t"+self.data!.attendance_start
            lbattend_end.text = "Work End\t"+self.data!.attendance_end
        }

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
