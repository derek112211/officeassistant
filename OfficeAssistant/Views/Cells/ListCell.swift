//
//  AccountCell.swift
//  EM_Ios_new
//
//  Created by SocialMind on 16/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import Foundation
import UIKit

class ListCell : UITableViewCell{
    let margin:CGFloat = 20
    
    //var cv:UICollectionView!
    let pageControl = UIPageControl()
    let pageNumber = PageIndicator()
    
    var icon:UIImageView!
    let lbTitle = UILabel(fontSize: 20, color: UIColor.darkText)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        contentView.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            //make.leading.top.bottom.trailing.equalToSuperview().inset(margin)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().inset(margin*2+25)
            make.height.equalTo(30) //.equalTo(sw-margin*2)
        })
        
        icon = UIImageView()
        self.addSubview(icon)
        icon.snp.makeConstraints({ (make)->Void in
            make.width.height.equalTo(25)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(margin)
        })
        
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.gray
        contentView.addSubview(pageControl)
        pageControl.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalToSuperview().inset(margin)
            make.top.equalTo(lbTitle.snp.bottom).offset(0)
            make.height.equalTo(20)
            make.bottom.equalToSuperview()
        })
        pageControl.isUserInteractionEnabled = false
        
        self.addSubview(pageNumber)
        pageNumber.snp.makeConstraints({ (make)->Void in
            make.edges.equalTo(pageControl)
        })
        
    }
    
    func setData(data:IconListModel) {
        pageNumber.setTotalPage(number: pageControl.numberOfPages)
        pageNumber.setPage(currentPage: 1)
        if pageControl.numberOfPages > PAGE_INDICATOR_MAX {
            pageControl.isHidden = true
        }
        let Img = UIImage.init(icon: data.icon, size: CGSize(width: 70, height: 70),textColor: data.color)
        icon.image = Img
        lbTitle.text = data.name
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

