//
//  StaffCell.swift
//  ExpertMedical
//
//  Created by SocialMind on 25/6/2019.
//  Copyright © 2019 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
class StaffCell : UITableViewCell{
    let margin:CGFloat = 20
    
    var data:StaffDataModel?
    //var cv:UICollectionView!
    let pageControl = UIPageControl()
    let pageNumber = PageIndicator()
    
    let lbid = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbTitle = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbdesc = UILabel(fontSize: 14, color: UIColor.darkText)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
//        cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
//        cv.showsHorizontalScrollIndicator = false
//        cv.showsVerticalScrollIndicator = false
//        cv.register(InfoPhotoCell.self, forCellWithReuseIdentifier: "InfoPhotoCell")
//        cv.dataSource = self
//        cv.delegate = self
//        cv.layer.cornerRadius = KEY_RADIUS
//        cv.layer.masksToBounds = true
//        cv.isPagingEnabled = true
//        cv.backgroundColor = UIColor.init(hex: "F2F2F2")
//
//        contentView.addSubview(cv)
//        cv.snp.makeConstraints({ (make)->Void in
//            make.leading.top.trailing.equalToSuperview().inset(margin)
//            make.height.equalTo(cv.snp.width) //.equalTo(sw-margin*2)
//        })
        
        contentView.addSubview(lbid)
        lbid.snp.makeConstraints({ (make)->Void in
//            make.leading.trailing.equalTo(cv).inset(0)
//            make.top.equalTo(cv.snp.bottom).offset(KEY_MARGIN*2)
            make.leading.top.trailing.equalToSuperview().inset(margin)
            make.height.equalTo(30) //.equalTo(sw-margin*2)
        })
        
        contentView.addSubview(lbTitle)
        lbTitle.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(lbid).inset(0)
            make.top.equalTo(lbid.snp.bottom).offset(KEY_MARGIN/2)
        })
        
        contentView.addSubview(lbdesc)
        lbdesc.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(lbTitle).inset(0)
            make.top.equalTo(lbTitle.snp.bottom).offset(KEY_MARGIN/2)
        })
        
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.gray
        contentView.addSubview(pageControl)
        pageControl.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalToSuperview().inset(margin)
            make.top.equalTo(lbTitle.snp.bottom).offset(0)
            make.height.equalTo(40)
            make.bottom.equalToSuperview()
        })
        pageControl.isUserInteractionEnabled = false
        
        self.addSubview(pageNumber)
        pageNumber.snp.makeConstraints({ (make)->Void in
            make.edges.equalTo(pageControl)
        })
        
    }
    
    func setData(data:StaffDataModel) {
        self.data = data
//        cv.reloadData()
//        pageControl.numberOfPages = self.data!.images.count + self.data!.youtubes.count
//        pageControl.currentPage = 0
//        pageControl.isHidden = true
//        if self.data!.images.count + self.data!.youtubes.count > 1 {
//            pageControl.isHidden = false
//        }
        
        pageNumber.setTotalPage(number: pageControl.numberOfPages)
        pageNumber.setPage(currentPage: 1)
        if pageControl.numberOfPages > PAGE_INDICATOR_MAX {
            pageControl.isHidden = true
        }
        
        lbid.text = self.data!.id
        lbTitle.text = self.data!.name
        lbdesc.text = self.data!.my_description
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
    
