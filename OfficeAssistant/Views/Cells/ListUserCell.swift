//
//  ListUserCell.swift
//  EM_Ios_new
//
//  Created by SocialMind on 17/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//

import UIKit
class ListUserCell : UITableViewCell{
    let margin:CGFloat = 20
    
    var data:ReportUserModel?
    //var cv:UICollectionView!
//    let pageControl = UIPageControl()
//    let pageNumber = PageIndicator()
    
    let lbFullName = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbshop = UILabel(fontSize: 14, color: UIColor.darkText)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        contentView.addSubview(lbFullName)
        lbFullName.snp.makeConstraints({ (make)->Void in
            make.leading.top.trailing.equalToSuperview().inset(margin)
            make.height.equalTo(30) //.equalTo(sw-margin*2)
        })
        
        contentView.addSubview(lbshop)
        lbshop.snp.makeConstraints({ (make)->Void in
            make.leading.trailing.equalTo(lbFullName).inset(0)
            make.top.equalTo(lbFullName.snp.bottom).offset(KEY_MARGIN/2)
            make.bottom.equalToSuperview()
            make.leading.bottom.trailing.equalToSuperview().inset(margin)
        })
        
//        pageControl.pageIndicatorTintColor = UIColor.lightGray
//        pageControl.currentPageIndicatorTintColor = UIColor.gray
//        contentView.addSubview(pageControl)
//        pageControl.snp.makeConstraints({ (make)->Void in
//            make.leading.trailing.equalToSuperview().inset(margin)
//            make.top.equalTo(lbshop.snp.bottom).offset(0)
//            make.height.equalTo(40)
//            make.bottom.equalToSuperview()
//        })
//        pageControl.isUserInteractionEnabled = false
//
//        self.addSubview(pageNumber)
//        pageNumber.snp.makeConstraints({ (make)->Void in
//            make.edges.equalTo(pageControl)
//        })
        
    }
    
    func setData(data:ReportUserModel) {
        self.data = data
        
//        pageNumber.setTotalPage(number: pageControl.numberOfPages)
//        pageNumber.setPage(currentPage: 1)
//        if pageControl.numberOfPages > PAGE_INDICATOR_MAX {
//            pageControl.isHidden = true
//        }
        
        lbFullName.text = self.data!.getFullNameWithUserName()
        lbshop.text = self.data!.shop_name
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
