//
//  ReportCheckinCell.swift
//  EM_Ios_new
//
//  Created by SocialMind on 18/7/2019.
//  Copyright © 2019 SocialMind. All rights reserved.
//
import Foundation
import UIKit

class ReportCheckinCell : UITableViewCell{
    let margin:CGFloat = 20
    
    var data:ReportCheckinModel?
    
    let lbdate = UILabel(fontSize: 14, color: UIColor.darkText)
    let lbcount = UILabel(fontSize: 14, color: UIColor.darkText)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        contentView.addSubview(lbdate)
        lbdate.snp.makeConstraints({ (make)->Void in
            make.leading.top.trailing.equalToSuperview().inset(margin)
            make.leading.bottom.trailing.equalToSuperview().inset(margin)
            //make.height.equalTo(20) //.equalTo(sw-margin*2)
        })
        
        contentView.addSubview(lbcount)
        lbcount.snp.makeConstraints({ (make)->Void in
            make.bottom.top.equalToSuperview().inset(margin)
            make.trailing.equalToSuperview().inset(margin)
        })
        
    }
    
    func setData(data:ReportCheckinModel) {
        self.data = data
        
        
        lbdate.text = String(self.data!.day)
        lbcount.text = String(self.data!.count)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class ReportCheckinDetailCell : UITableViewCell{
    let margin:CGFloat = 20
    
    var data:ReportCheckinDetailModel?
    
    let lbtime = UILabel(fontSize: 24, color: UIColor.darkText)
    let lbaddress = UILabel(fontSize: 12, color: UIColor.darkText)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        contentView.addSubview(lbtime)
        lbtime.snp.makeConstraints({ (make)->Void in
            make.leading.top.trailing.equalToSuperview().inset(margin)
            //make.height.equalTo(20) //.equalTo(sw-margin*2)
        })
        
        contentView.addSubview(lbaddress)
        lbaddress.snp.makeConstraints({ (make)->Void in
            make.bottom.equalToSuperview().inset(margin)
            make.leading.equalTo(lbtime)
            make.top.equalTo(lbtime.snp.bottom)
        })
        
    }
    
    func setData(data:ReportCheckinDetailModel) {
        self.data = data
        
        
        lbtime.text = String(self.data!.time)
        if(self.data!.shop_name != nil && self.data!.shop_name != ""){
            lbaddress.text = "\(self.data!.shop_name!) - \(self.data!.address!)"
        }else{
            if(self.data!.address! != nil){
                lbaddress.text = "GPS - \(self.data!.address!)"
            }else{
                lbaddress.text = "GPS"
            }
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
