//
//  MyButton.swift
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 11/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit
import SwiftIcons

typealias MyButtonData = (title: String, txtColor: UIColor, bgColor: UIColor)
typealias MySquareButtonData = (title: String, txtColor: UIColor, bgColor: UIColor,icon:FontType)

class MyButtonWithTick: MyButton {
    let tickIcon = "img-tick-white".imageView(resizedWidth: 18)
    override init(withData data: MyButtonData) {
        super.init(withData: data)
        self.addSubview(tickIcon)
        tickIcon.snp.makeConstraints({ (make)->Void in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-KEY_MARGIN)
            make.width.height.equalTo(40)
        })
        tickIcon.randomBorder()
        tickIcon.contentMode = .center
        tickIcon.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class MyButton: UIButton {
    
    var onClick:(()->())?
    
    init(withData data:MyButtonData) {
        super.init(frame: .zero)
        
        self.layer.cornerRadius = 20
        
        backgroundColor = data.bgColor
        self.setTitleColor(data.txtColor, for: UIControlState())
        self.setTitle(data.title, for: UIControlState())
        self.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        
    }
    
    @objc func buttonClicked() {
        print("Button Clicked")
        
        if onClick != nil {
            self.onClick!()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
class MySquareButton: UIButton {
    
    var onClick:(()->())?
    
    init(withData data:MySquareButtonData,pos:String) {
        super.init(frame: .zero)

        backgroundColor = data.bgColor
        self.setTitleColor(data.txtColor, for: UIControlState())
        self.width(w: 200)
        self.height(h: 200)
        self.setTitle(data.title, for: UIControlState())
//        let lbtitle = UILabel(fontSize: 24, color: data.txtColor,str: data.title)
//        self.addSubview(lbtitle)
//        lbtitle.snp.makeConstraints({ (make)->Void in
//            make.width.height.equalTo(25)
//            make.centerY.centerX.equalToSuperview()
//        })
        
        let imgIcon = UIImage.init(icon: data.icon, size: CGSize(width: 70, height: 70), textColor: UIColor.white)
        if(pos == "right"){
            let iconR = UIImageView()
            self.addSubview(iconR)
            iconR.snp.makeConstraints({ (make)->Void in
                make.width.height.equalTo(25)
                make.centerY.equalToSuperview()
                make.trailing.equalTo(self).inset(5)
            })
            iconR.image = imgIcon
        }else if(pos == "left"){
            let iconL = UIImageView()
            self.addSubview(iconL)
            iconL.snp.makeConstraints({ (make)->Void in
                make.width.height.equalTo(25)
                make.centerY.equalToSuperview()
                make.leading.equalTo(self).offset(5)
            })
            iconL.image = imgIcon
        }
       
        self.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        
    }
    
    @objc func buttonClicked() {
        print("Button Clicked")
        
        if onClick != nil {
            self.onClick!()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
