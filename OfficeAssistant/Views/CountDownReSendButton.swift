//
//  CountDownReSendButton.swift
//  SOHOEast
//
//  Created by kelvin.lee(VTL) on 01/06/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

import UIKit

//class CountDownHelper: NSObject {
//
//    static var shared = CountDownHelper()
//    var count:Int = 0
//    var button:CountDownReSendButton?
//
//    func firstStart() {
//        startCountDown()
//        if let b = button {
//            b.setCount()
//        }
//    }
//
//    func startCountDown() {
//        self.count = 60
//        if let b = button {
//            b.setCount()
//            b.isUserInteractionEnabled = false
//        }
//        countDown()
//    }
//
//    func countDown() {
//        Timer.scheduledTimer(timeInterval: TimeInterval(1),
//                             target: self,
//                             selector:  #selector(tick),
//                             userInfo: nil,
//                             repeats: false)
//    }
//
//    @objc func tick() {
//        self.count = self.count - 1
//        if let b = button {
//            b.setCount()
//            if self.count == 0 {
//                b.reset()
//                b.isUserInteractionEnabled = true
//            }else{
//                countDown()
//            }
//        }
//    }
//}

class CountDownReSendButton: MyButton {
    var str:String!
    var count:Int = 0
    var myData:MyButtonData!
    var isEnable_MaxPhoneNo = false
    
    override init(withData data:MyButtonData) {
        super.init(withData: data)
        
        self.myData = data
        
        self.str = data.title
        
//        CountDownHelper.shared.button = self
//        if CountDownHelper.shared.count > 0 {
//            self.isUserInteractionEnabled = false
//        }
        
    }
    @objc override func buttonClicked() {
        print("Button Clicked")
        if onClick != nil {
            self.onClick!()
        }
    }
    //func startCountDown() {
    //    CountDownHelper.shared.startCountDown()
    //}
    //func setCount() {
    //    self.setTitle(self.str + "(\(CountDownHelper.shared.count))", for: UIControlState())
    //}
    //func reset() {
    //    self.setTitle(self.str, for: UIControlState())
    //}
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    func firstStart() {
//        startCountDown()
//        self.setTitle(self.str + "(\(self.count))", for: UIControlState())
//    }
    
    func startCountDown() {
        self.count = 60
        self.setTitle(self.str + "(\(self.count))", for: UIControlState())
        self.isUserInteractionEnabled = false
        countDown()
    }
    
    func countDown() {
        Timer.scheduledTimer(timeInterval: TimeInterval(1),
                             target: self,
                             selector:  #selector(tick),
                             userInfo: nil,
                             repeats: false)
    }
    
    @objc func tick() {
        self.count = self.count - 1
        self.setTitle(self.str + "(\(self.count))", for: UIControlState())
        if self.count == 0 {
            self.setTitle(self.str, for: UIControlState())
            self.isUserInteractionEnabled = true
        }else{
            countDown()
        }
        
//        if let b = button {
//            b.setCount()
//            if self.count == 0 {
//                b.reset()
//                b.isUserInteractionEnabled = true
//            }else{
//                countDown()
//            }
//        }
    }
    
    func addHandleTextfield(tf:UITextField) {
        self.backgroundColor = UIColor.gray
        tf.delegate = self
    }
    
}

extension CountDownReSendButton : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.length)! + (string.length - range.length)) > 0 {
            self.backgroundColor = self.myData.bgColor
            self.isUserInteractionEnabled = true
        }else{
            self.backgroundColor = UIColor.gray
            self.isUserInteractionEnabled = false
        }

        if ((textField.text?.length)! + (string.length - range.length)) > 8 {
            return false
        }
        
        return true
    }
    
}


