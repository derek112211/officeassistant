//
//  ObjC.h
//  ExpertMedical
//
//  Created by kelvin.lee(VTL) on 24/04/2018.
//  Copyright © 2018 VTL-Solutions Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ObjC : NSObject

-(void)test;
        
@end
